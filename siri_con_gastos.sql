-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 06-02-2014 a las 19:16:56
-- Versión del servidor: 5.5.34
-- Versión de PHP: 5.3.10-1ubuntu3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `inventarios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogos_formas_pago`
--

CREATE TABLE IF NOT EXISTS `catalogos_formas_pago` (
  `id_forma_pago` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_forma_pago` varchar(45) NOT NULL,
  `tipo_boveda` int(11) NOT NULL,
  PRIMARY KEY (`id_forma_pago`),
  KEY `fk_catalogos_formas_pago_catalogos_tipos_bovedas1_idx` (`tipo_boveda`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `catalogos_formas_pago`
--

INSERT INTO `catalogos_formas_pago` (`id_forma_pago`, `activo`, `nombre_forma_pago`, `tipo_boveda`) VALUES
(1, 1, 'Efectivo', 1),
(2, 1, 'Cuerpomático', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogos_personas`
--

CREATE TABLE IF NOT EXISTS `catalogos_personas` (
  `id_persona` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombres` varchar(45) NOT NULL,
  `apellido_paterno` varchar(45) NOT NULL,
  `apellido_materno` varchar(45) NOT NULL,
  `sexo` char(1) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `codigo_postal` mediumint(5) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `curp` varchar(18) DEFAULT NULL,
  `seccion_electoral` varchar(45) DEFAULT NULL,
  `entidad_federativa` int(11) NOT NULL,
  `municipio` int(11) NOT NULL,
  `localidad` int(11) NOT NULL,
  PRIMARY KEY (`id_persona`),
  KEY `fk_catalogos_personas_universoyucatan_entidades1_idx` (`entidad_federativa`),
  KEY `fk_catalogos_personas_universo_yucatan_municipios1_idx` (`municipio`),
  KEY `fk_catalogos_personas_universoyucatan_localidades1_idx` (`localidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `catalogos_personas`
--

INSERT INTO `catalogos_personas` (`id_persona`, `activo`, `nombres`, `apellido_paterno`, `apellido_materno`, `sexo`, `direccion`, `codigo_postal`, `telefono`, `curp`, `seccion_electoral`, `entidad_federativa`, `municipio`, `localidad`) VALUES
(1, 1, 'LUIS', 'PECH', 'RUIZ', 'M', 'UNA CALLE', NULL, NULL, NULL, NULL, 1, 1, 3),
(2, 1, 'GEORGINA', 'CHAN', 'PECH', 'F', 'UNA CALLE', NULL, NULL, NULL, NULL, 1, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogos_puestos`
--

CREATE TABLE IF NOT EXISTS `catalogos_puestos` (
  `id_puesto` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_puesto` varchar(45) NOT NULL,
  PRIMARY KEY (`id_puesto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `catalogos_puestos`
--

INSERT INTO `catalogos_puestos` (`id_puesto`, `activo`, `nombre_puesto`) VALUES
(1, 1, 'ADMINISTRADOR'),
(2, 1, 'CAJERO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogos_tipos_concepto`
--

CREATE TABLE IF NOT EXISTS `catalogos_tipos_concepto` (
  `id_tipo_concepto` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_tipo_concepto` varchar(45) NOT NULL,
  `descripcion_tipo_concepto` mediumtext NOT NULL,
  `tipo_cuenta` int(11) NOT NULL,
  PRIMARY KEY (`id_tipo_concepto`),
  KEY `fk_catalogos_tipos_concepto_catalogos_tipos_cuentas1_idx` (`tipo_cuenta`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `catalogos_tipos_concepto`
--

INSERT INTO `catalogos_tipos_concepto` (`id_tipo_concepto`, `activo`, `nombre_tipo_concepto`, `descripcion_tipo_concepto`, `tipo_cuenta`) VALUES
(1, 1, 'Licitacion', 'una licitacion', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogos_tipos_cuentas`
--

CREATE TABLE IF NOT EXISTS `catalogos_tipos_cuentas` (
  `id_tipo_cuenta` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_tipo_cuenta` varchar(45) NOT NULL,
  `aumenta_capital` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_tipo_cuenta`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `catalogos_tipos_cuentas`
--

INSERT INTO `catalogos_tipos_cuentas` (`id_tipo_cuenta`, `activo`, `nombre_tipo_cuenta`, `aumenta_capital`) VALUES
(1, 1, 'Ingreso', 1),
(2, 1, 'Egreso', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogos_usuarios`
--

CREATE TABLE IF NOT EXISTS `catalogos_usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_usuario` varchar(45) NOT NULL,
  `contrasena` varchar(45) NOT NULL,
  `puesto` int(11) NOT NULL,
  `persona` int(11) NOT NULL,
  `permisos` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_usuario`),
  KEY `fk_usuarios_puestos_idx` (`puesto`),
  KEY `fk_usuarios_personas1_idx` (`persona`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `catalogos_usuarios`
--

INSERT INTO `catalogos_usuarios` (`id_usuario`, `activo`, `nombre_usuario`, `contrasena`, `puesto`, `persona`, `permisos`) VALUES
(2, 1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogos_usuarios_permisos`
--

CREATE TABLE IF NOT EXISTS `catalogos_usuarios_permisos` (
  `id_permiso` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_permiso` varchar(45) NOT NULL,
  `valor` smallint(6) NOT NULL,
  PRIMARY KEY (`id_permiso`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nocomprometido_amigos`
--

CREATE TABLE IF NOT EXISTS `nocomprometido_amigos` (
  `id_amigo` int(11) NOT NULL AUTO_INCREMENT,
  `catalogos_usuarios_id_usuario` int(11) NOT NULL,
  `secretaria_amigo` int(11) NOT NULL,
  PRIMARY KEY (`id_amigo`),
  KEY `fk_nocomprometido_amigos_catalogos_usuarios1_idx` (`catalogos_usuarios_id_usuario`),
  KEY `fk_nocomprometido_amigos_universoyucatan_secretarias1_idx` (`secretaria_amigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nocomprometido_estado_licitacion`
--

CREATE TABLE IF NOT EXISTS `nocomprometido_estado_licitacion` (
  `id_estado_no_comprometido` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_estado` varchar(45) NOT NULL,
  PRIMARY KEY (`id_estado_no_comprometido`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nocomprometido_licitaciones`
--

CREATE TABLE IF NOT EXISTS `nocomprometido_licitaciones` (
  `id_licitacion_no_comprometida` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(45) NOT NULL,
  `nombre_licitacion` varchar(45) NOT NULL,
  `fecha_pago` datetime NOT NULL,
  `monto_entregado` float(24,2) NOT NULL,
  `organizacion_beneficiada` int(11) NOT NULL,
  `agregada_por` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `prioridad` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_licitacion_no_comprometida`),
  KEY `fk_nocomprometido_licitaciones_universoyucatan_organizacion_idx` (`organizacion_beneficiada`),
  KEY `fk_nocomprometido_licitaciones_nocomprometido_amigos1_idx` (`agregada_por`),
  KEY `fk_nocomprometido_licitaciones_nocomprometido_estado_licita_idx` (`estado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_bovedas`
--

CREATE TABLE IF NOT EXISTS `siri_bovedas` (
  `id_tipo_boveda` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_boveda` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo_boveda`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `siri_bovedas`
--

INSERT INTO `siri_bovedas` (`id_tipo_boveda`, `activo`, `nombre_boveda`) VALUES
(1, 1, 'Caja Chica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_bovedas_estados`
--

CREATE TABLE IF NOT EXISTS `siri_bovedas_estados` (
  `id_estado_boveda` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `tipo_boveda` int(11) NOT NULL,
  `saldo_actual` float(24,2) NOT NULL,
  PRIMARY KEY (`id_estado_boveda`),
  KEY `fk_estado_boveda_catalogos_tipos_bovedas1_idx` (`tipo_boveda`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `siri_bovedas_estados`
--

INSERT INTO `siri_bovedas_estados` (`id_estado_boveda`, `activo`, `tipo_boveda`, `saldo_actual`) VALUES
(1, 1, 1, 1000.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_cajas`
--

CREATE TABLE IF NOT EXISTS `siri_cajas` (
  `id_caja` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_caja` varchar(45) NOT NULL,
  `tipo_boveda` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_caja`),
  KEY `fk_cajas_catalogos_tipos_bovedas1_idx` (`tipo_boveda`),
  KEY `fk_cajas_usuarios1_idx` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_cajas_historial`
--

CREATE TABLE IF NOT EXISTS `siri_cajas_historial` (
  `id_caja_historial` int(11) NOT NULL AUTO_INCREMENT,
  `caja` int(11) NOT NULL,
  `fecha_apertura` datetime NOT NULL,
  `saldo_apertura` float(24,2) NOT NULL DEFAULT '0.00',
  `fecha_cierre` datetime NOT NULL,
  `saldo_cierre` float(24,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id_caja_historial`),
  KEY `fk_siri_cajas_historial_siri_cajas1_idx` (`caja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_conceptos`
--

CREATE TABLE IF NOT EXISTS `siri_conceptos` (
  `id_concepto` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_concepto` varchar(50) NOT NULL,
  `descripcion_concepto` mediumtext NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipo_cuenta` int(11) NOT NULL,
  `tipo_concepto` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id_concepto`),
  KEY `fk_conceptos_tipos_concepto1_idx` (`tipo_concepto`),
  KEY `fk_siri_conceptos_siri_estados_concepto1_idx` (`estado`),
  KEY `fk_siri_conceptos_catalogos_tipos_cuentas1_idx` (`tipo_cuenta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_conceptos_detalles`
--

CREATE TABLE IF NOT EXISTS `siri_conceptos_detalles` (
  `id_detalle_concepto` int(11) NOT NULL AUTO_INCREMENT,
  `concepto` int(11) NOT NULL,
  `duracion_en_meses` mediumint(9) NOT NULL,
  `ente_generador` int(11) NOT NULL,
  `responsable_proyecto` int(11) NOT NULL,
  `intermediario` int(11) NOT NULL,
  `latitud` float DEFAULT NULL,
  `longitud` float DEFAULT NULL,
  `municipio` int(11) NOT NULL,
  `localidad` int(11) NOT NULL,
  `entidad_federativa` int(11) NOT NULL,
  PRIMARY KEY (`id_detalle_concepto`),
  KEY `fk_detalles_concepto_universoyucatan_municipios1_idx` (`municipio`),
  KEY `fk_detalles_concepto_universoyucatan_localidades1_idx` (`localidad`),
  KEY `fk_detalles_concepto_universoyucatan_entidades1_idx` (`entidad_federativa`),
  KEY `fk_siri_detalles_concepto_siri_conceptos1_idx` (`concepto`),
  KEY `fk_siri_conceptos_detalles_siri_responsables_proyectos1_idx` (`responsable_proyecto`),
  KEY `fk_siri_conceptos_detalles_siri_entes_generadores_flujos1_idx` (`ente_generador`),
  KEY `fk_siri_conceptos_detalles_siri_intermediarios1_idx` (`intermediario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_conceptos_detalles_financieros`
--

CREATE TABLE IF NOT EXISTS `siri_conceptos_detalles_financieros` (
  `id_detalle_financieros` int(11) NOT NULL AUTO_INCREMENT,
  `concepto` int(11) NOT NULL,
  `costo_publicado` float(24,2) NOT NULL,
  `costo_autorizado` float(24,2) NOT NULL,
  `costo_incrementado` float(24,2) NOT NULL DEFAULT '0.00',
  `monto_acordado` float(24,2) NOT NULL,
  `numero_pagos` int(11) NOT NULL DEFAULT '1',
  `forma_pago` int(11) NOT NULL,
  PRIMARY KEY (`id_detalle_financieros`),
  KEY `fk_detalles_financieros_catalogos_formas_pago1_idx` (`forma_pago`),
  KEY `fk_siri_detalles_financieros_siri_conceptos1_idx` (`concepto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_entes_generadores_flujos`
--

CREATE TABLE IF NOT EXISTS `siri_entes_generadores_flujos` (
  `id_ente` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `titular` int(11) NOT NULL,
  `secretaria` int(11) NOT NULL,
  PRIMARY KEY (`id_ente`),
  KEY `fk_entes_generadores_flujos_universoyucatan_secretarias1_idx` (`secretaria`),
  KEY `titular` (`titular`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `siri_entes_generadores_flujos`
--

INSERT INTO `siri_entes_generadores_flujos` (`id_ente`, `activo`, `titular`, `secretaria`) VALUES
(1, 1, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_estados_concepto`
--

CREATE TABLE IF NOT EXISTS `siri_estados_concepto` (
  `id_estado_concepto` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_estado_concepto` varchar(45) NOT NULL,
  PRIMARY KEY (`id_estado_concepto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_gastos`
--

CREATE TABLE IF NOT EXISTS `siri_gastos` (
  `id_gasto` int(11) NOT NULL AUTO_INCREMENT,
  `concepto` int(11) NOT NULL,
  `encargado_pago` int(11) NOT NULL,
  `organizacion` int(11) NOT NULL,
  `monto_entregado` float(24,2) NOT NULL,
  `fecha_entrega` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_pago` timestamp NULL DEFAULT NULL,
  `fecha_confirmacion` timestamp NULL DEFAULT NULL,
  `evidencia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_gasto`),
  KEY `fk_siri_gastos_siri_conceptos1_idx` (`concepto`),
  KEY `fk_siri_gastos_universoyucatan_organizaciones1_idx` (`organizacion`),
  KEY `fk_siri_gastos_catalogos_personas1_idx` (`encargado_pago`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_intermediarios`
--

CREATE TABLE IF NOT EXISTS `siri_intermediarios` (
  `id_intermediario` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `persona` int(11) NOT NULL,
  `organizacion` int(11) NOT NULL,
  PRIMARY KEY (`id_intermediario`),
  KEY `fk_intermediarios_universoyucatan_organizaciones1_idx` (`organizacion`),
  KEY `fk_siri_intermediarios_catalogos_personas1_idx` (`persona`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `siri_intermediarios`
--

INSERT INTO `siri_intermediarios` (`id_intermediario`, `activo`, `persona`, `organizacion`) VALUES
(1, 1, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_movimientos`
--

CREATE TABLE IF NOT EXISTS `siri_movimientos` (
  `id_pago` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `concepto` int(11) NOT NULL,
  `fecha_pago` datetime NOT NULL,
  `forma_pago` int(11) NOT NULL,
  `tipo_cuenta` int(11) NOT NULL,
  `monto` float(24,2) NOT NULL DEFAULT '0.00',
  `gasto_comprobar` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_pago`),
  KEY `fk_pagos_siri_conceptos1_idx` (`concepto`),
  KEY `fk_siri_pagos_catalogos_formas_pago1_idx` (`forma_pago`),
  KEY `fk_siri_pagos_catalogos_tipos_cuentas1_idx` (`tipo_cuenta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `siri_responsables_proyectos`
--

CREATE TABLE IF NOT EXISTS `siri_responsables_proyectos` (
  `id_responsable` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `persona` int(11) NOT NULL,
  `organizacion` int(11) NOT NULL,
  PRIMARY KEY (`id_responsable`),
  KEY `fk_responsables_proyectos_universoyucatan_organizaciones1_idx` (`organizacion`),
  KEY `fk_siri_responsables_proyectos_catalogos_personas1_idx` (`persona`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `siri_responsables_proyectos`
--

INSERT INTO `siri_responsables_proyectos` (`id_responsable`, `activo`, `persona`, `organizacion`) VALUES
(1, 1, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `universoyucatan_entidades_federativas`
--

CREATE TABLE IF NOT EXISTS `universoyucatan_entidades_federativas` (
  `id_entidad_federativa` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_entidad` varchar(45) NOT NULL,
  `nombre_corto` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id_entidad_federativa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `universoyucatan_entidades_federativas`
--

INSERT INTO `universoyucatan_entidades_federativas` (`id_entidad_federativa`, `activo`, `nombre_entidad`, `nombre_corto`) VALUES
(1, 1, 'Campeche', NULL),
(2, 1, 'Yucatán', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `universoyucatan_localidades`
--

CREATE TABLE IF NOT EXISTS `universoyucatan_localidades` (
  `id_localidad` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_localidad` varchar(45) NOT NULL,
  `municipio` int(11) NOT NULL,
  PRIMARY KEY (`id_localidad`),
  KEY `fk_table1_universo_yucatan_municipios1_idx` (`municipio`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `universoyucatan_localidades`
--

INSERT INTO `universoyucatan_localidades` (`id_localidad`, `activo`, `nombre_localidad`, `municipio`) VALUES
(3, 1, 'Campeche Centro', 1),
(4, 1, 'Mérida Centro', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `universoyucatan_municipios`
--

CREATE TABLE IF NOT EXISTS `universoyucatan_municipios` (
  `id_municipio` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_municipio` varchar(45) NOT NULL,
  `nombre_corto` varchar(45) DEFAULT NULL,
  `entidad_federativa` int(11) NOT NULL,
  PRIMARY KEY (`id_municipio`),
  KEY `fk_universo_yucatan_municipios_universoyucatan_estados1_idx` (`entidad_federativa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `universoyucatan_municipios`
--

INSERT INTO `universoyucatan_municipios` (`id_municipio`, `activo`, `nombre_municipio`, `nombre_corto`, `entidad_federativa`) VALUES
(1, 1, 'Campeche', NULL, 1),
(2, 1, 'Mérida', NULL, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `universoyucatan_organizaciones`
--

CREATE TABLE IF NOT EXISTS `universoyucatan_organizaciones` (
  `id_organizacion` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_organizacion` varchar(150) NOT NULL,
  `descripcion` mediumtext NOT NULL,
  `siglas` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) NOT NULL,
  `entidad_federativa` int(11) NOT NULL,
  `municipio` int(11) NOT NULL,
  `localidad` int(11) NOT NULL,
  PRIMARY KEY (`id_organizacion`),
  KEY `fk_universoyucatan_organizaciones_universoyucatan_entidades_idx` (`entidad_federativa`),
  KEY `fk_universoyucatan_organizaciones_universoyucatan_municipio_idx` (`municipio`),
  KEY `fk_universoyucatan_organizaciones_universoyucatan_localidad_idx` (`localidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `universoyucatan_organizaciones`
--

INSERT INTO `universoyucatan_organizaciones` (`id_organizacion`, `activo`, `nombre_organizacion`, `descripcion`, `siglas`, `direccion`, `entidad_federativa`, `municipio`, `localidad`) VALUES
(1, 1, 'Servicios Profesionales', 'una descripción', 'Serpro', 'una calle', 1, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `universoyucatan_secretarias`
--

CREATE TABLE IF NOT EXISTS `universoyucatan_secretarias` (
  `id_secretaria` int(11) NOT NULL AUTO_INCREMENT,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `nombre_secretaria` varchar(255) NOT NULL,
  `siglas` varchar(45) NOT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `entidad_federativa` int(11) NOT NULL,
  `municipio` int(11) NOT NULL,
  `localidad` int(11) NOT NULL,
  PRIMARY KEY (`id_secretaria`),
  KEY `fk_universoyucatan_secretarias_universoyucatan_entidades1_idx` (`entidad_federativa`),
  KEY `fk_universoyucatan_secretarias_universo_yucatan_municipios1_idx` (`municipio`),
  KEY `fk_universoyucatan_secretarias_universoyucatan_localidades1_idx` (`localidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `universoyucatan_secretarias`
--

INSERT INTO `universoyucatan_secretarias` (`id_secretaria`, `activo`, `nombre_secretaria`, `siglas`, `direccion`, `entidad_federativa`, `municipio`, `localidad`) VALUES
(1, 1, 'Secretaria X', 'SECRETARIA', 'Una calle', 2, 2, 4);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `catalogos_formas_pago`
--
ALTER TABLE `catalogos_formas_pago`
  ADD CONSTRAINT `fk_catalogos_formas_pago_catalogos_tipos_bovedas1` FOREIGN KEY (`tipo_boveda`) REFERENCES `siri_bovedas` (`id_tipo_boveda`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `catalogos_personas`
--
ALTER TABLE `catalogos_personas`
  ADD CONSTRAINT `fk_catalogos_personas_universoyucatan_entidades1` FOREIGN KEY (`entidad_federativa`) REFERENCES `universoyucatan_entidades_federativas` (`id_entidad_federativa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_catalogos_personas_universoyucatan_localidades1` FOREIGN KEY (`localidad`) REFERENCES `universoyucatan_localidades` (`id_localidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_catalogos_personas_universo_yucatan_municipios1` FOREIGN KEY (`municipio`) REFERENCES `universoyucatan_municipios` (`id_municipio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `catalogos_tipos_concepto`
--
ALTER TABLE `catalogos_tipos_concepto`
  ADD CONSTRAINT `fk_catalogos_tipos_concepto_catalogos_tipos_cuentas1` FOREIGN KEY (`tipo_cuenta`) REFERENCES `catalogos_tipos_cuentas` (`id_tipo_cuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `catalogos_usuarios`
--
ALTER TABLE `catalogos_usuarios`
  ADD CONSTRAINT `fk_usuarios_personas1` FOREIGN KEY (`persona`) REFERENCES `catalogos_personas` (`id_persona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_puestos` FOREIGN KEY (`puesto`) REFERENCES `catalogos_puestos` (`id_puesto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `nocomprometido_amigos`
--
ALTER TABLE `nocomprometido_amigos`
  ADD CONSTRAINT `fk_nocomprometido_amigos_catalogos_usuarios1` FOREIGN KEY (`catalogos_usuarios_id_usuario`) REFERENCES `catalogos_usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_nocomprometido_amigos_universoyucatan_secretarias1` FOREIGN KEY (`secretaria_amigo`) REFERENCES `universoyucatan_secretarias` (`id_secretaria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `nocomprometido_licitaciones`
--
ALTER TABLE `nocomprometido_licitaciones`
  ADD CONSTRAINT `fk_nocomprometido_licitaciones_universoyucatan_organizaciones1` FOREIGN KEY (`organizacion_beneficiada`) REFERENCES `universoyucatan_organizaciones` (`id_organizacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_nocomprometido_licitaciones_nocomprometido_amigos1` FOREIGN KEY (`agregada_por`) REFERENCES `nocomprometido_amigos` (`id_amigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_nocomprometido_licitaciones_nocomprometido_estado_licitaci1` FOREIGN KEY (`estado`) REFERENCES `nocomprometido_estado_licitacion` (`id_estado_no_comprometido`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `siri_bovedas_estados`
--
ALTER TABLE `siri_bovedas_estados`
  ADD CONSTRAINT `fk_estado_boveda_catalogos_tipos_bovedas1` FOREIGN KEY (`tipo_boveda`) REFERENCES `siri_bovedas` (`id_tipo_boveda`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `siri_cajas`
--
ALTER TABLE `siri_cajas`
  ADD CONSTRAINT `fk_cajas_catalogos_tipos_bovedas1` FOREIGN KEY (`tipo_boveda`) REFERENCES `siri_bovedas` (`id_tipo_boveda`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cajas_usuarios1` FOREIGN KEY (`usuario`) REFERENCES `catalogos_usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `siri_cajas_historial`
--
ALTER TABLE `siri_cajas_historial`
  ADD CONSTRAINT `fk_siri_cajas_historial_siri_cajas1` FOREIGN KEY (`caja`) REFERENCES `siri_cajas` (`id_caja`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `siri_conceptos`
--
ALTER TABLE `siri_conceptos`
  ADD CONSTRAINT `fk_conceptos_tipos_concepto1` FOREIGN KEY (`tipo_concepto`) REFERENCES `catalogos_tipos_concepto` (`id_tipo_concepto`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_siri_conceptos_siri_estados_concepto1` FOREIGN KEY (`estado`) REFERENCES `siri_estados_concepto` (`id_estado_concepto`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_siri_conceptos_catalogos_tipos_cuentas1` FOREIGN KEY (`tipo_cuenta`) REFERENCES `catalogos_tipos_cuentas` (`id_tipo_cuenta`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `siri_conceptos_detalles`
--
ALTER TABLE `siri_conceptos_detalles`
  ADD CONSTRAINT `fk_detalles_concepto_universoyucatan_entidades1` FOREIGN KEY (`entidad_federativa`) REFERENCES `universoyucatan_entidades_federativas` (`id_entidad_federativa`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_detalles_concepto_universoyucatan_localidades1` FOREIGN KEY (`localidad`) REFERENCES `universoyucatan_localidades` (`id_localidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalles_concepto_universoyucatan_municipios1` FOREIGN KEY (`municipio`) REFERENCES `universoyucatan_municipios` (`id_municipio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siri_detalles_concepto_siri_conceptos1` FOREIGN KEY (`concepto`) REFERENCES `siri_conceptos` (`id_concepto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siri_conceptos_detalles_siri_responsables_proyectos1` FOREIGN KEY (`responsable_proyecto`) REFERENCES `siri_responsables_proyectos` (`id_responsable`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siri_conceptos_detalles_siri_entes_generadores_flujos1` FOREIGN KEY (`ente_generador`) REFERENCES `siri_entes_generadores_flujos` (`id_ente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siri_conceptos_detalles_siri_intermediarios1` FOREIGN KEY (`intermediario`) REFERENCES `siri_intermediarios` (`id_intermediario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `siri_conceptos_detalles_financieros`
--
ALTER TABLE `siri_conceptos_detalles_financieros`
  ADD CONSTRAINT `fk_detalles_financieros_catalogos_formas_pago1` FOREIGN KEY (`forma_pago`) REFERENCES `catalogos_formas_pago` (`id_forma_pago`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siri_detalles_financieros_siri_conceptos1` FOREIGN KEY (`concepto`) REFERENCES `siri_conceptos` (`id_concepto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `siri_entes_generadores_flujos`
--
ALTER TABLE `siri_entes_generadores_flujos`
  ADD CONSTRAINT `fk_entes_generadores_flujos_universoyucatan_secretarias1` FOREIGN KEY (`secretaria`) REFERENCES `universoyucatan_secretarias` (`id_secretaria`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `siri_entes_generadores_flujos_ibfk_1` FOREIGN KEY (`titular`) REFERENCES `catalogos_personas` (`id_persona`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `siri_gastos`
--
ALTER TABLE `siri_gastos`
  ADD CONSTRAINT `fk_siri_gastos_siri_conceptos1` FOREIGN KEY (`concepto`) REFERENCES `siri_conceptos` (`id_concepto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siri_gastos_universoyucatan_organizaciones1` FOREIGN KEY (`organizacion`) REFERENCES `universoyucatan_organizaciones` (`id_organizacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siri_gastos_catalogos_personas1` FOREIGN KEY (`encargado_pago`) REFERENCES `catalogos_personas` (`id_persona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `siri_intermediarios`
--
ALTER TABLE `siri_intermediarios`
  ADD CONSTRAINT `fk_intermediarios_universoyucatan_organizaciones1` FOREIGN KEY (`organizacion`) REFERENCES `universoyucatan_organizaciones` (`id_organizacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siri_intermediarios_catalogos_personas1` FOREIGN KEY (`persona`) REFERENCES `catalogos_personas` (`id_persona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `siri_movimientos`
--
ALTER TABLE `siri_movimientos`
  ADD CONSTRAINT `fk_pagos_siri_conceptos1` FOREIGN KEY (`concepto`) REFERENCES `siri_conceptos` (`id_concepto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siri_pagos_catalogos_formas_pago1` FOREIGN KEY (`forma_pago`) REFERENCES `catalogos_formas_pago` (`id_forma_pago`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siri_pagos_catalogos_tipos_cuentas1` FOREIGN KEY (`tipo_cuenta`) REFERENCES `catalogos_tipos_cuentas` (`id_tipo_cuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `siri_responsables_proyectos`
--
ALTER TABLE `siri_responsables_proyectos`
  ADD CONSTRAINT `fk_responsables_proyectos_universoyucatan_organizaciones1` FOREIGN KEY (`organizacion`) REFERENCES `universoyucatan_organizaciones` (`id_organizacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siri_responsables_proyectos_catalogos_personas1` FOREIGN KEY (`persona`) REFERENCES `catalogos_personas` (`id_persona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `universoyucatan_localidades`
--
ALTER TABLE `universoyucatan_localidades`
  ADD CONSTRAINT `fk_table1_universo_yucatan_municipios1` FOREIGN KEY (`municipio`) REFERENCES `universoyucatan_municipios` (`id_municipio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `universoyucatan_municipios`
--
ALTER TABLE `universoyucatan_municipios`
  ADD CONSTRAINT `fk_universo_yucatan_municipios_universoyucatan_estados1` FOREIGN KEY (`entidad_federativa`) REFERENCES `universoyucatan_entidades_federativas` (`id_entidad_federativa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `universoyucatan_organizaciones`
--
ALTER TABLE `universoyucatan_organizaciones`
  ADD CONSTRAINT `fk_universoyucatan_organizaciones_universoyucatan_entidades_f1` FOREIGN KEY (`entidad_federativa`) REFERENCES `universoyucatan_entidades_federativas` (`id_entidad_federativa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_universoyucatan_organizaciones_universoyucatan_municipios1` FOREIGN KEY (`municipio`) REFERENCES `universoyucatan_municipios` (`id_municipio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_universoyucatan_organizaciones_universoyucatan_localidades1` FOREIGN KEY (`localidad`) REFERENCES `universoyucatan_localidades` (`id_localidad`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `universoyucatan_secretarias`
--
ALTER TABLE `universoyucatan_secretarias`
  ADD CONSTRAINT `fk_universoyucatan_secretarias_universoyucatan_entidades1` FOREIGN KEY (`entidad_federativa`) REFERENCES `universoyucatan_entidades_federativas` (`id_entidad_federativa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_universoyucatan_secretarias_universoyucatan_localidades1` FOREIGN KEY (`localidad`) REFERENCES `universoyucatan_localidades` (`id_localidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_universoyucatan_secretarias_universo_yucatan_municipios1` FOREIGN KEY (`municipio`) REFERENCES `universoyucatan_municipios` (`id_municipio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
