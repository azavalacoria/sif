function ola(){
	alert("ola");

}

// Formatear campos numéricos con decimales
 function formatNumber2(input, numberOfDigitsBeforeComma, numberOfDigitsAfterComma, completeDecimalsWithZeros) {
  var num = input.value.replace(/\./g, ',');
  var numparts = num.split(',');
  numparts[0] = numparts[0].replace(/\D/g, ''); //Elimina cualquier cosa que no sea un carácter numérico
  if (numparts[0].length > 1) //Elimina 0 delante de otro carácter numérico
   if (numparts[0].substr(0, 1) == 0)
    numparts[0] = numparts[0].substr(1, numparts[0].length - 1);
  
  if (numparts[0].length > numberOfDigitsBeforeComma) {
   //Un bug de Android provoca que al picar 1234 se ponga 12,43!!!
   if (numparts.length == 1) { // Poner la coma automáticamente
    numparts = (numparts[0].substr(0,numberOfDigitsBeforeComma) + ',' + numparts[0].substr(numberOfDigitsBeforeComma,1)).split(',');
   }
   else {
    numparts[0] = numparts[0].substr(0,numberOfDigitsBeforeComma);
   }
  }
  if (numparts.length >= 2) {
   numparts[1] = numparts[1].replace(/\D/g, '');
   if (numparts[1].length > numberOfDigitsAfterComma)
    numparts[1] = numparts[1].substr(0,numberOfDigitsAfterComma);
  }
  if (completeDecimalsWithZeros == true) {
   if (numparts.length >= 2) {
    if (numparts[0].length == 0)
     numparts[0] = '0';
    numparts[1] = (numparts[1] + '00').substring(0,2);
    input.value = numparts[0] + ',' + numparts[1];
   }
   else {
    if (numparts[0].length > 0)
     input.value = numparts[0] + ',00';
    else
     input.value = '0,00';
   }
  }
  else {
   if (numparts.length >= 2)
    input.value = numparts[0] + ',' + numparts[1];
   else
    input.value = numparts[0];
  }
 }
	

