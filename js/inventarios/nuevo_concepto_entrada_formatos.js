jQuery(function($){
	//$("#monto_acordado").number(true, 2);
	//$("#costo_publicado").number(true, 2);
	//$("#costo_incrementado").number(true, 2);
	//$("#costo_autorizado").number(true, 2);
	$('#duracion_meses').number(true, 0);
	$('#numero_pagos').number(true, 0);
});

$(function () {
	$('#monto_acordado').priceFormat({
		prefix: '',
		allowNegative: true,
		centsSeparator: '.',
		thousandsSeparator: ','
	});
	$('#costo_publicado').priceFormat({
		prefix: '',
		allowNegative: true,
		centsSeparator: '.',
		thousandsSeparator: ','
	});
	$('#costo_incrementado').priceFormat({
		prefix: '',
		allowNegative: true,
		centsSeparator: '.',
		thousandsSeparator: ','
	});
	$('#costo_autorizado').priceFormat({
		prefix: '',
		allowNegative: true,
		centsSeparator: '.',
		thousandsSeparator: ','
	});
});