function cambia_estado (id , valor) {
	
	modelo = $('#modelo').val();

	$.ajax({
		url: $('#url_servicio').val(),
		async: false,
		type: "POST",
		data: {id_elemento : id, activo : valor, modelo : modelo},
		dataType: "json",
		success: function (datos) {
			if (datos.errores == 0) {
				if (datos.elemento.activo == 0) {
					$('#' + id).val(1);
					$('#' + id).attr('checked','FALSE');
				} else{
					$('#' + id).val(0);
					$('#' + id).attr('checked','TRUE');
				}
			} else{
				alert('Hubo problemas al actualizar el registro, intente más tarde');
			}
		}
	});
	
}