function desplegar_formulario_busqueda () {
	$('#form-grid').css('display','none');
	$('#search-grid').css('display','block');
	$('#table-results-grid').empty();
	//alert('hola');
}

function cancelar_busqueda_persona () {
	$('#form-grid').css('display','block');
	$('#search-grid').css('display','none');
	$('#table-results-grid').empty();
}

function buscar_persona () {
	apellidopaterno = $('#apellido_paterno').val();
	apellidomaterno = $('#apellido_materno').val();
	nombres = $('#nombres').val();
	$.ajax({
		url: $('#url_servicio').val(),
		async: false,
		type: "POST",
		data: {apellido_paterno : apellidopaterno, apellido_materno : apellidomaterno, nombres : nombres},
		dataType: "json",
		success: function (datos) {
			if (datos.errores == 0) {
				//rellenar_select('#municipio',datos.municipios);
				aplicar_resultados_busqueda(datos.personas, '#table-results-grid');
			} else{
				alert('El hubo problemas al obtener municipios, intente más tarde');
			};
		}
	});
	
}

function aplicar_resultados_busqueda (personas , elemento) {
	$(elemento).empty();
	encabezado = 
		'<tr>' +
		'<th>Opción</th> <th>Apellido Paterno</th> <th>Apellido Materno</th> <th>Nombres</th>' +
		'<tr>';
	$(elemento).append(encabezado);

	$.each(personas, function (i, c) {
		texto = '<tr>' + 
				'<td><input name="id_persona" type="radio" value="' + c.id + '"></td>' +
				'<td>' + c.apellidopaterno + '</td>' +
				'<td>' + c.apellidomaterno + '</td>' +
				'<td>' + c.nombres + '</td>' +
				'</tr>';
		$(elemento).append(texto);
	});
}

$(document).ready(function () {
	$('#seleccionar-persona').click(function () {
		id = $('input[name=id_persona]:checked', '#personas-form').val();
		if (id != 'undefined') {
			obtener_datos_persona(id);
		} else{
			alert('Debe seleccionar una persona del listado');
		};
	});
});

function obtener_datos_persona (id) {
	$.ajax({
		url: $('#url_servicio').val() + '/especifica',
		async: false,
		type: "POST",
		data: {id_persona: id},
		dataType: "json",
		success: function (datos) {
			if (datos.errores == 0) {
				//rellenar_select('#persona',datos.personas);
				asignar_datos_persona('#persona', '#persona_nombre', datos.personas);
				$('#search-grid').css('display','none');
				$('#form-grid').css('display','block');
			} else{
				alert('El hubo problemas al obtener municipios, intente más tarde');
			};
		}
	});
}

function asignar_datos_persona (elemento_id, elemento_nombre, datos) {
	//$(elemento).empty();
	$.each(datos, function (i, c) {
		$(elemento_id).val(c.id);
		//$(elemento).append('<option value= "' + c.id + '">' + c.nombre + '</option>');
		$(elemento_nombre).val(c.nombre);
	});
}