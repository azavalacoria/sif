function desplegar_formulario_busqueda_secretaria() {
	$('#form-grid').css('display','none');
	$('#search-org-grid').css('display','block');
}

function cancelar_busqueda_secretaria() {
	$('#form-grid').css('display','block');
	$('#search-org-grid').css('display','none');
}



function buscar_ente () {
	nombre = $('#nombre_ente').val();
	$.ajax({
		url: $('#url_org_servicio').val(),
		async: false,
		type: "POST",
		data: {nombre: nombre},
		dataType: "json",
		success: function (datos) {
			if (datos.errores == 0) {
				//rellenar_select('#municipio',datos.municipios);
				enlistar_secretarias('#table-org-results-grid', datos.secretarias);
			} else{
				alert('El hubo problemas al obtener las organizaciones, intente más tarde');
			};
		}
	});
}

function enlistar_secretarias (elemento, organizaciones) {
	$(elemento).empty();
	encabezado = 
		'<tr>' +
		'<th>Opción</th> <th>Nombre</th>'+
		'<tr>';
	$(elemento).append(encabezado);

	$.each(organizaciones, function (i, c) {
		texto = '<tr>' + 
				'<td><input name="id_secretaria" type="radio" value="' + c.id + '"></td>' +
				'<td>' + c.nombre + '</td>'
				'</tr>';
		$(elemento).append(texto);
	});
}

function asignar_ente () {
	id = $('input[name=id_secretaria]:checked', '#secretarias-form').val();
		if (id != 'undefined') {
			obtener_datos_organizacion(id);
		} else{
			alert('Debe seleccionar una persona del listado');
		};
}

function obtener_datos_organizacion (id) {
	console.log(id);
	$.ajax({
		url: $('#url_org_servicio').val() + '/especifica',
		async: false,
		type: "POST",
		data: {id: id},
		dataType: "json",
		success: function (datos) {
			if (datos.errores == 0) {
				//rellenar_select('#secretaria',datos.secretaria);

				asignar_datos_secretaria('#secretaria', '#secretaria_nombre', datos.secretaria);
				//aplicar_resultados_busqueda(datos.personas, '#table-results-grid');
				$('#search-org-grid').css('display','none');
				$('#form-grid').css('display','block');
				$('#table-org-results-grid').empty();
				$('#nombre_ente').val('');
				$('#secretaria').blur();
			} else{
				alert('El hubo problemas al obtener municipios, intente más tarde');
			};
		}
	});
}

function asignar_datos_secretaria (elemento_id, elemento_nombre, datos) {
	//$(elemento).empty();
	$.each(datos, function (i, c) {
		$(elemento_id).val(c.id);
		//$(elemento).append('<option value= "' + c.id + '">' + c.nombre + '</option>');
		$(elemento_nombre).val(c.nombre);
	});
}