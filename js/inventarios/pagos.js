function buscar_conceptos_por_tipo_cuenta() {
	cuenta = $('#tipo_cuenta').val();
	movimiento = $("#tipo_cuenta option:selected").text();

	if (cuenta != '') {
		$('#page-main-title').html('Agregar Nuevo Movimiento de ' + movimiento);
		$.ajax({
			url: $('#url_servicio_pagos').val() + "/obtener_concepto_por_cuenta",
			async: false,
			type: "POST",
			data: {cuenta: cuenta},
			dataType: "json",
			success: function (datos) {
				if (datos.errores == 0) {
					rellenar_select('#concepto',datos.conceptos);
				} else{
					alert('El hubo problemas al obtener los conceptos de esa cuenta, intente más tarde');
				};
			}
		});
	} else{
		$('#page-main-title').html('Agregar Nuevo Movimiento');
		
		$.Dialog({
			shadow: true,
			overlay: false,
			icon: '<span class="icon-warning"></span>',
			title: 'Alerta',
			width: 500,
			padding: 10,
			content: 'Debe elegir un tipo de cuenta para seleccionar un concepto'
		});
	}
}