function obtener_tipos_concepto() {
	cuenta = $('#tipo_cuenta').val();

	aumenta_capital(cuenta);
}

function aumenta_capital (cuenta) {
	$.ajax({
		url: $('#url_servicio_tipos_concepto').val() + "/aumenta_capital",
		async: false,
		type: "POST",
		data: {cuenta : cuenta},
		dataType: "json",
		success: function (datos) {
			if (datos.errores == 0) {
				//rellenar_select('#tipo_concepto',datos.tipos);
				//alert(datos.aumenta_capital)
				$('#form_nuevo_concepto').attr('action', datos.destino);
				if (datos.aumenta_capital == 0) {
					
					console.log('hola');
					$('#salida').css('display', 'block');
					$('#entrada').css('display','none');
					obtener_conceptos(cuenta, '#tipo_concepto_gasto')
				} else if (datos.aumenta_capital == 1) {
					$('#entrada').css('display','block');
					$('#salida').css('display', 'none');
					obtener_conceptos(cuenta, '#tipo_concepto')
				}
			} else{
				alert('El hubo problemas al obtener los tipos de concepto, intente más tarde');
			};
		}
	});
}

function obtener_conceptos (cuenta, elemento) {
	$.ajax({
		url: $('#url_servicio_tipos_concepto').val(),
		async: false,
		type: "POST",
		data: {cuenta : cuenta},
		dataType: "json",
		success: function (datos) {
			if (datos.errores == 0) {
				rellenar_select(elemento, datos.tipos);
				//aplicar_resultados_busqueda(datos.personas, '#table-results-grid');
			} else{
				alert('El hubo problemas al obtener los tipos de concepto, intente más tarde');
			};
		}
	});
}