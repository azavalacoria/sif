function desplegar_formulario_busqueda_organizacion() {
	$('#form-grid').css('display','none');
	$('#search-org-grid').css('display','block');
}

function cancelar_busqueda_organizacion() {
	$('#form-grid').css('display','block');
	$('#search-org-grid').css('display','none');
}


function buscar_organizacion () {
	nombre = $('#nombre_organizacion').val();
	$.ajax({
		url: $('#url_org_servicio').val(),
		async: false,
		type: "POST",
		data: {nombre: nombre},
		dataType: "json",
		success: function (datos) {
			if (datos.errores == 0) {
				//rellenar_select('#municipio',datos.municipios);
				enlistar_organizaciones('#table-org-results-grid', datos.organizaciones);
			} else{
				alert('El hubo problemas al obtener las organizaciones, intente más tarde');
			};
		}
	});
}

function enlistar_organizaciones (elemento, organizaciones) {
	$(elemento).empty();
	encabezado = 
		'<tr>' +
		'<th>Opción</th> <th>Nombre</th> <th>Descripción</th> <th>Dirección</th>' +
		'<tr>';
	$(elemento).append(encabezado);

	$.each(organizaciones, function (i, c) {
		texto = '<tr>' + 
				'<td><input name="id_organizacion" type="radio" value="' + c.id + '"></td>' +
				'<td>' + c.nombre + '</td>' +
				'<td>' + c.descripcion + '</td>' +
				'<td>' + c.direccion + '</td>' +
				'</tr>';
		$(elemento).append(texto);
	});
}

function asignar_organizacion () {
	id = $('input[name=id_organizacion]:checked', '#organizaciones-form').val();
		if (id != 'undefined') {
			obtener_datos_organizacion(id);
		} else{
			alert('Debe seleccionar una persona del listado');
		};
}

function obtener_datos_organizacion (id) {
	console.log(id);
	$.ajax({
		url: $('#url_org_servicio').val() + '/especifica',
		async: false,
		type: "POST",
		data: {id: id},
		dataType: "json",
		success: function (datos) {
			if (datos.errores == 0) {
				//rellenar_select('#organizacion',datos.organizacion);
				
				asignar_datos_organizacion('#organizacion', '#organizacion_nombre', datos.organizacion);

				$('#search-org-grid').css('display','none');
				$('#form-grid').css('display','block');
				$('#table-org-results-grid').empty();
				$('#nombre_organizacion').val('');
				$('#organizacion').blur();
			} else{
				alert('El hubo problemas al obtener las organizaciones, intente más tarde');
			};
		}
	});
}


function asignar_datos_organizacion (elemento_id, elemento_nombre, datos) {
	//$(elemento).empty();
	$.each(datos, function (i, c) {
		$(elemento_id).val(c.id);
		//$(elemento).append('<option value= "' + c.id + '">' + c.nombre + '</option>');
		$(elemento_nombre).val(c.nombre);
	});
}