function obtener_municipios () {
	entidad = $('#entidad_federativa').val();
	//alert($entidad);
	if (entidad != '' && entidad > 0) {
		//alert($('#url_servicio').val());
		$.ajax({
			url: $('#url_servicio').val(),
			async: false,
			type: "POST",
			data: {id_entidad : entidad},
			dataType: "json",
			success: function (datos) {
				if (datos.errores == 0) {
					rellenar_select('#municipio',datos.municipios);
				} else{
					alert('El hubo problemas al obtener municipios, intente más tarde');
				};
			}
		});
	} else{
		alert('Debe elegir una entidad federativa');
	};
}

function obtener_localidades () {
	municipio = $('#municipio').val();
	if (municipio != '' && municipio > 0) {
		url = $('#url_servicio').val() + "/obtener_localidad";
		$.ajax({
		url: url,
		async: false,
		type: "POST",
		data: {municipio: municipio},
		dataType: "json",
		success: function (datos) {
			if (datos.errores == 0) {
				rellenar_select('#localidad',datos.localidades);
			} else{
				alert('El hubo problemas al obtener las localidades, intente más tarde');
			};
		}
	});
	} else{
		alert('Debe elegir un municipio');
	};
}

function rellenar_select (elemento, datos) {
	$(elemento).empty();
	$.each(datos, function (i, c) {
		$(elemento).append(
			'<option value= "' + c.id + '">' + c.nombre + '</option>'
			);
	});
}

