$(function(){
	   $('#form_nueva_forma_pago').validate({
		   rules: {
			descripcion_forma_pago : {
					required : true
				},
			boveda : {
					required : true
				},
		   },
	  debug: true,
	  //errorElement: "span",
	  //errorContainer: $("#errores"),
	  submitHandler: function(form){
		//$('#agregar_nuevo_usuario').attr('type','submit');
		form.submit();
	  }
	  });

	   
	   $('#form_nueva_organizacion').validate({
		   rules: {
		   'nombre_organizacion': 'required',
		   'siglas_organizacion': 'required',
		   'descripcion_organizacion': 'required',
		   'direccion_organizacion': 'required',
		   'entidad_federativa': 'required',
		   'municipio': 'required',
		   'localidad': 'required'
		   },
	   messages: {
		   'nombre_organizacion': 'Ingresa una nombre para organización',
		   'siglas_organizacion': 'Ingresa una sigla para la organización',
		   'descripcion_organizacion': 'Ingresa una descripción para la organización',
		   'direccion_organizacion': 'Ingresa una dirección para la organización',
		   'entidad_federativa': 'Ingresa una entidad federativa para la organización',
		   'municipio': 'Ingresa un municipio para la organización',
		   'localidad': 'Ingresa una localidad para la organización'
	   },

	  debug: true,
	  //errorElement: "span",
	  //errorContainer: $("#errores"),
	  submitHandler: function(form){
		//$('#agregar_nuevo_usuario').attr('type','submit');
		form.submit();
	  }
	  });

	  


	  $('#form_nueva_secretaria').validate({
		   rules: {
		   'nombre_secretaria': 'required',
		   'siglas_secretaria': 'required',
		   'direccion_secretaria': 'required',
		   'entidad_federativa': 'required',
		   'municipio': 'required',
		   'localidad': 'required'
		   },
	   messages: {
		   'nombre_secretaria': 'Ingresa una nombre para la secretaría',
		   'siglas_secretaria': 'Ingresa siglas para la secretaría',
		   'direccion_secretaria': 'Ingresa una dirección para la secretaría',
		   'entidad_federativa': 'Ingresa una entidad federativa para la organización',
		   'municipio': 'Ingresa un municipio para la organización',
		   'localidad': 'Ingresa una localidad para la organización'
	   },

	  debug: true,
	  //errorElement: "span",
	  //errorContainer: $("#errores"),
	  submitHandler: function(form){
		//$('#agregar_nuevo_usuario').attr('type','submit');
		form.submit();
	  }
	  });

	  $('#form_nuevo_intermediario').validate({
		   rules: {
		   'persona_nombre': 'required',
		   'organizacion_nombre': 'required'
		   },
	   messages: {
		   'persona_nombre': 'Ingresa un responsable',
		   'organizacion_nombre': 'Ingresa una organización'
	   },

	  debug: true,
	  //errorElement: "span",
	  //errorContainer: $("#errores"),
	  submitHandler: function(form){
		//$('#agregar_nuevo_usuario').attr('type','submit');
		form.submit();
	  }
	  });

	  $('#form_nuevo_puesto').validate({
		   rules: {
		   'descripcion_puesto': 'required',
		   },
	   messages: {
		   'descripcion_puesto': 'Ingresa una descripción para el puesto'
	   },

	  debug: true,
	  //errorElement: "span",
	  //errorContainer: $("#errores"),
	  submitHandler: function(form){
		//$('#agregar_nuevo_usuario').attr('type','submit');
		form.submit();
	  }
	  });

		$('#form_responsable_proyecto').validate({
		   rules: {
		   'persona_nombre': 'required',
		   'organizacion_nombre': 'required',
		   },
	   messages: {
		   'persona_nombre': 'Ingresa un responsable',
		   'organizacion_nombre': 'Ingresa una organización'
	   },

	  debug: true,
	  //errorElement: "span",
	  //errorContainer: $("#errores"),
	  submitHandler: function(form){
		//$('#agregar_nuevo_usuario').attr('type','submit');
		form.submit();
	  }
	  });

	  $('#form_nuevo_tipo_boveda').validate({
		   rules: {
		   'descripcion_boveda': 'required',
		   'saldo_inicial': 'required'
		   },
	   messages: {
		   'descripcion_boveda': 'Ingresa un responsable',
		   'saldo_inicial': 'Ingresa una organización'
	   },

	  debug: true,
	  //errorElement: "span",
	  //errorContainer: $("#errores"),
	  submitHandler: function(form){
		//$('#agregar_nuevo_usuario').attr('type','submit');
		form.submit();
	  }
	  });

});



