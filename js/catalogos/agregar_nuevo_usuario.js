$(function () {
	$('#form_nuevo_usuario').validate({
		rules: {
				'nombre_usuario': {required : true, minlength: 4, maxlength: 15},
				'contrasena': {required : true, minlength: 6, maxlength: 25},
				'persona': 'required',
				'puesto': 'required'
			},
		messages: {
			'nombre_usuario' : { 
								required : 'El campo nombre de usuario es obligario',
								minlength : 'El nombre de usuario debe tener al menos {0} caracteres',
								maxlength : 'El nombre de usuario no puede rebasar los {0} caracteres'
							},
			'contrasena' : { 
								required : 'El campo contraseña es obligario',
								minlength : 'El nombre de usuario debe tener al menos {0} caracteres',
								maxlength : 'El nombre de usuario no puede rebasar los {0} caracteres'
							},
			'persona': 'Debe seleccionar a una persona',
			'puesto': 'Debe seleccionar un puesto'
		},

		debug: true,
		//errorElement: "span",
		//errorContainer: $("#errores"),
		submitHandler: function(form){
			//form.submit();
			//console.log($('#form_nuevo_usuario').serialize());
			$.ajax({
				url: 'http://sif.com/servicios/servicio_usuarios/validar_usuario_repetido',
				async: false,
				type: "POST",
				data: $('#form_nuevo_usuario').serialize(),
				dataType: "json",
				success: function (datos) {
					if (datos.repetido == 0) {
						form.submit();
					} else{
						alert('El nombre de usuario ya ha sido dado de alta. \n Intente con otro');
					}
				}
			});
		}
	});
});