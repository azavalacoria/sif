$(function() {
	$('#form_nuevo_concepto_salida').validate({
		rules: {
			tipo_cuenta : {required : true},
			nombre : {required : true, minlength: 10, maxlength : 50},
			tipo_concepto : {required : true},
			estado : {required : true},
		},
		messages: {
			'tipo_cuenta' : {
				required : 'El campo tipo de cuenta es obligario',
			},
			'nombre' : {
				required : 'El nombre del concepto es obligario',
				minlength : 'El nombre del concepto debe tener al menos {0} caracteres',
				maxlength : 'El nombre del concepto no puede rebasar los {0} caracteres'
			},
			'tipo_concepto' : {
				required : 'El campo contraseña es obligario',
			},
			'estado': {
				required: 'Debe asignarle un estado al concepto'
			}
		},
		debug: true,
		submitHandler: function(form){
			form.submit();
		}
	});
});