$(function() {
	$('#form_nuevo_concepto').validate({
		rules: {
			tipo_cuenta : {required : true},
			nombre : {required : true, minlength: 10, maxlength : 50},
			tipo_concepto : {required : true},
			estado : {required : true},
			duracion_meses : {required : true, number : true},
			costo_publicado : {required : true},
			costo_autorizado : {required : true},
			costo_incrementado : {required : true},
			monto_acordado : {required : true},
			forma_pago : {required : true},
			numero_pagos : {required : true, number : true},
			ente : {required : true},
			responsable : {required : true},
			intermediario : {required : true},
			entidad_federativa : {required : true},
			municipio : {required : true},
			localidad : {required : true},
		},
		messages: {
			'tipo_cuenta' : {
				required : 'El campo tipo de cuenta es obligario',
			},
			'nombre' : {
				required : 'El nombre del concepto es obligario',
				minlength : 'El nombre del concepto debe tener al menos {0} caracteres',
				maxlength : 'El nombre del concepto no puede rebasar los {0} caracteres'
			},
			'tipo_concepto' : {
				required : 'El campo contraseña es obligario',
			},
			'estado': {
				required: 'Debe asignarle un estado al concepto'
			},
			'duracion_meses' : {
				number : 'Éste campo sólo acepta números enteros',
				required : 'El campo duración en meses es obligario'
			},
			'costo_publicado': {
				required: 'Debe ingresar el monto con que fue publicado el concepto'
			},
			'costo_autorizado': {
				required: 'Debe ingresar el monto con que fue autorizado el concepto'
			},
			'costo_incrementado': {
				required: 'Debe ingresar el monto incrementado que se asignó al concepto'
			},
			'monto_acordado': {
				required: 'Debe ingresar el monto acordado en la negociación'
			},
			'forma_pago': {
				required: 'Debe elegir alguna forma de pago'
			},
			'numero_pagos': {
				required: 'Indique el numero de pagos para saldar éste concepto'
			},
			'ente': {
				required: 'Debe elegir un ente generador'
			},
			'responsable': {
				required: 'Debe elegir al responsable de éste concepto'
			},
			'intermediario': {
				required: 'Indique quién es el intermediario en éste concepto'
			},
			'entidad_federativa': {
				required: 'Seleccione una entidad federativa'
			},
			'municipio': {
				required: 'Seleccione el municipio en donde se realizarán las acciones del concepto'
			},
			'localidad': {
				required: 'Seleccione la localidad en donde se realizarán las acciones del concepto'
			},
		},
		debug: true,
		submitHandler: function(form){
			form.submit();
		}
	});
});