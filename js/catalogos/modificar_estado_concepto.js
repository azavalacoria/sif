$(function () {
	$('#form_modificar_estado_concepto').validate({
		rules: {
				'nombre_estado_concepto': {required : true, minlength: 3, maxlength: 45}
			},
		messages: {
			'nombre_estado_concepto' : { 
				required : 'El campo del estado es obligario',
				minlength : 'El nombre del estado debe tener al menos {0} caracteres',
				maxlength : 'El nombre del estado no puede rebasar los {0} caracteres'
			}
		},
		debug: true,
		//errorElement: "span",
		//errorContainer: $("#errores"),
		submitHandler: function(form){
			$.ajax({
				url: $('#url_servicio_catalogos').val() + "/modificar_estado", 
				async: false,
				type: "POST",
				data: $('#form_modificar_estado_concepto').serialize(),
				dataType: "json",
				success: function (datos) {
					if (datos.errores == 0) {
						if (datos.repetido == 0) {
							form.submit();
						} else{
							alert('Ya existe otro estado con los mismos datos. \n Intente con otros datos');
						}
					} else {
						alert('Ocurrió un error en el servidor, intente más tarde');
					}
					
				}
			});
		}
	});
});