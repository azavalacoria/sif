$(function() {
	$('#form_nueva_persona').validate({
		rules: {
			'nombres': {required: true, minlength: 2, maxlength: 45},
			'apellido_paterno': {required: true, minlength: 2, maxlength: 45},
			'apellido_materno': {required: true, minlength: 2, maxlength: 45},
			'sexo': {required: true},
			'curp' : {required: true, minlength: 18, maxlength: 18},
			'codigo_postal' : {required: true, minlength: 5, maxlength: 5, number : true},
			'telefono' : {required: true, minlength: 12, maxlength: 18},
			'direccion': {required: true, minlength: 2, maxlength: 45},
			'seccion_electoral': {required: true},
			'entidad_federativa': {required: true},
			'municipio': {required: true},
			'localidad': {required: true}
		},
		messages: {
			'nombres': {
				required : 'Ingresa un nombre para la persona',
				minlength : 'El nombre de la persona debe tener al menos {0} caracteres',
				maxlength : 'El nombre de la persona no puede rebasar los {0} caracteres'
			},
			'apellido_paterno': {
				required : 'Ingresa un apellido paterno para la persona',
				minlength : 'El apellido paterno debe tener al menos {0} caracteres',
				maxlength : 'El apellido paterno no puede rebasar los {0} caracteres'
			},
			'apellido_materno': {
				required : 'Ingresa un apellido materno para la persona',
				minlength : 'El apellido materno debe tener al menos {0} caracteres',
				maxlength : 'El apellido materno no puede rebasar los {0} caracteres'
			},
			'sexo': {
				required : 'Elija el sexo de la persona',
			},
			'seccion_electoral': {
				required : 'La sección electoral es obligatoria',
			},
			'curp' : {
				required : 'La clave del CURP es obligatoria',
				minlength : 'La clave del CURP debe tener al menos {0} caracteres',
				maxlength : 'La clave del CURP debe tener al menos {0} caracteres'
			},
			'direccion': {
				required : 'Ingresa una dirección para la persona',
				minlength : 'La dirección de la persona debe tener al menos {0} caracteres',
				maxlength : 'La dirección de la persona no puede rebasar los {0} caracteres'
			},
			'codigo_postal': {
				required : 'Ingresa el código postal en donde se localiza la persona',
				minlength : 'El código postal debe tener {0} números',
				maxlength : 'El código postal debe tener {0} números',
				number : 'El código postal sólo debe contener números'
			},
			'telefono': {
				required : 'Ingresa teléfono de la persona',
				minlength : 'El teléfono debe tener al menos {0} caracteres',
				maxlength : 'El teléfono no puede rebasar los {0} caracteres'
			},
			'entidad_federativa' : {
				required : 'Elija la entidad federativa en donde radica la persona'
			},
			'municipio' : {
				required : 'Elija el municipio en donde radica la persona'
			},
			'localidad': {
				required : 'Elija la localidad en donde radica la persona'
			}
		},
		debug: true,
		submitHandler: function(form){
			form.submit();
		}
	});
});