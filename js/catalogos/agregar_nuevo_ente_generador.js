$(function () {
	$('#form_nuevo_ente_generador').validate({
		rules: {
			'persona_nombre': 'required',
			'secretaria_nombre': 'required'
		},
		messages: {
			'persona_nombre': 'Ingresa un responsable',
			'secretaria_nombre': 'Ingresa una secretaría'
		},

		debug: true,
		submitHandler: function(form){
			$.ajax({
				url: $('#url_catalogo_servicio').val() + '/validar_ente_repetido',
				async: false,
				type: "POST",
				data: $('#form_nuevo_ente_generador').serialize(),
				dataType: "json",
				success: function (datos) {
					if (datos.repetido == 0) {
						form.submit();
					} else{
						alert('Otro ya ente generador ya ha sido dado de alta con esos datos. \n');
					}
				}
			});
		}
	});
});