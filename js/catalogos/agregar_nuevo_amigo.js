$(function () {
	$('#form_nuevo_amigo').validate({
		rules: {
				'nickname' 	: {required : true, minlength: 4, maxlength: 15},
				'password1'	: {required : true, minlength: 6, maxlength: 25},
				'password2'	: {required : true, minlength: 6, maxlength: 25, equalTo: '#password1'},
				'persona'	: {required : true},
				'secretaria': {required : true},
				'persona_nombre'   : {required : true},
				'secretaria_nombre': {required : true}
			},
		messages: {
			'nickname' : { 
								required : 'El campo nombre de usuario es obligario',
								minlength : 'El nombre de usuario debe tener al menos {0} caracteres',
								maxlength : 'El nombre de usuario no puede rebasar los {0} caracteres'
							},
			'password1' : { 
								required : 'El campo contraseña es obligario',
								minlength : 'La contraseña debe tener al menos {0} caracteres',
								maxlength : 'La contraseña no puede rebasar los {0} caracteres'
							},
			'password2' : { 
								required : 'El campo contraseña es obligario',
								minlength : 'La contraseña debe tener al menos {0} caracteres',
								maxlength : 'La contraseña no puede rebasar los {0} caracteres',
								equalTo : 'Los campos de contraseña deben coincidir'
							},
			'persona': {required: 'Debe seleccionar a una persona'},
			'secretaria': {required: 'Debe seleccionar una secretaria'},
			'persona_nombre': {required: 'Debe seleccionar a una persona'},
			'secretaria_nombre': {required: 'Debe seleccionar una secretaria'}
		},

		debug: true,
		//errorElement: "span",
		//errorContainer: $("#errores"),
		submitHandler: function(form){
			//form.submit();
			//console.log($('#form_nuevo_usuario').serialize());
			$.ajax({
				url: $('#url_catalogo_servicio').val() + '/validar_amigo_repetido',
				async: false,
				type: "POST",
				data: $('#form_nuevo_amigo').serialize(),
				dataType: "json",
				success: function (datos) {
					if (datos.repetido == 0) {
						form.submit();
					} else{
						alert('Otro ya ha sido dado de alta con esos datos. \n');
					}
				}
			});
		}
	});
});