$(function () {
	$('#form_nuevo_tipo_concepto').validate({
		rules: {
			'nombre_concepto': {required : true, minlength: 5, maxlength : 45},
			'descripcion_concepto': {required : true, minlength: 5, maxlength : 45},
			'tipo_cuenta' : {required : true}
		},
		messages: {

			'nombre_concepto': { 
				required : 'El nombre es obligario',
				minlength : 'El nombre debe tener al menos {0} caracteres',
				maxlength : 'El nombre no puede rebasar los {0} caracteres'
			},
			'descripcion_concepto': { 
				required : 'La descripción es obligaria',
				minlength : 'La descripción debe tener al menos {0} caracteres',
				maxlength : 'La descripción no puede rebasar los {0} caracteres'
			},
			'tipo_cuenta' : { 
				required : 'El campo tipo de cuenta es obligario',
			},
		},
		debug: true,
		submitHandler: function(form){
			
			$.ajax({
				url: $('#url_servicio_catalogos').val() + "/verificar_repetido_al_agregar", 
				async: false,
				type: "POST",
				data: $('#form_nuevo_tipo_concepto').serialize(),
				dataType: "json",
				success: function (datos) {
					if (datos.errores == 0) {
						if (datos.repetido == 0) {
							form.submit();
						} else{
							alert('Ya existe otro tipo de concepto con los mismos datos.');
						}
					} else {
						alert('Ocurrió un error en el servidor, intente más tarde');
					}
					
				}
			});
		}
	});
});