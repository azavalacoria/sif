<?php

/**
* 
*/
class Tipo_Cuenta extends CI_Model
{
	var $tabla = 'catalogos_tipos_cuentas';

	function __construct()
	{
		parent::__construct();
	}

	public function agregar_nuevo_tipo_cuenta($datos)
	{
		try {
			$cuenta = array_map('strtoupper', $datos);
			$this->db->insert($this->tabla, $cuenta);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			return array(
				'mensaje' => $this->db->_error_message(),
				'codigo' => $this->db->_error_number(),
				'excepcion' => $e->getMessage()
				);
		}
	}

	public function listar_tipos_cuenta()
	{
		$this->db->select('id_tipo_cuenta, nombre_tipo_cuenta');
		$this->db->order_by('nombre_tipo_cuenta', 'ASC');
		$this->db->where('activo', TRUE);
		
		$query = $this->db->get($this->tabla);

		$cuentas = array('' => 'Seleccione...');

		foreach ($query->result() as $cuenta) {
			$cuentas[$cuenta->id_tipo_cuenta] = $cuenta->nombre_tipo_cuenta;
		}

		return $cuentas;
	}

	public function listar_tipos_cuenta_entrada()
	{
		$this->db->select('id_tipo_cuenta, nombre_tipo_cuenta');
		$this->db->order_by('nombre_tipo_cuenta', 'ASC');
		$this->db->where('activo', TRUE);
		$this->db->where('aumenta_capital', 1);
		
		$query = $this->db->get($this->tabla);

		$cuentas = array('' => 'Seleccione...');

		foreach ($query->result() as $cuenta) {
			$cuentas[$cuenta->id_tipo_cuenta] = $cuenta->nombre_tipo_cuenta;
		}

		return $cuentas;
	}

	public function listar_tipos_cuenta_salida()
	{
		$this->db->select('id_tipo_cuenta, nombre_tipo_cuenta');
		$this->db->order_by('nombre_tipo_cuenta', 'ASC');
		$this->db->where('activo', TRUE);
		$this->db->where('aumenta_capital', 0);
		
		$query = $this->db->get($this->tabla);

		$cuentas = array('' => 'Seleccione...');

		foreach ($query->result() as $cuenta) {
			$cuentas[$cuenta->id_tipo_cuenta] = $cuenta->nombre_tipo_cuenta;
		}

		return $cuentas;
	}

	public function obtener_tipos_cuentas($inicio, $limite)
	{
		$this->db->select('id_tipo_cuenta, nombre_tipo_cuenta, aumenta_capital,'.$this->tabla.'.activo');
		$this->db->order_by('nombre_tipo_cuenta', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get($this->tabla);

		$cuentas = array();

		foreach ($query->result() as $s) {
			$cuenta = array(
					'id_tipo_cuenta' => $s->id_tipo_cuenta,
					'descripcion' => $s->nombre_tipo_cuenta,
					'aumenta_capital' => $s->aumenta_capital,
					'activo' => $s->activo
				);
			array_push($cuentas, $cuenta);
		}

		return $cuentas;
	}

	public function aumenta_capital($id_tipo_cuenta)
	{
		$this->db->select('aumenta_capital');
		$this->db->where('id_tipo_cuenta', $id_tipo_cuenta);
		$query = $this->db->get($this->tabla);
		$aumenta_capital = 0;

		foreach ($query->result() as $cuenta) {
			$aumenta_capital = $cuenta->aumenta_capital;
		}

		return $aumenta_capital;
	}

	public function cambiar_estado_activo($id_tipo_cuenta, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_tipo_cuenta', $id_tipo_cuenta);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_tipo_cuenta, activo');
		$this->db->where('id_tipo_cuenta', $id_tipo_cuenta);
		$query = $this->db->get($this->tabla);

		$cuenta = array();
		foreach ($query->result() as $c) {
			$cuenta = array('id_tipo_cuenta' => $c->id_tipo_cuenta, 'activo' => $c->activo);
		}

		return $cuenta;
	}
}
?>