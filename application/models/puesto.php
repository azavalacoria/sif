<?php

/**
* 
*/
class Puesto extends CI_Model
{
	var $tabla = "catalogos_puestos";

	function __construct()
	{
		parent::__construct();
	}

	public function obtener_todos_puestos()
	{
		$this->db->select('id_puesto, nombre_puesto');
		$this->db->where('activo', TRUE);
		$query = $this->db->get($this->tabla);

		$puestos[''] = 'Seleccione...';

		foreach ($query->result() as $p) {
			$puestos[$p->id_puesto] = $p->nombre_puesto;
		}
		return $puestos;
	}

	public function agregar_nuevo_puesto($datos)
	{
		$this->db->insert($this->tabla, $datos);
		return $this->db->affected_rows();
	}

	public function listar_puestos($inicio, $limite)
	{
		$this->db->select('id_puesto, nombre_puesto, activo');
		$this->db->order_by('nombre_puesto', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get($this->tabla);

		$puestos = array();

		foreach ($query->result() as $p) {
			$puesto = array('id_puesto' => $p->id_puesto, 'descripcion' => $p->nombre_puesto, 'activo' => $p->activo);
			array_push($puestos, $puesto);
		}

		return $puestos;
	}

	public function obtener_puesto($id_puesto)
	{
		$this->db->select('id_puesto, nombre_puesto');
		$this->db->where('id_puesto', $id_puesto);
		$this->db->where('activo', TRUE);
		$query = $this->db->get($this->tabla);

		$puesto = null;

		foreach ($query->result() as $p) {
			$puesto = array('id_puesto' => $p->id_puesto, 'descripcion' => $p->nombre_puesto);
		}

		return $puesto;
	}

	public function verificar_dato_repetido($datos)
	{
		$this->db->select('id_puesto');
		$this->db->from($this->tabla);
		$this->db->where($datos);

		return $this->db->count_all_results();
	}

	public function actualizar_puesto($id_puesto, $datos)
	{
		$this->db->where('id_puesto', $id_puesto);

		$this->db->update($this->tabla, $datos);

		return $this->db->affected_rows();
	}

	public function cambiar_estado_activo($id_puesto, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_puesto', $id_puesto);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_puesto, activo');
		$this->db->where('id_puesto', $id_puesto);
		$query = $this->db->get($this->tabla);
		
		$activo = null;

		$usuario = array();
		foreach ($query->result() as $p) {
			$usuario = array('id_puesto' => $p->id_puesto, 'activo' => $p->activo);
		}

		return $usuario;
	}
}
?>