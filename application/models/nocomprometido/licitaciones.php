<?php

/**
* 
*/
class Licitaciones extends CI_Model
{
	var $tabla = 'nocomprometido_licitaciones';

	function __construct()
	{
		parent::__construct();
	}

	public function obtener_licitaciones($inicio, $limite, $agregada_por)
	{
		$this->db->select('id_licitacion_no_comprometida AS id_licitacion, clave, nombre_licitacion, descripcion, 
			nombre_secretaria, prioridad , nombre_estado');
		$this->db->from($this->tabla);
		$this->db->join('nocomprometido_estado_licitacion', 'estado = id_estado_no_comprometido');
		$this->db->join('universoyucatan_secretarias', 'secretaria = id_secretaria');
		$this->db->where('agregada_por', $agregada_por);
		$this->db->limit($limite, $inicio);
		$query = $this->db->get();

		$licitaciones = array();
		foreach ($query->result_array() as $licitacion) {
			array_push($licitaciones, $licitacion);
		}
		return $licitaciones;
	}

	public function agregar_licitacion($datos)
	{
		$this->db->insert($this->tabla, $datos);
		return $this->db->affected_rows();
	}
}
?>