<?php

/**
* 
*/
class Fuente_Publicacion extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_todas_fuentes()
	{
		$this->db->select('idfuentes_publicacion as id_fuente, nombre_fuente as nombre');

		$this->db->where('activo', 1);

		$query = $this->db->get('nocomprometido_fuentes_publicacion');

		$fuentes = array('' => 'Seleccione...');

		foreach ($query->result() as $fuente) {
			$fuentes[$fuente->id_fuente] = $fuente->nombre;
		}

		return $fuentes;
	}
}
?>