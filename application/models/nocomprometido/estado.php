<?php

/**
* 
*/
class Estado extends CI_Model
{
	var $tabla = 'nocomprometido_estado_licitacion';
	
	function __construct()
	{
		parent::__construct();
	}

	public function listar_estados()
	{
		$this->db->select('id_estado_no_comprometido, nombre_estado');
		$query = $this->db->get($this->tabla);

		$estados = array('' => 'Seccione...');

		foreach ($query->result() as $estado) {
			$estados[$estado->id_estado_no_comprometido] = $estado->nombre_estado;
		}
		return $estados;
	}
}
?>