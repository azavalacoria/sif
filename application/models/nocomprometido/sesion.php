<?php

/**
* 
*/
class Sesion extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function existe_amigo($datos)
	{
		$this->db->select('id_amigo, persona, secretaria_amigo');
		$this->db->where($datos);
		$query = $this->db->get('nocomprometido_amigos');

		$amigo = array();
		foreach ($query->result() as $resultado) {
			$amigo = array(
						'id_amigo' => $resultado->id_amigo,
						'persona' => $resultado->persona,
						'secretaria_amigo' => $resultado->secretaria_amigo
					);
		}
		return $amigo;
	}
}
?>