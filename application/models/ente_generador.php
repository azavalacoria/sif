<?php

/**
* 
*/
class Ente_Generador extends CI_Model
{
	var $tabla = 'siri_entes_generadores_flujos';
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_todos_entes()
	{
		$this->db->select('id_ente, apellido_paterno, apellido_materno, nombres, nombre_secretaria');
		$this->db->join('catalogos_personas','titular = id_persona');
		$this->db->join('universoyucatan_secretarias', 'secretaria = id_secretaria');
		$this->db->where($this->tabla.'.activo', TRUE);

		$this->db->order_by('nombres', 'ASC');
		$this->db->order_by('apellido_paterno', 'ASC');
		$this->db->order_by('apellido_materno', 'ASC');
		$this->db->order_by('nombre_secretaria', 'ASC');

		$query = $this->db->get($this->tabla);
		$entes = array('' => 'Seleccione...');
		foreach ($query->result() as $tipo) {
			$nombre = $tipo->nombre_secretaria.' ('.$tipo->nombres.' '.$tipo->apellido_paterno.' '.$tipo->apellido_materno.') ';
			$entes[$tipo->id_ente] = $nombre;
		}
		return $entes;
	}

	public function agregar_nuevo_ente($datos)
	{
		$this->db->insert($this->tabla, $datos);
		return $this->db->affected_rows();
	}

	public function listar_entes($inicio, $limite)
	{
		$this->db->select('id_ente,nombres,apellido_paterno,apellido_materno,nombre_secretaria,'.$this->tabla.'.activo');
		$this->db->from($this->tabla);
		$this->db->join('catalogos_personas','titular = id_persona');
		$this->db->join('universoyucatan_secretarias', 'secretaria = id_secretaria');
		$this->db->order_by('activo', 'DESC');
		$this->db->order_by('apellido_paterno', 'ASC');
		$this->db->order_by('apellido_materno', 'ASC');
		$this->db->order_by('nombres', 'ASC');
		$this->db->order_by('nombre_secretaria', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get();
		$entes = array();
		foreach ($query->result() as $r) {
			$nombre = $r->apellido_paterno." ".$r->apellido_materno." ".$r->nombres;
			$ente = array(
					'id_ente' => $r->id_ente, 
					'nombre' => $nombre, 
					'secretaria' => $r->nombre_secretaria,
					'activo' => $r->activo
				);
			array_push($entes, $ente);
		}
		return $entes;
	}

	public function validar_ente_repetido($datos)
	{
		$this->db->select('id_ente');
		$this->db->from($this->tabla);
		$this->db->where($datos);

		return $this->db->count_all_results();
	}

	public function cambiar_estado_activo($id_ente, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_ente', $id_ente);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_ente,	 activo');
		$this->db->where('id_ente', $id_ente);
		$query = $this->db->get($this->tabla);
		
		$activo = null;

		$r = array();
		foreach ($query->result() as $ente) {
			$r = array('id_ente' => $ente->id_ente, 'activo' => $ente->activo);
		}

		return $r;
	}
}
?>