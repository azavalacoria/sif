<?php

/**
* 
*/
class Sesion extends CI_Model
{
	var $tabla = "catalogos_usuarios";
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_usuario($nombre_usuario, $contrasena)
	{
		$this->db->select('id_usuario, nombre_usuario, nombre_puesto');
		
		$this->db->from('catalogos_usuarios');
		$this->db->join('catalogos_puestos','id_puesto = puesto');
		//$this->db->join('nocomprometido_amigos','catalogos_usuarios_id_usuario = id_usuario');

		$this->db->where(array('nombre_usuario' => $nombre_usuario, 'contrasena' => sha1($contrasena)));
		$this->db->where($this->tabla.'.activo', TRUE);

		$query = $this->db->get();

		$datos = null;

		foreach ($query->result() as $usuario) {
			$datos = array(
					'id_usuario' => $usuario->id_usuario,
					'nombre_usuario' => $usuario->nombre_usuario,
					'puesto' => $usuario->nombre_puesto
				);
		}

		return $datos;
	}
}
?>