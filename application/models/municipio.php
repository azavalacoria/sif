<?php

/**
* 
*/
class Municipio extends CI_Model
{

	var $tabla = 'universoyucatan_municipios';
	
	function __construct()
	{
		parent::__construct();
	}

	public function enlistar_municipios_entidad($entidad_federativa)
	{
		$this->db->select('id_municipio, nombre_municipio');
		$this->db->where('entidad_federativa', $entidad_federativa);
		$query = $this->db->get($this->tabla);
		
		$municipios = array('' => 'Seleccione...');
		
		foreach ($query->result() as $municipio) {
			$municipios[$municipio->id_municipio] = $municipio->nombre_municipio;
		}
		return $municipios;
	}

	public function obtener_municipios_entidad($entidad_federativa)
	{
		$this->db->select('id_municipio, nombre_municipio');
		$this->db->where('entidad_federativa', $entidad_federativa);
		$query = $this->db->get($this->tabla);
		$municipios = array();
		array_push($municipios, array('id'=>'', 'nombre' => 'Seleccione...'));
		foreach ($query->result() as $municipio) {
			$m = array('id'=> $municipio->id_municipio, 'nombre'=> $municipio->nombre_municipio);
			array_push($municipios, $m);
		}
		return $municipios;
	}
}
?>