<?php

/**
* 
*/
class Secretaria extends CI_Model
{
	var $tabla = 'universoyucatan_secretarias';
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_nueva_secretaria($datos)
	{
		$this->db->insert($this->tabla, $datos);
		return $this->db->affected_rows();
	}

	public function buscar_secretaria($nombre_secretaria)
	{
		$this->db->select('id_secretaria, nombre_secretaria');
		$this->db->like('nombre_secretaria', $nombre_secretaria, 'after');
		$this->db->order_by('nombre_secretaria', 'ASC');
		$this->db->where('activo', TRUE);

		$query = $this->db->get($this->tabla);

		$secretarias = array();

		foreach ($query->result() as $s) {
			$secretaria = array('id' => $s->id_secretaria, 'nombre' => $s->nombre_secretaria);
			array_push($secretarias, $secretaria);
		}

		return $secretarias;
	}

	public function obtener_secretaria($id_organizacion)
	{
		$this->db->select('id_secretaria, nombre_secretaria');
		$this->db->where('id_secretaria', $id_organizacion);
		$this->db->where('activo', TRUE);
		
		$query = $this->db->get($this->tabla);

		$secretarias = array();

		foreach ($query->result() as $s) {
			$secretaria = array('id' => $s->id_secretaria, 'nombre' => $s->nombre_secretaria);
			array_push($secretarias, $secretaria);
		}

		return $secretarias;
	}


	public function listar_secretarias($inicio, $limite)
	{
		$this->db->select('id_secretaria, nombre_secretaria, siglas, direccion, activo');
		$this->db->order_by('nombre_secretaria', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get($this->tabla);

		$secretarias = array();

		foreach ($query->result() as $s) {
			$secretaria = array(
					'id_secretaria' => $s->id_secretaria,
					'nombre' => $s->nombre_secretaria,
					'siglas' => $s->siglas,
					'direccion' => $s->direccion,
					'activo' => $s->activo
				);
			array_push($secretarias, $secretaria);
		}

		return $secretarias;
	}

	public function obtener_secretaria_modificacion($id_secretaria)
	{
		$this->db->select('id_secretaria, nombre_secretaria, siglas, direccion, entidad_federativa, municipio, localidad');
		$this->db->where('id_secretaria', $id_secretaria);
		$query = $this->db->get($this->tabla);

		$secretaria = null;

		foreach ($query->result() as $s) {
			$secretaria = array(
					'id_secretaria' => $s->id_secretaria,
					'nombre' => $s->nombre_secretaria,
					'siglas' => $s->siglas,
					'direccion' => $s->direccion,
					'entidad_federativa' => $s->entidad_federativa,
					'municipio' => $s->municipio,
					'localidad' => $s->localidad
				);
		}

		return $secretaria;
	}

	public function verificar_datos_repetidos($datos)
	{
		$this->db->select('id_secretaria');
		$this->db->from($this->tabla);
		$this->db->where($datos);

		return $this->db->count_all_results();
	}

	public function actualizar_secretaria($id_secretaria, $datos)
	{
		$this->db->where('id_secretaria', $id_secretaria);
		$this->db->update($this->tabla, $datos);
		return $this->db->affected_rows();
	}

	public function cambiar_estado_activo($id_secretaria, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_secretaria', $id_secretaria);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_secretaria, activo');
		$this->db->where('id_secretaria', $id_secretaria);
		$query = $this->db->get($this->tabla);
		
		$activo = null;

		$secretaria = array();
		foreach ($query->result() as $s) {
			$secretaria = array('id_secretaria' => $s->id_secretaria, 'activo' => $s->activo);
		}

		return $secretaria;
	}
}
?>