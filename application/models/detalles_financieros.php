<?php

/**
* 
*/
class Detalles_Financieros extends CI_Model
{

	var $tabla = 'siri_conceptos_detalles_financieros';
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_detalles_financieros($datos)
	{
		try {
			$finanzas = array_map('strtoupper', $datos);
			$this->db->insert($this->tabla, $datos);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);
		}
	}
}
?>