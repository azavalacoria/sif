<?php

/**
* 
*/
class Entidad extends CI_Model
{
	var $tabla = 'universoyucatan_entidades_federativas';
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_todas_entidades()
	{
		$this->db->select('id_entidad_federativa, nombre_entidad');
		$query = $this->db->get($this->tabla);
		$entidades = array('' => 'Seleccione...');

		foreach ($query->result() as $entidad) {
			$entidades[$entidad->id_entidad_federativa] = $entidad->nombre_entidad;
		}

		return $entidades;
	}
}
?>