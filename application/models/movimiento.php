<?php

/**
* 
*/
class Movimiento extends CI_Model
{
	var $tabla = 'siri_movimientos';
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_nuevo_movimiento($datos)
	{
		$t = strlen($datos['gasto_comprobar']);

		if ($t == 0) {
			$datos['gasto_comprobar'] = null;
		}
		
		try {
			$movimiento = array_map('strtoupper', $datos);
			$this->db->insert($this->tabla, $movimiento);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);
		}
		
	}

	public function listar_movimientos($inicio, $limite)
	{
		$this->db->select('id_pago, date(fecha_pago) as fecha, nombre_concepto, monto, nombre_tipo_cuenta');
		$this->db->from($this->tabla);
		$this->db->join('siri_conceptos', 'id_concepto = concepto');
		$this->db->join('catalogos_tipos_cuentas', 'siri_movimientos.tipo_cuenta = id_tipo_cuenta');

		$this->db->order_by('fecha', 'DESC');

		$this->db->limit($limite, $inicio);

		$query = $this->db->get();

		$movimientos = array();

		foreach ($query->result() as $m) {
			$movimiento = array(
									'id_movimiento' => $m->id_pago,
									'fecha' => $m->fecha,
									'concepto' => $m->nombre_concepto,
									'monto' => $m->monto,
									'cuenta' => $m->nombre_tipo_cuenta
								);
			array_push($movimientos, $movimiento);
		}
		return $movimientos;
	}

	public function listar_gastos_comprobar($inicio, $limite)
	{
		$this->db->select('id_pago, date(fecha_pago) as fecha, nombre_concepto, monto, nombre_tipo_cuenta');
		$this->db->from($this->tabla);
		$this->db->join('siri_conceptos', 'id_concepto = concepto');
		$this->db->join('catalogos_tipos_cuentas', 'siri_movimientos.tipo_cuenta = id_tipo_cuenta');
		$this->db->where('gasto_comprobar', 1);
		$this->db->order_by('fecha', 'DESC');

		$this->db->limit($limite, $inicio);

		$query = $this->db->get();

		$movimientos = array();

		foreach ($query->result() as $m) {
			$movimiento = array(
									'id_movimiento' => $m->id_pago,
									'fecha' => $m->fecha,
									'concepto' => $m->nombre_concepto,
									'monto' => $m->monto,
									'cuenta' => $m->nombre_tipo_cuenta
								);
			array_push($movimientos, $movimiento);
		}
		return $movimientos;
	}
}
?>