<?php

/**
* 
*/
class Estado_Proyecto extends CI_Model
{
	var $tabla = 'siri_estados_concepto';
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_nuevo_estado($datos)
	{
		try {
			$estado = array_map('strtoupper', $datos);
			$this->db->insert($this->tabla, $estado);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			$error = array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);
			return $error;
		}
	}

	public function obtener_estado_para_modificacion($id_estado_concepto)
	{
		try {
			$this->db->select('id_estado_concepto, nombre_estado_concepto');
			$this->db->where('id_estado_concepto', $id_estado_concepto);
			$query = $this->db->get($this->tabla);
			$estado = null;

			foreach ($query->result_array() as $resultado) {
				$estado = $resultado;
			}

			return $estado;
		} catch (Exception $e) {
			return null;
		}
	}

	public function obtener_todos_estados()
	{
		$this->db->select('id_estado_concepto, nombre_estado_concepto');
		$this->db->where('activo', TRUE);
		$query = $this->db->get($this->tabla);
		$estados = array('' => 'Seleccione...');
		foreach ($query->result() as $e) {
			$estados[$e->id_estado_concepto] = $e->nombre_estado_concepto;
		}
		return $estados;
	}

	public function listar_estados_concepto($inicio, $limite)
	{
		$this->db->select('id_estado_concepto, nombre_estado_concepto, activo');
		$this->db->order_by('nombre_estado_concepto', 'ASC');
		$this->db->order_by('activo', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get($this->tabla);

		$formas = array();

		foreach ($query->result_array() as $forma) {
			array_push($formas, $forma);
		}
		return $formas;
	}

	public function verificar_repetido($id_estado_concepto, $nombre_estado_concepto)
	{
		try {
			$this->db->select('id_estado_concepto');
			$this->db->from($this->tabla);
			$this->db->where('nombre_estado_concepto', $nombre_estado_concepto);
			$this->db->where('id_estado_concepto !=', $id_estado_concepto);

			return $this->db->count_all_results();
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);	
		}
		
	}

	public function actualizar_estado_concepto($datos, $id_estado_concepto)
	{
		try {
			$estado = array_map('strtoupper', $datos);
			$this->db->where('id_estado_concepto', $id_estado_concepto);
			$this->db->update($this->tabla, $estado);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);		
		}
	}

	public function cambiar_estado_activo($id_estado_concepto, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_estado_concepto', $id_estado_concepto);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_estado_concepto, activo');
		$this->db->where('id_estado_concepto', $id_estado_concepto);
		$query = $this->db->get($this->tabla);

		$estado = array();
		foreach ($query->result() as $b) {
			$estado = $b;
		}

		return $estado;
	}
}
?>