<?php

/**
* 
*/
class Detalles_Concepto extends CI_Model
{
	var $tabla = "siri_conceptos_detalles";

	function __construct()
	{
		parent::__construct();
	}

	public function agregar_detalles_concepto($datos)
	{
		try {
			$detalles = array_map('strtoupper', $datos);
			$this->db->insert($this->tabla, $detalles);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);	
		}
	}
}
?>