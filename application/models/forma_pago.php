<?php

/**
* 
*/
class Forma_Pago extends CI_Model
{
	var $tabla = 'catalogos_formas_pago';

	function __construct()
	{
		parent::__construct();
	}

	public function obtener_todas_formas_pago()
	{
		$this->db->select('id_forma_pago, nombre_forma_pago');
		$this->db->where('activo', TRUE);
		$this->db->order_by('nombre_forma_pago', 'ASC');
		$query = $this->db->get($this->tabla);

		$formas = array('' => 'Seleccione...');

		foreach ($query->result() as $f) {
			$formas[$f->id_forma_pago] = $f->nombre_forma_pago;
		}

		return $formas;
	}

	public function agregar_nueva_forma_pago($datos)
	{
		try {
			$forma = array_map('strtoupper', $datos);
			$this->db->insert($this->tabla, $forma);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);
		}
		
	}

	public function listar_formas_pago($inicio, $limite)
	{
		$this->db->select('id_forma_pago, nombre_forma_pago, nombre_boveda,'.$this->tabla.'.activo');
		$this->db->from($this->tabla);
		$this->db->join('siri_bovedas', 'tipo_boveda = id_tipo_boveda');
		$this->db->order_by('nombre_forma_pago', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get();

		$formas = array();

		foreach ($query->result_array() as $forma) {
			array_push($formas, $forma);
		}
		return $formas;
	}

	public function obtener_forma_pago($id_forma_pago)
	{
		$this->db->select('id_forma_pago, nombre_forma_pago, tipo_boveda');
		$this->db->where('id_forma_pago', $id_forma_pago);
		$query = $this->db->get($this->tabla);

		$forma = array();

		foreach ($query->result_array() as $f) {
			$forma = $f;
		}
		return $forma;
	}

	public function comprobar_registro_repetido($datos)
	{
		$this->db->select('id_forma_pago');
		$this->db->from($this->tabla);
		$this->db->where($datos);
		return $this->db->count_all_results();
	}

	public function actualizar_forma_pago($datos, $id_forma_pago)
	{
		$this->db->where('id_forma_pago', $id_forma_pago);
		$this->db->update($this->tabla, $datos);
		return $this->db->affected_rows();
	}

	public function obtener_id_boveda($id_forma_pago)
	{
		$this->db->select('tipo_boveda');
		$this->db->where('id_forma_pago', $id_forma_pago);
		$query = $this->db->get($this->tabla);

		$id = 0;

		foreach ($query->result() as $boveda) {
			$id = $boveda->tipo_boveda;
		}
		return $id;
	}

	public function cambiar_estado_activo($id_forma_pago, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_forma_pago', $id_forma_pago);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_forma_pago, activo');
		$this->db->where('id_forma_pago', $id_forma_pago);
		$query = $this->db->get($this->tabla);
		
		$activo = null;

		$forma = array();
		foreach ($query->result() as $f) {
			$forma = $f;
		}

		return $forma;
	}
}
?>