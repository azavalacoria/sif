<?php

/**
* 
*/
class Amigo extends CI_Model
{
	var $tabla = 'nocomprometido_amigos';
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_nuevo_amigo($datos)
	{
		try {
			$this->db->insert($this->tabla, $datos);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			return 0;
		}
		
	}

	public function listar_amigos($inicio, $limite)
	{
		$this->db->select(
			'id_amigo, nombres, apellido_paterno, apellido_materno, nickname, nombre_secretaria, '.$this->tabla.'.activo'
			);
		$this->db->from($this->tabla);
		$this->db->join('catalogos_personas', 'persona = id_persona');
		$this->db->join('universoyucatan_secretarias', 'secretaria_amigo = id_secretaria');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get();

		$amigos = array();

		foreach ($query->result() as $amigo) {
			$a = array(
					'id_amigo' => $amigo->id_amigo,
					'nombre' => $amigo->nombres.' '.$amigo->apellido_paterno.' '.$amigo->apellido_materno,
					'nickname' => $amigo->nickname,
					'activo' => $amigo->activo,
					'nombre_secretaria' => $amigo->nombre_secretaria
				);
			array_push($amigos, $a);
		}

		return $amigos;
	}

	public function validar_amigo_repetido($datos)
	{
		$this->db->select('id_amigo');
		$this->db->from($this->tabla);
		$this->db->where($datos);

		return $this->db->count_all_results();
	}

	public function cambiar_estado_activo($id_amigo, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_amigo', $id_amigo);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_amigo, activo');
		$this->db->where('id_amigo', $id_amigo);
		$query = $this->db->get($this->tabla);

		$amigo = array();
		foreach ($query->result() as $b) {
			$boveda = $b;
		}

		return $amigo;
	}
}
?>