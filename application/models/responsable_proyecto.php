<?php

/**
* 
*/
class Responsable_Proyecto extends CI_Model
{
	var $tabla = 'siri_responsables_proyectos';
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_todos_responsables()
	{
		$this->db->select('id_responsable, nombres, apellido_paterno, apellido_materno, nombre_organizacion');
		$this->db->from($this->tabla);
		$this->db->join('catalogos_personas','persona = id_persona');
		$this->db->join('universoyucatan_organizaciones', 'organizacion = id_organizacion');
		$this->db->where($this->tabla.'.activo', TRUE);
		$query = $this->db->get();
		$responsables = array('' => 'Seleccione...');
		foreach ($query->result() as $responsable) {
			$nombre =  $responsable->nombres." ".$responsable->apellido_paterno." ".$responsable->apellido_materno;
			$organizacion = " (".$responsable->nombre_organizacion.")";
			$responsables[$responsable->id_responsable] = $nombre.$organizacion;
		}
		return $responsables;
	}

	public function obtener_empresas_responsables()
	{
		$this->db->select('id_responsable, nombre_organizacion');
		$this->db->from($this->tabla);
		$this->db->join('universoyucatan_organizaciones', 'organizacion = id_organizacion');
		$this->db->where($this->tabla.'.activo', TRUE);
		$query = $this->db->get();

		$responsables = array('' => 'Seleccione...');

		foreach ($query->result() as $responsable) {
			$responsables[$responsable->id_responsable] = strtoupper($responsable->nombre_organizacion);
		}
		return $responsables;
	}

	public function agregar_nuevo_responsable($datos)
	{
		$this->db->insert($this->tabla, $datos);
		return $this->db->affected_rows();
	}

	public function listar_responsables($inicio, $limite)
	{
		$this->db->select(
			'id_responsable, nombres, apellido_paterno, apellido_materno, nombre_organizacion, '.$this->tabla.'.activo');
		$this->db->from($this->tabla);
		$this->db->join('catalogos_personas','persona = id_persona');
		$this->db->join('universoyucatan_organizaciones', 'organizacion = id_organizacion');
		$this->db->order_by('apellido_paterno', 'ASC');
		$this->db->order_by('apellido_materno', 'ASC');
		$this->db->order_by('nombres', 'ASC');
		$this->db->order_by('nombre_organizacion', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get();
		$responsables = array();
		foreach ($query->result() as $r) {
			$nombre = $r->apellido_paterno." ".$r->apellido_materno." ".$r->nombres;
			$responsable = array(
					'id_responsable' => $r->id_responsable, 
					'nombre' => $nombre, 
					'organizacion' => $r->nombre_organizacion,
					'activo' => $r->activo
				);
			array_push($responsables, $responsable);
		}
		return $responsables;
	}

	public function obtener_responsable($id_responsable)
	{
		$responsables = null;
		try {
			$this->db->select('id_responsable, nombres, apellido_paterno, apellido_materno, nombre_organizacion, persona, organizacion ');
			$this->db->from($this->tabla);
			$this->db->join('catalogos_personas','persona = id_persona');
			$this->db->join('universoyucatan_organizaciones', 'organizacion = id_organizacion');
			$this->db->where('id_responsable', $id_responsable);
			$query = $this->db->get();
			$responsables = array();
			foreach ($query->result() as $r) {
				$nombre = $r->apellido_paterno." ".$r->apellido_materno." ".$r->nombres;
				$responsable = array(
						'id_responsable' => $r->id_responsable, 
						'persona' => $r->persona,
						'persona_nombre' => $nombre, 
						'organizacion' => $r->organizacion,
						'organizacion_nombre' => $r->nombre_organizacion
					);
				array_push($responsables, $responsable);
			}
		} catch (Exception $e) {
			$responsables = $e->getMessage();
		}
		return $responsables;
	}

	public function comprobar_registro_repetido($datos)
	{
		$this->db->select('id_responsable');
		$this->db->from($this->tabla);
		$this->db->where($datos);
		return $this->db->count_all_results();
	}

	public function actualizar_responsable_proyecto($datos, $id_responsable)
	{
		$this->db->where('id_responsable', $id_responsable);
		$this->db->update($this->tabla, $datos);

		return $this->db->affected_rows();
	}

	public function cambiar_estado_activo($id_responsable, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_responsable', $id_responsable);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_responsable,	 activo');
		$this->db->where('id_responsable', $id_responsable);
		$query = $this->db->get($this->tabla);
		
		$activo = null;

		$r = array();
		foreach ($query->result() as $responsable) {
			$r = array('id_responsable' => $responsable->id_responsable, 'activo' => $responsable->activo);
		}

		return $r;
	}
}
?>