<?php

/**
* 
*/
class Usuario extends CI_Model
{
	var $tabla = 'catalogos_usuarios';
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_nuevo_usuario($datos)
	{
		$this->db->insert($this->tabla, $datos);
		$afectadas= $this->db->affected_rows();
		return $afectadas;
	}

	public function listar_usuarios($inicio, $limite)
	{
		$this->db->select('id_usuario, nombres, apellido_paterno, apellido_materno, nombre_usuario,'.$this->tabla.'.activo');
		$this->db->from($this->tabla);
		$this->db->join('catalogos_personas','persona = id_persona');
		$this->db->order_by('apellido_paterno', 'ASC');
		$this->db->order_by('apellido_materno', 'ASC');
		$this->db->order_by('nombres', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get();
		$usuarios = array();
		foreach ($query->result() as $r) {
			$nombre = $r->apellido_paterno." ".$r->apellido_materno." ".$r->nombres;
			$usuario = array(
					'id_usuario' => $r->id_usuario, 
					'nombre' => $nombre, 
					'nombre_usuario' => $r->nombre_usuario,
					'activo' => $r->activo
				);
			array_push($usuarios, $usuario);
		}
		return $usuarios;
	}

	public function obtener_usuario_modificacion($id_usuario)
	{
		$this->db->select('id_usuario, nombre_usuario, puesto , persona, nombre_puesto, 
			nombres, apellido_paterno, apellido_materno');
		$this->db->from($this->tabla);
		$this->db->join('catalogos_personas', 'persona = id_persona');
		$this->db->join('catalogos_puestos', 'puesto = id_puesto');
		$this->db->where('id_usuario', $id_usuario);

		$query = $this->db->get();

		$persona = null;

		foreach ($query->result() as $p) {
			$persona = array(
					'id_usuario' => $p->id_usuario,
					'nombre_usuario' => $p->nombre_usuario,
					'puesto' => $p->puesto,
					'nombre_puesto' => $p->nombre_puesto,
					'persona' => $p->persona,
					'nombre_persona' => $p->nombres.' '.$p->apellido_paterno.' '.$p->apellido_materno
				);
		}
		return $persona;
	}

	public function actualizar_datos_usuario($datos, $id_usuario)
	{
		$this->db->where('id_usuario', $id_usuario);
		$afectadas = 0;
		try {
			$this->db->update($this->tabla, $datos);
			$afectadas = $this->db->affected_rows();
		} catch (Exception $e) {
			$afectadas = $e;etMessage();
		}
		
		return $afectadas;
	}

	public function cambiar_estado_activo($id_usuario, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_usuario', $id_usuario);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_usuario, activo');
		$this->db->where('id_usuario', $id_usuario);
		$query = $this->db->get($this->tabla);
		
		$activo = null;

		$usuario = array();
		foreach ($query->result() as $u) {
			$usuario = array('id_usuario' => $u->id_usuario, 'activo' => $u->activo);
		}

		return $usuario;
	}

	public function validar_usuario_repetido($datos)
	{
		$this->db->select('id_usuario');
		$this->db->from($this->tabla);
		$this->db->where($datos);

		return $this->db->count_all_results();
	}
}
?>