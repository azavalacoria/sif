<?php

/**
* 
*/
class Entrada extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_total_fecha_especifica($dias, $numero_registros)
	{
		$total_entrada = 0;
		$conceptos = array();
		foreach ($dias as $fecha) {
			$sql = 
				"SELECT date(fecha_pago) AS fecha, format(sum(monto), 2 ) AS total_entrada from siri_pagos  
				WHERE monto > 0 and date(fecha_pago) = '".$fecha."'";
			$query = $this->db->query($sql);
			foreach ($query->result() as $v) {
				if ($v->total_entrada > 1) {
					$total_entrada = $v->total_entrada;
				}
			}
			array_push($conceptos, array('fecha' => $fecha, 'entrada' => $total_entrada));
			$total_entrada = 0;
		}
		return $conceptos;
	}

	public function generar_dias($fecha , $numero_registros)
	{
		$dia = strtotime($fecha);
		for ($i=0; $i < $numero_registros; $i++) { 
			$dia = strtotime('last day', $dia);
			echo date('Y-m-d', $dia)."<br>";
		}
	}
}
?>