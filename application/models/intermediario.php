<?php

/**
* 
*/
class Intermediario extends CI_Model
{
	var $tabla = 'siri_intermediarios';
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_todos_intermediarios()
	{
		$this->db->select('id_intermediario, nombres, apellido_paterno, apellido_materno, nombre_organizacion');
		$this->db->from($this->tabla);
		$this->db->join('catalogos_personas','persona = id_persona');
		$this->db->join('universoyucatan_organizaciones', 'organizacion = id_organizacion');
		$this->db->where($this->tabla.'.activo', TRUE);
		$query = $this->db->get();
		$responsables = array('' => 'Seleccione...');
		foreach ($query->result() as $responsable) {
			$nombre = $responsable->nombres." ".$responsable->apellido_paterno." ".$responsable->apellido_materno;
			$organizacion = " (".$responsable->nombre_organizacion.")";
			$responsables[$responsable->id_intermediario] = $nombre.$organizacion;
		}
		return $responsables;
	}

	public function obtener_nombres_intermediarios()
	{
		$this->db->select('id_intermediario, nombres, apellido_paterno, apellido_materno');
		$this->db->from($this->tabla);
		$this->db->join('catalogos_personas','persona = id_persona');
		$this->db->where($this->tabla.'.activo', TRUE);
		$query = $this->db->get();

		$responsables = array('' => 'Seleccione...');

		foreach ($query->result() as $responsable) {
			$nombre = $responsable->nombres." ".$responsable->apellido_paterno." ".$responsable->apellido_materno;
			$responsables[$responsable->id_intermediario] = $nombre;
		}
		return $responsables;
	}

	public function agregar_nuevo_intermediario($datos)
	{
		$this->db->insert($this->tabla, $datos);
		return $this->db->affected_rows();
	}

	public function listar_intermediarios($inicio, $limite)
	{
		$this->db->select('id_intermediario, nombres, apellido_paterno, apellido_materno, 
			nombre_organizacion, '.$this->tabla.'.activo');
		$this->db->from($this->tabla);
		$this->db->join('catalogos_personas','persona = id_persona');
		$this->db->join('universoyucatan_organizaciones', 'organizacion = id_organizacion');
		$this->db->order_by('apellido_paterno', 'ASC');
		$this->db->order_by('apellido_materno', 'ASC');
		$this->db->order_by('nombres', 'ASC');
		$this->db->order_by('nombre_organizacion', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get();
		$intermediarios = array();
		foreach ($query->result() as $r) {
			$nombre = $r->apellido_paterno." ".$r->apellido_materno." ".$r->nombres;
			$intermediario = array(
					'id_intermediario' => $r->id_intermediario, 
					'nombre' => $nombre, 
					'organizacion' => $r->nombre_organizacion,
					'activo' => $r->activo
				);
			array_push($intermediarios, $intermediario);
		}
		return $intermediarios;
	}

	public function obtener_intermediario($id_intermediario)
	{
		$this->db->select('id_intermediario, nombres, apellido_paterno, apellido_materno, nombre_organizacion, persona, organizacion');
		$this->db->from($this->tabla);
		$this->db->join('catalogos_personas','persona = id_persona');
		$this->db->join('universoyucatan_organizaciones', 'organizacion = id_organizacion');
		$this->db->order_by('apellido_paterno', 'ASC');
		$this->db->order_by('apellido_materno', 'ASC');
		$this->db->order_by('nombres', 'ASC');
		$this->db->order_by('nombre_organizacion', 'ASC');
		$this->db->where('id_intermediario', $id_intermediario);
		$this->db->where($this->tabla.'.activo', TRUE);
		$query = $this->db->get();
		$intermediarios = array();
		foreach ($query->result() as $r) {
			$nombre = $r->apellido_paterno." ".$r->apellido_materno." ".$r->nombres;
			$intermediario = array(
					'id_intermediario' => $r->id_intermediario,
					'persona' => $r->persona,
					'persona_nombre' => $nombre,
					'organizacion' => $r->organizacion,
					'organizacion_nombre' => $r->nombre_organizacion
				);
			array_push($intermediarios, $intermediario);
		}
		return $intermediarios;
	}

	public function comprobar_registro_repetido($datos)
	{
		$this->db->select('id_intermediario');
		$this->db->from($this->tabla);
		$this->db->where($datos);
		return $this->db->count_all_results();
	}

	public function actualizar_intermediario($datos, $id_intermediario)
	{
		$this->db->where('id_intermediario', $id_intermediario);
		$this->db->update($this->tabla, $datos);

		return $this->db->affected_rows();
	}

	public function cambiar_estado_activo($id_intermediario, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_intermediario', $id_intermediario);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_intermediario, activo');
		$this->db->where('id_intermediario', $id_intermediario);
		$query = $this->db->get($this->tabla);

		$intermediarios = array();
		foreach ($query->result() as $i) {
			$intermediario = array('id_intermediario' => $i->id_intermediario, 'activo' => $i->activo);
		}

		return $intermediario;
	}

}
?>