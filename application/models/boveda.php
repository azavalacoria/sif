<?php


/**
* 
*/
class Boveda extends CI_Model
{
	var $tabla = 'siri_bovedas';
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_nuevo_tipo_boveda($datos)
	{
		try {
			$boveda = array_map('strtoupper', $datos);
			$this->db->insert($this->tabla, $boveda);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);
		}
	}

	public function obtener_todas_bovedas()
	{
		$this->db->select('id_tipo_boveda, nombre_boveda');
		$this->db->where('activo', TRUE);
		$query = $this->db->get($this->tabla);

		$bovedas = array('' => 'Seleccione...');

		foreach ($query->result() as $b) {
			$bovedas[$b->id_tipo_boveda] = $b->nombre_boveda;
		}

		return $bovedas;
	}


	public function listar_bovedas($inicio, $limite)
	{
		$this->db->select('id_tipo_boveda, nombre_boveda, activo');
		$this->db->order_by('nombre_boveda', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get($this->tabla);

		$bovedas = array();

		foreach ($query->result_array() as $boveda) {
			array_push($bovedas, $boveda);
		}
		return $bovedas;
	}

	public function obtener_tipo_boveda($id_tipo_boveda)
	{
		$this->db->select('id_tipo_boveda, nombre_boveda');
		$this->db->where('id_tipo_boveda', $id_tipo_boveda);
		$this->db->where('activo', TRUE);
		$query = $this->db->get($this->tabla);

		$bovedas = array();

		foreach ($query->result_array() as $boveda) {
			array_push($bovedas, $boveda);
		}

		return $bovedas;
	}

	public function verificar_repetido($datos)
	{
		$this->db->select('id_tipo_boveda');
		$this->db->from($this->tabla);
		$this->db->where($datos);
		return $this->db->count_all_results();
	}

	public function actualizar_boveda($id_tipo_boveda, $datos)
	{
		try {
			$this->db->where('id_tipo_boveda', $id_tipo_boveda);
			$boveda = array_map('strtoupper', $datos);
			$this->db->update($this->tabla, $boveda);
			return $this->db->affected_rows();
			
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);
		}
	}

	public function agregar_y_regresar_boveda($datos)
	{
		try {
			$nueva = array_map('strtoupper', $datos);
			$this->db->insert($this->tabla, $nueva);

			$this->db->select('id_tipo_boveda');
			$this->db->where($datos);

			$query = $this->db->get($this->tabla);

			$id = 0;

			foreach ($query->result() as $boveda) {
				$id = $boveda->id_tipo_boveda;
			}

			return $id;
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);
		}
	}

	public function obtener_id_boveda($id_tipo_boveda)
	{
		$this->db->select('id_tipo_boveda');
		$this->db->where('id_tipo_boveda', $id_tipo_boveda);
		$query = $this->db->get($this->tabla);

		$id = 0;

		foreach ($query->result() as $boveda) {
			$id = $this->boveda->id_tipo_boveda;
		}
		return $id;
	}

	public function cambiar_estado_activo($id_tipo_boveda, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_tipo_boveda', $id_tipo_boveda);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_tipo_boveda, activo');
		$this->db->where('id_tipo_boveda', $id_tipo_boveda);
		$query = $this->db->get($this->tabla);

		$boveda = array();
		foreach ($query->result() as $b) {
			$boveda = $b;
		}

		return $boveda;
	}
}
?>