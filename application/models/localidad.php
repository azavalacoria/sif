<?php

/**
* 
*/
class Localidad extends CI_Model
{
	var $tabla ="universoyucatan_localidades";
	
	function __construct()
	{
		parent::__construct();
	}

	public function enlistar_localidades_municipio($municipio)
	{
		$this->db->select('id_localidad, nombre_localidad');
		$this->db->where('municipio', $municipio);
		$query = $this->db->get($this->tabla);

		$localidades = array('' => 'Seleccione...');
		
		foreach ($query->result() as $localidad) {
			$localidades[$localidad->id_localidad] = $localidad->nombre_localidad;
		}
		
		return $localidades;
	}

	public function obtener_localidades_municipio($municipio)
	{
		$this->db->select('id_localidad, nombre_localidad');
		$this->db->where('municipio', $municipio);
		$query = $this->db->get($this->tabla);
		$localidades = array(array('id' => '', 'nombre' => 'Seleccione...'));
		foreach ($query->result() as $localidad) {
			$l = array('id' => $localidad->id_localidad, 'nombre' => $localidad->nombre_localidad);
			array_push($localidades, $l);
		}
		return $localidades;
	}
}
?>