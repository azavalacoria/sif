<?php

/**
* 
*/
class Estado_Boveda extends CI_Model
{
	var $tabla = 'siri_bovedas_estados';

	function __construct()
	{
		parent::__construct();
	}

	public function agregar_estado($datos)
	{
		try {
			$estado = array_map('strtoupper', $datos);
			$this->db->insert($this->tabla, $estado);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);
		}
		
	}

	public function actualizar_saldo($tipo_boveda, $monto, $aumenta_capital)
	{
		$this->db->trans_start();

		$this->db->select('saldo_actual, ');
		$this->db->where('tipo_boveda', $tipo_boveda);

		$query = $this->db->get($this->tabla);

		$saldo = 0;

		foreach ($query->result() as $estado) {
			$saldo = $estado->saldo_actual;
		}

		if ($aumenta_capital == 1) {
			$nuevo_saldo = $saldo + $monto;
		} else {
			$nuevo_saldo = $saldo - $monto;
		}
		
		
		$datos = array('saldo_actual' => $nuevo_saldo);

		$this->db->where('tipo_boveda', $tipo_boveda);
		$this->db->update($this->tabla, $datos);
		$actualizado = $this->db->affected_rows();
		$this->db->trans_complete();
		
		
		return $actualizado;
	}
}
?>