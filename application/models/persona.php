<?php

/**
* 
*/
class Persona extends CI_Model
{
	var $tabla = "catalogos_personas";
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_nueva_persona($datos)
	{
		$persona = array_map("strtoupper", $datos);
		$this->db->insert($this->tabla, $persona);
		return $this->db->affected_rows();
	}

	public function obtener_todas_personas()
	{
		$this->db->select('id_persona, nombres, apellido_paterno, apellido_materno');
		$query = $this->db->get($this->tabla);
		$this->db->where('activo', TRUE);
		
		$personas = array();

		foreach ($query->result() as $p) {
			$personas[$p->id_persona] = "".$p->nombres.' '.$p->apellido_paterno.' '.$p->apellido_materno;
		}
		return $personas;
	}

	public function listar_personas($inicio, $limite)
	{
		$this->db->select('id_persona, nombres, apellido_paterno, apellido_materno');
		
		$this->db->order_by('apellido_paterno', 'ASC');
		$this->db->order_by('apellido_materno', 'ASC');
		$this->db->order_by('nombres', 'ASC');
		$this->db->limit($limite, $inicio);

		$query = $this->db->get($this->tabla);

		$personas = array();

		foreach ($query->result() as $p) {
			$nombre = $p->apellido_paterno.' '.$p->apellido_materno.' '.$p->nombres;
			$persona = array('id_persona' => $p->id_persona, 'nombres' => $nombre);
			array_push($personas, $persona);
		}
		return $personas;
	}

	public function obtener_persona($id_persona)
	{
		$this->db->select('id_persona, nombres, apellido_paterno, apellido_materno');
		$this->db->where('id_persona', $id_persona);
		$query = $this->db->get($this->tabla);

		$personas = array();

		foreach ($query->result() as $p) {
			array_push(
					$personas, 
					array(
						'id' => $p->id_persona, 
						'nombre' => $p->nombres.' '.$p->apellido_paterno.' '.$p->apellido_materno
						)
				);
		}
		return $personas;
	}

	public function buscar_personas($apellido_paterno, $apellido_materno, $nombres)
	{
		
		$this->db->select('id_persona, apellido_paterno, apellido_materno, nombres');
		$this->db->like('apellido_paterno', $apellido_paterno, 'after');
		if ($apellido_materno != null) {
			$this->db->like('apellido_materno', $apellido_materno, 'after');
		}
		if ($nombres != null) {
			$this->db->like('nombres', $nombres, 'after');
		}

		$query = $this->db->get($this->tabla);

		$personas = array();

		foreach ($query->result() as $p) {
			$persona = array(
					'id' => $p->id_persona,
					'apellidopaterno' => $p->apellido_paterno,
					'apellidomaterno' => $p->apellido_materno,
					'nombres' => $p->nombres
				);
			array_push($personas, $persona);
		}
		return $personas;
	}
}
?>