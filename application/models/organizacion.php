<?php

/**
* 
*/
class Organizacion extends CI_Model
{
	var $tabla = 'universoyucatan_organizaciones';
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_nueva_organizacion($datos)
	{
		$this->db->insert($this->tabla, $datos);
		return $this->db->affected_rows();
	}

	public function buscar_organizacion($nombre)
	{
		$this->db->select('id_organizacion, nombre_organizacion, siglas, descripcion, direccion');
		$this->db->like('nombre_organizacion', $nombre, 'after');
		$this->db->where('activo', TRUE);

		$query = $this->db->get($this->tabla);

		$organizaciones = array();

		foreach ($query->result() as $o) {
			$nombre = '';
			if ($o->siglas == '0' || $o->siglas == null) {
				$nombre = $o->nombre_organizacion;
				
			} else {
				$nombre = $o->nombre_organizacion.' ('.$o->siglas.')';
			}
			$organizacion = array(
						'id' => $o->id_organizacion,
						'nombre' => $nombre,
						'descripcion' => $o->descripcion,
						'direccion' => $o->direccion
					);
			array_push($organizaciones, $organizacion);
		}

		return $organizaciones;
	}

	public function obtener_organizacion($id_organizacion)
	{
		$this->db->select('id_organizacion, nombre_organizacion, siglas');
		$this->db->where('id_organizacion', $id_organizacion);
		$this->db->where('activo', TRUE);
		$query = $this->db->get($this->tabla);

		$organizaciones = array();

		foreach ($query->result() as $o) {
			$nombre = '';
			if ($o->siglas == '0' || $o->siglas == null) {
				$nombre = $o->nombre_organizacion;
				
			} else {
				$nombre = $o->nombre_organizacion.' ('.$o->siglas.')';
			}
			$organizacion = array(
						'id' => $o->id_organizacion,
						'nombre' => $nombre
					);
			array_push($organizaciones, $organizacion);
		}

		return $organizaciones;
	}

	public function listar_organizaciones($inicio, $limite)
	{
		$this->db->select('id_organizacion, nombre_organizacion, descripcion, siglas, direccion, activo');
		$this->db->order_by('nombre_organizacion', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get($this->tabla);

		$organizaciones = array();

		foreach ($query->result() as $o) {
			$organizacion = array(
					'id_organizacion' => $o->id_organizacion,
					'nombre' => $o->nombre_organizacion,
					'descripcion' => $o->descripcion,
					'siglas' => $o->siglas,
					'direccion' => $o->direccion,
					'activo' => $o->activo
				);
			array_push($organizaciones, $organizacion);
		}

		return $organizaciones;
	}

	public function obtener_organizacion_modificacion($id_organizacion)
	{
		$this->db->select('nombre_organizacion, descripcion, siglas, direccion, entidad_federativa, municipio, localidad');
		$this->db->where('id_organizacion', $id_organizacion);
		$query = $this->db->get($this->tabla);

		$organizacion = null;

		foreach ($query->result() as $o) {
			$organizacion = array(
					'id_organizacion' => $id_organizacion,
					'nombre' => $o->nombre_organizacion,
					'descripcion' => $o->descripcion,
					'siglas' => $o->siglas,
					'direccion' => $o->direccion,
					'entidad_federativa' => $o->entidad_federativa,
					'municipio' => $o->municipio,
					'localidad' => $o->localidad
				);
		}
		return $organizacion;
	}

	public function comprobar_registro_repetido($datos)
	{
		$this->db->select('id_organizacion');
		$this->db->from($this->tabla);
		$this->db->where($datos);
		return $this->db->count_all_results();
	}

	public function actualizar_datos($datos, $id_organizacion)
	{
		$this->db->where('id_organizacion', $id_organizacion);
		$this->db->update($this->tabla, $datos);
		return $this->db->affected_rows();
	}

	public function cambiar_estado_activo($id_organizacion, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_organizacion', $id_organizacion);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_organizacion, activo');
		$this->db->where('id_organizacion', $id_usuario);
		$query = $this->db->get($this->tabla);
		
		$activo = null;

		$organizacion = array();
		foreach ($query->result() as $o) {
			$organizacion = array('id_organizacion' => $o->id_organizacion, 'activo' => $o->activo);
		}

		return $organizacion;
	}
}
?>