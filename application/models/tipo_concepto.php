<?php

/**
* 
*/
class Tipo_Concepto extends CI_Model
{
	var $tabla = 'catalogos_tipos_concepto';
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_todos_tipos()
	{
		$this->db->select('id_tipo_concepto, nombre_tipo_concepto');
		$this->db->where('activo', TRUE);
		$query = $this->db->get($this->tabla);
		$tipos = array('' => 'Seleccione...');
		foreach ($query->result() as $tipo) {
			$tipos[$tipo->id_tipo_concepto] = $tipo->nombre_tipo_concepto;
		}
		return $tipos;
	}

	public function verificar_repetido_al_agregar($datos)
	{
		try {
			$this->db->select('id_tipo_concepto');
			$this->db->from($this->tabla);
			$this->db->where($datos);
			return $this->db->count_all_results();
		} catch (Exception $e) {
			$error = array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);
			return $error;
		}
	}
	public function agregar_nuevo_tipo_concepto($datos)
	{
		try {
			$tipo = array_map('strtoupper', $datos);
			$this->db->insert($this->tabla, $tipo);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);
		}
		
	}

	public function obtener_tipos_por_cuenta($tipo_cuenta)
	{
		$this->db->select('id_tipo_concepto, nombre_tipo_concepto');
		$this->db->where('tipo_cuenta', $tipo_cuenta);

		$query = $this->db->get($this->tabla);

		$tipos = array(array('id' => '', 'nombre' => 'Seleccione...'));

		foreach ($query->result() as $t) {
			$tipo = array('id'=> $t->id_tipo_concepto, 'nombre' => $t->nombre_tipo_concepto);
			array_push($tipos, $tipo);
		}

		return $tipos;
	}

	public function listar_tipos_concepto($inicio, $limite)
	{
		$this->db->select('id_tipo_concepto, nombre_tipo_concepto, activo');
		$this->db->order_by('nombre_tipo_concepto', 'ASC');
		$this->db->limit($limite, $inicio);
		$query = $this->db->get($this->tabla);
		$tipos = array();
		foreach ($query->result() as $tipo) {
			$tipo = array(
							'id_tipo_concepto' => $tipo->id_tipo_concepto,
							'nombre' => $tipo->nombre_tipo_concepto,
							'activo' => $tipo->activo
							);
			array_push($tipos, $tipo);
		}
		return $tipos;
	}

	public function cambiar_estado_activo($id_tipo_concepto, $activo)
	{
		$datos = array('activo' => $activo);
		$this->db->where('id_tipo_concepto', $id_tipo_concepto);
		$this->db->update($this->tabla, $datos);

		$this->db->select('id_tipo_concepto, activo');
		$this->db->where('id_tipo_concepto', $id_tipo_concepto);
		$query = $this->db->get($this->tabla);

		$tipo = array();
		foreach ($query->result() as $t) {
			$tipo = array('id_tipo_concepto' => $t->id_tipo_concepto, 'activo' => $t->activo);
		}

		return $tipo;
	}
}
?>