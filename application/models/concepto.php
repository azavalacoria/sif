<?php

/**
* 
*/
class Concepto extends CI_Model
{
	var $tabla = 'siri_conceptos';
	var $tabla_detalles = 'siri_conceptos_detalles';
	var $tabla_finanzas = 'siri_conceptos_detalles_financieros';

	function __construct()
	{
		parent::__construct();
	}

	public function agregar_nuevo_concepto($datos)
	{
		$concepto = array_map('strtoupper', $datos);
		try {
			$this->db->insert($this->tabla, $concepto);
			return $this->db->affected_rows();
		} catch (Exception $e) {
			return array(
					'mensaje' => $this->db->_error_message(),
					'codigo' => $this->db->_error_number(),
					'excepcion' => $e->getMessage()
				);
		}
		
	}
	
	public function obtener_id_concepto_creado($fecha_creacion)
	{
		$this->db->select('id_concepto');
		$this->db->where('fecha_creacion', $fecha_creacion);
		$query = $this->db->get($this->tabla);
		$id = 0;

		foreach ($query->result() as $c) {
			$id = $c->id_concepto;
		}

		return $id;
	}

	public function obtener_todos_conceptos()
	{
		$this->db->select('id_concepto, nombre_concepto');
		$this->db->order_by('nombre_concepto', 'ASC');
		$query = $this->db->get($this->tabla);

		$conceptos = array('' => 'Seleccione...');

		foreach ($query->result() as $c) {
			$conceptos[$c->id_concepto] = $c->nombre_concepto;
		}
		return $conceptos;
	}

	public function listar_todos_conceptos($inicio, $limite)
	{
		$this->db->select('id_concepto, nombre_concepto, costo_autorizado, monto_acordado, 
							apellido_paterno, apellido_materno, nombres');
		$this->db->from($this->tabla);
		$this->db->join($this->tabla_detalles, $this->tabla_detalles.'.concepto = id_concepto');
		$this->db->join($this->tabla_finanzas, $this->tabla_finanzas.'.concepto = id_concepto');
		$this->db->join('siri_responsables_proyectos', 'responsable_proyecto = id_responsable');
		$this->db->join('catalogos_personas', 'persona = id_persona');
		$this->db->where($this->tabla.'.activo', TRUE);
		$this->db->limit($limite, $inicio);

		$query = $this->db->get();

		$datos = array();

		foreach ($query->result() as $d) {
			$dato = array(
					'id_concepto' => $d->id_concepto,
					'nombre_concepto' => $d->nombre_concepto,
					'costo_autorizado' => $d->costo_autorizado,
					'monto_acordado' => $d->monto_acordado,
					'responsable' => ''.$d->nombres.' '.$d->apellido_paterno.' '.$d->apellido_materno
				);
			array_push($datos, $dato);
		}
		return $datos;
	}

	public function obtener_concepto_por_tipo_cuenta($tipo_cuenta)
	{

		$this->db->select('id_concepto, nombre_concepto');
		$this->db->where('tipo_cuenta', $tipo_cuenta);
		$this->db->where('activo', TRUE);
		$query = $this->db->get($this->tabla);

		$conceptos = array(array('id' => '', 'nombre' => 'Seleccione...'));

		foreach ($query->result() as $c) {
			$concepto = array('id' => $c->id_concepto, 'nombre' => $c->nombre_concepto);
			array_push($conceptos, $concepto);
		}
		return $conceptos;
		
	}

	public function listar_conceptos_por_tipo_cuenta($tipo_cuenta)
	{

		$this->db->select('id_concepto, nombre_concepto');
		$this->db->where('tipo_cuenta', $tipo_cuenta);
		$this->db->where('activo', TRUE);
		
		$query = $this->db->get($this->tabla);

		$conceptos = array('' => 'Seleccione...');

		foreach ($query->result() as $c) {
			$conceptos[$c->id_concepto] = $c->nombre_concepto;
		}
		return $conceptos;
		
	}
}
?>