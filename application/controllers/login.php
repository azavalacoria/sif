<?php

/**
* 
*/
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		$this->_cargar_vista();
	}

	public function iniciar()
	{
		$this->form_validation->set_rules('nickname','nombre de usuario','trim|required');
		$this->form_validation->set_rules('password','contraseña','trim|required');

		if ($this->form_validation->run()) {

			$nombre_usuario = $this->input->post('nickname');
			$contrasena = $this->input->post('password');

			$this->load->model('Sesion','sesion', TRUE);
			$datos = $this->sesion->obtener_usuario($nombre_usuario, $contrasena);

			$this->_validar_resultados($datos);
		} else {
			$this->_cargar_vista();
		}
		
	}

	private function _cargar_vista()
	{
		$this->load->view('header');
		$this->load->view('sesiones/login_inicio');
		
	}

	private function _validar_resultados($datos)
	{
		$tamanio = sizeof($datos);
		if ($tamanio > 0) {

			$this->session->set_userdata(
					array(
						'sesion_iniciada' => 1,
						'id_usuario' => $datos['id_usuario'],
						'nombre_usuario' => $datos['nombre_usuario'],
						'puesto' => $datos['puesto'],
						'amigo' => $datos['amigo']
						)
				);
			if ($datos['amigo'] == 1) {
				redirect(site_url('nocomprometido/listar_licitaciones'));
			} else {
				redirect('usuario');
			}
		} else {
			//$this->_cargar_vista();
			$urls = array(
				'Volver a intentar' => site_url('login')
				);
			$url_cerrar = site_url('login');
			$mensaje = $this->load->view('mensajes/ventana', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
			$datos = array('mensaje' => $mensaje);
			$this->load->view('header');
			$this->load->view('mensajes/ventana_error_login', $datos);
		}
		
	}

	public function limpiar()
	{
		$this->session->set_userdata(array('sesion_iniciada' => 0));
		$this->_cargar_vista();
	}

	public function salir_amigo()
	{
		$sesion = array(
					'sesion_amigo' => 0,
					'id_amigo' => 0,
					'persona' => 0,
					'secretaria' => 0
				);
		$this->session->set_userdata($sesion);
		redirect('amigo');
	}
}
?>