<?php

class Importar_Excel extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('importar_excel');
		
	}

	public function importar()
	{

		$name	  = $_FILES['file']['name'];
		$tname 	  = $_FILES['file']['tmp_name'];

		// Buscamos nuestra clase para leer el Excel
		require_once BASEPATH.'libraries/excel_reader2.php';
		// Instanciamos nuestra clase
		$dato = new Spreadsheet_Excel_Reader($tname);

		//print_r($dato);

		//Aqui vamos a leer el Excel, mostrandolo con un simple HTML
		$html = "<table cellpadding='2' border='1'>";
		for ($i = 1; $i <= $dato->rowcount($sheet_index=0); $i++) {

			//Verificamos que cada celda la fila no se encuentra vacia
			if($dato->val($i,2) != ''){
				$html .= "<tr>";
				// leemos columna por columna
				for ($j = 1; $j <= $dato->colcount($sheet_index=0); $j++) { 

					$value 	 = $dato->val($i,$j); 
					$html .="<td>".$value." </td>";

					// Aqui podemos insertar cada fila del excel en una tabla Mysql
				}
				$html .="</tr>";
			}
		}
		$html .="</table>";	

		echo $html;	
		//print_r($html);
	}
}

?>