<?php

/**
* 
*/
class Obtener_Personas extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$apellido_paterno = $this->_validar_espacio_blanco($this->input->post('apellido_paterno'));
		$apellido_materno = $this->_validar_espacio_blanco($this->input->post('apellido_materno'));
		$nombres = $this->_validar_espacio_blanco($this->input->post('nombres'));

		if ($apellido_paterno == null) {
			echo json_encode(array(
					'errores' => 1
				));
		} else {
			$personas = $this->_buscar_personas($apellido_paterno, $apellido_materno, $nombres);
			echo json_encode(array(
					'errores' => 0,
					'resultados' => sizeof($personas),
					'personas' => $personas
				));
		}
	}

	public function especifica()
	{
		$id_persona = $this->_validar_espacio_blanco($this->input->post('id_persona'));
		
		if ($id_persona == 0) {
			echo json_encode(array(
					'errores' => 1
				));
		} else {
			$personas = $this->_obtener_persona($id_persona);
			echo json_encode(array(
					'errores' => 0,
					'resultados' => sizeof($personas),
					'personas' => $personas
				));
		}
	}	

	private function _buscar_personas($apellido_paterno, $apellido_materno, $nombres)
	{
		$this->load->model('Persona', 'persona', TRUE);
		return $this->persona->buscar_personas($apellido_paterno, $apellido_materno, $nombres);
	}

	private function _obtener_persona($id)
	{
		$this->load->model('Persona', 'persona', TRUE);
		return $this->persona->obtener_persona($id);
	}

	private function _validar_espacio_blanco($valor)
	{
		$valor = trim($valor);

		if (!isset($valor) || $valor == '') {
			$valor = null;
		}
		return $valor;
		
	}
}
?>