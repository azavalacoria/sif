<?php

/**
* 
*/
class Obtener_Datos_Cuenta extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		echo json_encode(array('hola'));
	}

	public function aumenta_capital()
	{
		$id_tipo_cuenta = $this->input->post('cuenta');

		if ($id_tipo_cuenta != 0) {
			$this->load->model('Tipo_Cuenta', 'cuenta', TRUE );
			$aumenta = $this->cuenta->aumenta_capital($id_tipo_cuenta);
			echo json_encode(array('errores' => 0, 'aumenta_capital' => $aumenta));
		} else {
			echo json_encode(array('errores' => 1));
		}
		
		
	}
}
?>