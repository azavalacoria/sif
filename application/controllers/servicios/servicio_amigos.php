<?php

/**
* 
*/
class Servicio_Amigos extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function validar_amigo_repetido()
	{
		$datos = array(
						'nickname' => $this->input->post('nickname'),
						'persona' => $this->input->post('persona'),
						'secretaria_amigo' => $this->input->post('secretaria')
					);

		$this->load->model('Amigo','amigo',true);
		$repetido = $this->amigo->validar_amigo_repetido($datos);
		echo json_encode(
				array('repetido' => $repetido)
			);
	}
}
?>