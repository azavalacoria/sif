<?php

/**
* 
*/
class Obtener_Tipos_Concepto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$cuenta = $this->input->post('cuenta');

		if (isset($cuenta) && $cuenta > 0) {
			$this->load->model('Tipo_Concepto', 'tipos', TRUE);
			$tipos = $this->tipos->obtener_tipos_por_cuenta($cuenta);
			$datos = array( 'errores' => 0,'tipos' => $tipos);
			echo json_encode($datos);
		} else {
			$datos = array( 'errores' => 0);
			echo json_encode($datos);
		}
	}

	public function aumenta_capital()
	{
		$id_tipo_cuenta = $this->input->post('cuenta');

		if ($id_tipo_cuenta != 0) {
			$this->load->model('Tipo_Cuenta', 'cuenta', TRUE );
			$aumenta = $this->cuenta->aumenta_capital($id_tipo_cuenta);
			if ($aumenta == 0) {
				$destino = site_url('agregar_nuevo_concepto_salida');
			} else {
				$destino = site_url('agregar_nuevo_concepto');
			}
			
			echo json_encode(array('errores' => 0, 'aumenta_capital' => $aumenta, 'destino' => $destino) ); 
		} else {
			echo json_encode(array('errores' => 1));
		}
	}
}
?>