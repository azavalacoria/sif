<?php

/**
* 
*/
class Servicio_Usuarios extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function validar_usuario_repetido()
	{
		$nombre_usuario = $this->input->post('nombre_usuario');

		$datos = array('nombre_usuario' => $nombre_usuario);

		$this->load->model('Usuario','usuario',true);
		$repetido = $this->usuario->validar_usuario_repetido($datos);
		echo json_encode(
				array('repetido' => $repetido)
			);
	}
}
?>