<?php

/**
* 
*/
class Servicio_Estados_Concepto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function modificar_estado()
	{
		$id_estado_concepto = $this->input->post('id_estado_concepto');
		$nombre_estado_concepto = strtoupper($this->input->post('nombre_estado_concepto'));

		$this->load->model('Estado_Proyecto', 'estado', TRUE);
		$repetido = $this->estado->verificar_repetido($id_estado_concepto, $nombre_estado_concepto);

		if (is_array($repetido)) {
			$datos = array(
				'errores' => 1,
				'mensaje' => $repetido['mensaje']
				);
		} else {
			$datos = array(
				'errores' => 0,
				'repetido' => $repetido
				);
		}
		echo json_encode($datos);
	}
}

?>