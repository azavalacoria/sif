<?php

/**
* 
*/
class Cambiar_Estado_Elemento extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$modelo = $this->input->post('modelo');
		$id_elemento = $this->input->post('id_elemento');
		$activo = $this->input->post('activo');


		$this->load->model($modelo, 'modelo', TRUE);
		$this->modelo->cambiar_estado_activo($id_elemento, $activo);
		$elemento = array('id_elemento' => $id_elemento, 'activo' => $activo);
		echo json_encode(array('errores' => 0,'elemento' => $elemento));
	}

}
?>