<?php
/**
* 
*/
class Obtener_Municipios extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$id_entidad = $this->input->post('id_entidad');
		if ($id_entidad > 0) {
			$this->load->model('Municipio', '', TRUE);
			$municipios = $this->Municipio->obtener_municipios_entidad($id_entidad);
			if (sizeof($municipios) > 0) {
				$datos = array('errores' => 0, 'municipios' => $municipios);
				$this->_generar_json($datos);
			} else {
				$datos = $this->_generar_error('La entidad federativa no posee municipios');
				$this->_generar_json($datos);
			}
			
		} else {
			$datos = $this->_generar_error('La entidad federativa no se encuentra en el listado');
			$this->_generar_json($datos);
		}
	}

	public function obtener_localidad()
	{
		$municipio = $this->input->post('municipio');
		if ($municipio > 0) {
			$this->load->model('Localidad', '', TRUE);
			$localidades = $this->Localidad->obtener_localidades_municipio($municipio);
			if (sizeof($localidades) > 0) {
				$datos = array('errores' => 0, 'localidades' => $localidades);
				$this->_generar_json($datos);
			} else {
				$datos = $this->_generar_error('La municipio no posee localidades');
				$this->_generar_json($datos);
			}
			
		} else {
			$datos = $this->_generar_error('La localidad no se encuentra en el listado');
			$this->_generar_json($datos);
		}
	}

	private function _generar_error($mensaje)
	{
		return array('errores' => 1, 'mensaje' => $mensaje);
	}

	private function _generar_json($datos)
	{
		echo json_encode($datos);
	}
}
?>