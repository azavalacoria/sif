<?php

/**
* 
*/
class Obtener_Secretarias extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

		$nombre = $this->input->post('nombre');
		$this->load->model('Secretaria', 'secretaria', TRUE);
		$secretarias = $this->secretaria->buscar_secretaria($nombre);
		//$secretarias = array(array('id_secretaria'=> 1, 'nombre'=>'nombre'));
		echo json_encode(array('errores'=>0, 'secretarias' => $secretarias));
	}

	public function especifica()
	{
		$id_secretaria = $this->input->post('id');
		if ($id_secretaria != 0) {
			$this->load->model('Secretaria', 'secretaria', TRUE);
			$secretaria = $this->secretaria->obtener_secretaria($id_secretaria);
			$datos = array('errores' => 0, 'secretaria' => $secretaria);
			echo json_encode($datos);
		} else {
			echo json_encode(array('erroresorg'=>1, 'valor' => $id_secretaria));
		}
		
	}
}
?>