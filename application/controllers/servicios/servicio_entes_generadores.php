<?php

/**
* 
*/
class Servicio_Entes_Generadores extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function validar_ente_repetido()
	{
		$datos = array(
				'activo' => 1,
				'titular' => $this->input->post('persona'),
				'secretaria' => $this->input->post('secretaria')
			);

		$this->load->model('Ente_Generador', 'ente', TRUE);

		$repetido = $this->ente->validar_ente_repetido($datos);

		echo json_encode(array('repetido' => $repetido));
	}
}
?>