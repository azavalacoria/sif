<?php

/**
* 
*/
class Servicio_Tipos_Concepto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function verificar_repetido_al_agregar()
	{
		$nombre_tipo_concepto = strtoupper($this->input->post('nombre_concepto'));
		$descripcion_tipo_concepto = strtoupper($this->input->post('descripcion_concepto'));
		$tipo_cuenta = $this->input->post('tipo_cuenta');

		$tipo = array(
				'nombre_tipo_concepto' => $nombre_tipo_concepto,
				'descripcion_tipo_concepto' => $descripcion_tipo_concepto,
				'tipo_cuenta' => $tipo_cuenta
			);

		$this->load->model('Tipo_Concepto', 'tipo', TRUE);
		$repetido = $this->tipo->verificar_repetido_al_agregar($tipo);

		if (is_array($repetido)) {
			$datos = array(
				'errores' => 1,
				'mensaje' => $repetido['mensaje']
				);
		} else {
			$datos = array(
				'errores' => 0,
				'repetido' => $repetido
				);
		}
		echo json_encode($datos);
	}
}

?>