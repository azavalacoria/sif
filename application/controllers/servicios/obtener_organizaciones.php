<?php

/**
* 
*/
class Obtener_Organizaciones extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

		$nombre = $this->input->post('nombre');

		$this->load->model('Organizacion', 'organizacion', TRUE);
		$organizaciones = $this->organizacion->buscar_organizacion($nombre);
		echo json_encode(array('errores'=>0, 'organizaciones' => $organizaciones));
	}

	public function especifica()
	{
		$id_organizacion = $this->input->post('id');
		if ($id_organizacion != 0) {
			$this->load->model('Organizacion', 'organizacion', TRUE);
			$organizacion = $this->organizacion->obtener_organizacion($id_organizacion);
			$datos = array('errores' => 0, 'organizacion' => $organizacion);
			echo json_encode($datos);
		} else {
			echo json_encode(array('erroresorg'=>1, 'valor' => $id_organizacion));
		}
		
	}
}
?>