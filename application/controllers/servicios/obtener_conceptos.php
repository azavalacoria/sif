<?php

/**
* 
*/
class Obtener_Conceptos extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_concepto_por_cuenta()
	{
		$cuenta = $this->input->post('cuenta');

		if ($cuenta > 0) {
			
			$this->load->model('Concepto', 'concepto', TRUE);
			$conceptos = $this->concepto->obtener_concepto_por_tipo_cuenta($cuenta);
			echo json_encode(array('errores' => 0, 'conceptos' => $conceptos, 'cuenta' => $cuenta));
		} else {
			echo json_encode(array('errores' => 1));
		}
		
	}
}
?>