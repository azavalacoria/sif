<?php
/**
* 
*/
class Prueba_Formato_Numero extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('pruebas/formato_numero');
	}

	public function mostrar()
	{
		$numero = str_replace(',', '', $this->input->post('numero'));
		echo $numero;
	}
}
?>