<?php

/**
* 
*/
class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$sesion = $this->session->userdata('sesion_iniciada');

		switch ($sesion) {
			case 1:
				$this->load->view('inicio');
				break;
			
			default:
				redirect('login');
				break;
		}
		
		
	}
}
?>