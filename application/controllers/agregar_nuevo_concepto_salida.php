<?php

/**
* 
*/
class Agregar_Nuevo_Concepto_Salida extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->form_validation->set_rules('nombre_gasto', 'nombre del concepto de salida', 'trim|required');
		$this->form_validation->set_rules('descripcion_gasto', 'descripcion del concepto de salida', 'trim|required');
		$this->form_validation->set_rules('tipo_cuenta', 'tipo de cuenta', 'trim|required');
		$this->form_validation->set_rules('tipo_concepto_gasto', 'tipo de concepto', 'trim|required');
		$this->form_validation->set_rules('estado_gasto', 'estado del concepto', 'trim|required');

		if ($this->form_validation->run()) {
			date_default_timezone_set('America/Merida');
			$fecha_creacion = date('Y-m-d H:i:s');
			$datos = array(
				'nombre_concepto' => $this->input->post('nombre_gasto'),
				'descripcion_concepto' => $this->input->post('descripcion_gasto'),
				'tipo_cuenta'  => $this->input->post('tipo_cuenta'),
				'tipo_concepto'=> $this->input->post('tipo_concepto_gasto'),
				'estado' => $this->input->post('estado_gasto'),
				'fecha_creacion' => $fecha_creacion
				);

			$this->_agregar_concepto_salida($datos);
		} else {
			$this->_cargar_vista();
		}
	}

	public function _agregar_concepto_salida($datos)
	{
		$this->load->model('Concepto', 'concepto', TRUE);
		$agregadas = $this->concepto->agregar_nuevo_concepto($datos);
		$url_cerrar = site_url('usuario');

		if ($agregadas == 0) {
			$urls = array(
					'Intentar con otro registro' => site_url('agregar_nuevo_concepto_salida'),
					'Volver al catálogo' => site_url('listados/listar_todos_conceptos')
				);
			$this->_imprimir_mensaje('mensajes/ventana_registro_fallido', $urls, $url_cerrar);
		} else {
			$urls = array(
				'Agregar nuevo registro' => site_url('agregar_nuevo_concepto_salida'),
				'Volver al catálogo' => site_url('listados/listar_todos_conceptos')
				);
			$this->_imprimir_mensaje('mensajes/ventana_registro_exito', $urls, $url_cerrar);
		}
	}

	public function _imprimir_mensaje($vista_mensaje, $urls, $url_cerrar)
	{
		$direcciones = array('urls'=> $urls, 'url_cerrar' => $url_cerrar);
		$mensaje = $this->load->view($vista_mensaje, $direcciones, TRUE);
		$datos = array('mensaje' => $mensaje);
		$this->load->view('header');
		$this->load->view('mensajes/imprimir_mensaje', $datos);
	}
	private function _cargar_vista()
	{
		$this->load->model('Tipo_Cuenta', 'cuenta', TRUE);
		$cuentas = $this->cuenta->listar_tipos_cuenta_salida();

		$this->load->model('Forma_Pago','formas', TRUE);
		$formas = $this->formas->obtener_todas_formas_pago();

		$this->load->model('Tipo_Concepto','tipos',TRUE);
		$tipos = $this->tipos->obtener_todos_tipos();

		$this->load->model('Estado_Proyecto','estados', TRUE);
		$estados = $this->estados->obtener_todos_estados();

		$this->load->model('Entidad','',TRUE);
		$entidades = $this->Entidad->obtener_todas_entidades();

		$datos = array(
				'formas' => $formas,
				'tipos' => $tipos,
				'estados' => $estados,
				'entidades' => $entidades,
				'cuentas' => $cuentas,
				'url_servicio' => site_url('servicios/obtener_municipios'),
				'url_servicio_tipos_concepto' => site_url('servicios/obtener_tipos_concepto')
			);
		$this->load->view('header');
		$this->load->view('agregar_nuevo_concepto_salida', $datos);
	}
}
?>