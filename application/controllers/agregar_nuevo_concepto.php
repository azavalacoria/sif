<?php

/**
* 
*/
class Agregar_Nuevo_Concepto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->view('header');
	}

	public function index()
	{
		$this->form_validation->set_rules('nombre','nombre del concepto','trim|required');
		$this->form_validation->set_rules('descripcion','descripcion','trim|required');
		$this->form_validation->set_rules('tipo_cuenta','tipo de cuenta','trim|required');
		$this->form_validation->set_rules('tipo_concepto','tipo de concepto','trim|required');
		$this->form_validation->set_rules('duracion_meses','duracion en meses','trim|required');
		$this->form_validation->set_rules('costo_publicado','costo publicado','trim|required');
		$this->form_validation->set_rules('costo_autorizado','costo autorizado','trim|required');
		$this->form_validation->set_rules('costo_incrementado','costo incrementado','trim|required');
		$this->form_validation->set_rules('numero_pagos','numero de pagos','trim|required');
		$this->form_validation->set_rules('ente','ente','trim|required');
		$this->form_validation->set_rules('responsable','responsable','trim|required');
		$this->form_validation->set_rules('intermediario','intermediario','trim|required');
		$this->form_validation->set_rules('entidad_federativa','entidad federativa','trim|required');
		$this->form_validation->set_rules('localidad','localidad','trim|required');
		$this->form_validation->set_rules('municipio','municipio','trim|required');


		if ($this->form_validation->run()) {
			$datos_concepto = array(
					'nombre_concepto' => $this->input->post('nombre'),
					'descripcion_concepto' => $this->input->post('descripcion'),
					'fecha_creacion' => strftime("%Y-%m-%d %H:%M:%S"),
					'tipo_cuenta' => $this->input->post('tipo_cuenta'),
					'tipo_concepto' => $this->input->post('tipo_concepto'),
					'estado' => $this->input->post('estado')
				);
			$id = $this->_agregar_nuevo_concepto($datos_concepto);
			$datos_detalle = array(
					'concepto' => $id,
					'duracion_en_meses' => $this->input->post('duracion_meses'),
					'ente_generador' => $this->input->post('ente'),
					'responsable_proyecto' => $this->input->post('responsable'),
					'intermediario' => $this->input->post('intermediario'),
					'municipio' => $this->input->post('municipio'),
					'localidad' => $this->input->post('localidad'),
					'entidad_federativa' => $this->input->post('entidad_federativa')
				);
			$id_detalles = $this->_agregar_detalles_concepto($datos_detalle);

			$costo_publicado = str_replace(',', '', $this->input->post('costo_publicado'));
			$costo_autorizado = str_replace(',', '', $this->input->post('costo_autorizado'));
			$costo_incrementado = str_replace(',', '', $this->input->post('costo_incrementado'));
			$monto_acordado = str_replace(',', '', $this->input->post('monto_acordado'));

			$datos_financieros = array(
					'concepto' => $id,
					'costo_publicado' => $costo_publicado,
					'costo_autorizado' => $costo_autorizado,
					'costo_incrementado' => $costo_incrementado,
					'monto_acordado' => $monto_acordado,
					'numero_pagos' => $this->input->post('numero_pagos'),
					'forma_pago' => $this->input->post('forma_pago')
				);
			
			$id_financieros = $this->_agregar_detalles_financieros($datos_financieros);

			if ($id_detalles != 0) {
				$urls = array(
						'Agregar nuevo registro' => site_url('agregar_nuevo_concepto'),
						'Volver al catálogo' => site_url('listados/listar_todos_conceptos')
					);
				$url_cerrar = site_url('usuario');
				$mensaje = $this->load->view('mensajes/ventana_registro_exito', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
				$datos = array('mensaje' => $mensaje);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			} else {
				
				$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_concepto'),
						'Volver al catálogo' => site_url('listados/listar_todos_conceptos')
					);
				$url_cerrar = site_url('usuario');
				$mensaje = $this->load->view('mensajes/ventana_registro_fallido', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
				$datos = array('mensaje' => $mensaje);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			}

			
		} else {
			$this->_cargar_vista();
		}
		
	}

	private function _agregar_nuevo_concepto($datos)
	{
		$this->load->model('Concepto','concepto',TRUE);
		$agregado = $this->concepto->agregar_nuevo_concepto($datos);
		if ($agregado != 0) {
			$id = $this->concepto->obtener_id_concepto_creado($datos['fecha_creacion']);
			return $id;
		} else {
			return 0;
		}
	}

	private function _agregar_detalles_concepto($datos)
	{
		$this->load->model('Detalles_Concepto', 'detalles', TRUE);
		$id = $this->detalles->agregar_detalles_concepto($datos);
		return $id;
	}

	private function _agregar_detalles_financieros($datos)
	{
		$this->load->model('Detalles_Financieros', 'financieros', TRUE);
		$id = $this->financieros->agregar_detalles_financieros($datos);
		return $id;
	}

	private function _cargar_vista()
	{
		$this->load->model('Forma_Pago','formas', TRUE);
		$formas = $this->formas->obtener_todas_formas_pago();
		//$this->load->model('Tipo_Concepto','tipos',TRUE);
		$tipos =  array('' => 'Seleccione...'); //$this->tipos->obtener_todos_tipos();
		$this->load->model('Ente_Generador','entes',TRUE);
		$entes = $this->entes->obtener_todos_entes();
		$this->load->model('Responsable_Proyecto','responsable',TRUE);
		$responsables = $this->responsable->obtener_empresas_responsables();
		$this->load->model('Intermediario','intermediario',TRUE);
		$intermediarios = $this->intermediario->obtener_nombres_intermediarios();
		$this->load->model('Estado_Proyecto','estados', TRUE);
		$estados = $this->estados->obtener_todos_estados();
		$this->load->model('Entidad','',TRUE);
		$entidades = $this->Entidad->obtener_todas_entidades();
		$this->load->model('Tipo_Cuenta', 'cuenta', TRUE);
		$cuentas = $this->cuenta->listar_tipos_cuenta_entrada();

		$this->load->model('Municipio', 'municipio', TRUE);
		$municipios = $this->municipio->enlistar_municipios_entidad(31);
		//$datos['url_servicio'] = site_url('servicios/obtener_municipios');

		$datos = array(
				'formas' => $formas,
				'tipos' => $tipos,
				'entes' => $entes,
				'responsables' => $responsables,
				'estados' => $estados,
				'intermediarios' => $intermediarios,
				'entidades' => $entidades,
				'cuentas' => $cuentas,
				'municipios' => $municipios,
				'url_servicio' => site_url('servicios/obtener_municipios'),
				'url_servicio_tipos_concepto' => site_url('servicios/obtener_tipos_concepto')
			);

		$this->load->view('agregar_nuevo_concepto', $datos);
	}

	private function _obtener_fecha()
	{
		# code...
	}
}
?>