<?php

/**
* 
*/
class Listar_Tipos_Bovedas extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('header');
		$this->form_validation->set_message('required', 'Debe elegir alguna opción de resultados');
		$this->form_validation->set_rules('opcion', 'opción', 'required');
		if ($this->form_validation->run()) {
			$inicio_pagina = $this->_calcular_paginado( $this->input->post('opcion') );
			$this->_obtener_datos($inicio_pagina, 30);
		} else {
			$this->_obtener_datos(0, 30);
		}
	}

	private function _obtener_datos($inicio, $limite)
	{
		$this->load->model('Boveda', 'boveda', TRUE);
		$bovedas = $this->boveda->listar_bovedas($inicio, $limite);
		if (sizeof($bovedas) == 0) {
			$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_tipo_boveda'),
						'Volver al índice' => site_url('usuario')
					);
			$url_cerrar = site_url();

			$mensaje = $this->load->view('mensajes/ventana', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);

			$datos = array('mensaje' => $mensaje);
			$this->load->view('mensajes/imprimir_mensaje', $datos);
		} else {
			$datos = array(
							'url_servicio' => site_url('servicios/cambiar_estado_elemento'),
							'modelo' => 'Boveda',
							'bovedas' => $bovedas
							);
			$this->_establecer_paginado($inicio);
			$this->load->view('listados/listar_tipos_bovedas', $datos);
		}
	}

	private function _establecer_paginado($inicio_pagina)
	{
		$this->session->set_userdata(
				array('inicio_pagina' => $inicio_pagina)
			);
	}

	private function _calcular_paginado($opcion)
	{
		$inicio_pagina = $this->session->userdata('inicio_pagina');
		if ($opcion == 'dec') {
			$inicio_pagina = $inicio_pagina - 30;
			if ($inicio_pagina < 0) {
				$inicio_pagina = 0;
			}
		} else {
			$inicio_pagina = $inicio_pagina + 30;
		}
		return $inicio_pagina;
	}
}
?>