<?php

/**
* 
*/
class Listar_Conceptos extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this-> _cargar_cabecera();
		$this->_obtener_datos();
	}

	private function _cargar_cabecera()
	{
		//$this->load->view('header');
	}

	private function _obtener_datos()
	{
		$this->load->model('Entrada', 'entrada', TRUE);
		$dias = $this->_generar_dias('2014-01-11', 4);
		$entradas = $this->entrada->obtener_total_fecha_especifica($dias, 4);
		foreach ($entradas as $entrada) {
			print_r($entrada);
			echo "<br>";
		}
	}

	private function _generar_dias($fecha , $numero_registros)
	{
		$dia = strtotime($fecha);
		$dias = array($fecha);
		for ($i=0; $i < $numero_registros; $i++) { 
			$dia = strtotime('last day', $dia);
			array_push($dias, date('Y-m-d',$dia));
		}
		return $dias;
	}
}
?>