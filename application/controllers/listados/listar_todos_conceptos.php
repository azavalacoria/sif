<?php

/**
* 
*/
class Listar_Todos_Conceptos extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->load->view('header');
		$this->form_validation->set_message('required', 'Debe elegir alguna opción de resultados');
		$this->form_validation->set_rules('opcion', 'opción', 'required');
		if ($this->form_validation->run()) {
			$inicio_pagina = $this->_calcular_paginado( $this->input->post('opcion') );
			$this->_obtener_datos($inicio_pagina, 30);
		} else {
			$this->_obtener_datos(0, 30);
		}
	}

	private function _obtener_datos($inicio, $limite)
	{

		$this->load->model('Concepto', 'concepto', TRUE);
		$conceptos = $this->concepto->listar_todos_conceptos($inicio, $limite);

		if (sizeof($conceptos) == 0) {
			$urls = array(
						'Agregar nuevo concepto' => site_url('agregar_nuevo_concepto'),
						'Volver al índice' => site_url('usuario'),
					);
			$url_cerrar = site_url('usuario');

			$mensaje = $this->load->view('mensajes/ventana', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);

			$datos = array('mensaje' => $mensaje);
			$this->load->view('mensajes/imprimir_mensaje', $datos);
		} else {
			$datos = array(
							'url_servicio' => site_url('servicios/cambiar_estado_elemento'),
							'modelo' => 'Concepto',
							'conceptos' => $conceptos
							);
			$this->_establecer_paginado($inicio);
			$this->load->view('listados/listar_todos_conceptos', $datos);
		}
	}

	private function _establecer_paginado($inicio_pagina)
	{
		$this->session->set_userdata(
				array('inicio_pagina' => $inicio_pagina)
			);
	}

	private function _calcular_paginado($opcion)
	{
		$inicio_pagina = $this->session->userdata('inicio_pagina');
		if ($opcion == 'dec') {
			$inicio_pagina = $inicio_pagina - 30;
			if ($inicio_pagina < 0) {
				$inicio_pagina = 0;
			}
		} else {
			$inicio_pagina = $inicio_pagina + 30;
		}
		return $inicio_pagina;
	}

	public function inde2x()
	{
		$this->load->model('Concepto', 'concepto', TRUE);
		
		$conceptos = $this->concepto->listar_todos_conceptos();

		$datos = array('conceptos' => $conceptos);
		$this->load->view('header');
		$this->load->view('listados/listar_todos_conceptos', $datos);
	}
}
?>