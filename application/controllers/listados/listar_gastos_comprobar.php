<?php

/**
* 
*/
class Listar_Gastos_Comprobar extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('header');
		$this->form_validation->set_message('required', 'Debe elegir alguna opción de resultados');
		$this->form_validation->set_rules('opcion', 'opción', 'required');
		if ($this->form_validation->run()) {
			$inicio_pagina = $this->_calcular_paginado( $this->input->post('opcion') );
			$this->_obtener_datos($inicio_pagina, 30);
		} else {
			$this->_obtener_datos(0, 30);
		}
	}

	private function _obtener_datos($inicio, $limite)
	{
		$this->load->model('Movimiento', 'movimiento', TRUE);
		$movimientos = $this->movimiento->listar_gastos_comprobar($inicio, $limite);
		if (sizeof($movimientos) == 0) {
			$urls = array(
						'Volver al listado' => site_url('listados/listar_gastos_comprobar'),
						'Volver al índice' => site_url('usuario')
					);
			$url_cerrar = site_url('usuario');

			$mensaje = $this->load->view('mensajes/ventana', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);

			$datos = array('mensaje' => $mensaje);
			$this->load->view('mensajes/imprimir_mensaje', $datos);
		} else {
			$datos = array('movimientos' => $movimientos);
			$this->_establecer_paginado($inicio);
			$this->load->view('listados/listar_gastos_comprobar', $datos);
		}
	}

	private function _establecer_paginado($inicio_pagina)
	{
		$this->session->set_userdata(
				array('inicio_pagina' => $inicio_pagina)
			);
	}

	private function _calcular_paginado($opcion)
	{
		$inicio_pagina = $this->session->userdata('inicio_pagina');
		if ($opcion == 'dec') {
			$inicio_pagina = $inicio_pagina - 30;
			if ($inicio_pagina < 0) {
				$inicio_pagina = 0;
			}
		} else {
			$inicio_pagina = $inicio_pagina + 30;
		}
		return $inicio_pagina;
	}
}
?>