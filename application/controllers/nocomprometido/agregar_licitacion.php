<?php

/**
* 
*/
class Agregar_Licitacion extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->form_validation->set_rules('clave_licitacion','clave de la licitacion','trim|required');
		$this->form_validation->set_rules('nombre_licitacion','nombre de la licitacion','trim|required');
		$this->form_validation->set_rules('descripcion_licitacion','descripcion de la licitacion','trim|required');
		$this->form_validation->set_rules('estado','estado','trim|required');
		$this->form_validation->set_rules('prioridad','prioridad','trim|required|integer|greater_than[0]|less_than[6]');
		$this->form_validation->set_rules('secretaria','secretaria','trim|required');
		$this->form_validation->set_rules('fecha_publicacion','fecha de publicacion','trim|required');
		$this->form_validation->set_rules('fuente_publicacion','fuente de la publicacion','trim|required');
		
		$this->form_validation->set_rules('monto_publicado','','trim|required');
		$this->form_validation->set_rules('fecha_presentacion_apertura','fecha para adquirir las bases','trim|required');

		$sesion = $this->session->userdata('sesion_amigo');
		if($sesion != 1)
		{
			redirect(site_url());
		}

		if ($this->form_validation->run()) {
			$id_amigo = $this->session->userdata('id_amigo');
			$datos = array(
							'clave' => $this->input->post('clave_licitacion'),
							'nombre_licitacion' => $this->input->post('nombre_licitacion'),
							'descripcion' => $this->input->post('descripcion_licitacion'),
							'agregada_por' => $id_amigo,
							'estado' => $this->input->post('estado'),
							'prioridad' => $this->input->post('prioridad'),
							'secretaria' => $this->input->post('secretaria'),
							'fecha_publicacion' => $this->input->post('fecha_publicacion'),
							'monto_publicado' => $this->input->post('monto_publicado'),
							'fecha_presentacion_apertura' => $this->input->post('fecha_presentacion_apertura'),
							'fuente_publicacion' => $this->input->post('fuente_publicacion'),
						);
			$this->load->model('nocomprometido/licitaciones','licitaciones', TRUE);
			$afectadas = $this->licitaciones->agregar_licitacion($datos);

			if ($afectadas != 0) {

				$urls = array(
						'Agregar nuevo registro' => site_url('nocomprometido/agregar_licitacion'),
						'Volver al catálogo' => site_url('nocomprometido/listar_licitaciones')
					);
				$url_cerrar = site_url();
				$mensaje = $this->load->view('mensajes/ventana_registro_exito', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
				$datos = array('mensaje' => $mensaje);
				$this->load->view('header');
				$this->load->view('mensajes/imprimir_mensaje', $datos);

			} else {
				
				$urls = array(
						'Agregar nuevo registro' => site_url('nocomprometido/agregar_licitacion'),
						'Volver al catálogo' => site_url('listados/listar_formas_pago')
					);
				$url_cerrar = site_url();
				$mensaje = $this->load->view('mensajes/ventana_registro_fallido', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
				$datos = array('mensaje' => $mensaje);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			}
		} else{
			$this->_cargar_vista();
		}
	}

	private function _cargar_vista()
	{
		$this->load->model('nocomprometido/estado', 'estado', TRUE);
		$this->load->model('nocomprometido/fuente_publicacion','fuentes', TRUE);
		
		$fuentes = $this->fuentes->obtener_todas_fuentes();
		$estados = $this->estado->listar_estados();
		$prioridades = array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5);
		$datos = array(
				'personas' => array('' => 'Seleccione...'),
				'secretarias' => array('' => 'Seleccione...'),
				'url_ent_servicio' => site_url('servicios/obtener_secretarias'),
				'url_servicio' => site_url('servicios/obtener_personas'),
				'estados' => $estados,
				'fuentes' => $fuentes,
				'prioridades' => $prioridades
			);
		$this->load->view('header');
		$this->load->view('nocomprometido/agregar_licitacion', $datos);
	}
}
?>