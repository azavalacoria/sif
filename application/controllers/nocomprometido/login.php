<?php

/**
* 
*/
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
		$this->form_validation->set_rules('nickname','nombre de usuario','required|trim');
		$this->form_validation->set_rules('password','password','required|trim');
		if ($this->form_validation->run()) {
			$datos = array(
							'nickname' => $this->input->post('nickname'),
							'password' => sha1($this->input->post('password'))
						);
			$this->_buscar_amigo($datos);
		} else {
			$this->_cargar_vista(null);
		}
	}

	public function _buscar_amigo($datos)
	{
		$this->load->model('nocomprometido/sesion','amigo', TRUE);
		$amigo = $this->amigo->existe_amigo($datos);
		if (sizeof($amigo) == 3) {
			
			$sesion = array(
					'sesion_amigo' => 1,
					'id_amigo' => $amigo['id_amigo'],
					'persona' => $amigo['persona'],
					'secretaria' => $amigo['secretaria_amigo']
				);
			$this->session->set_userdata($sesion);

			redirect('home_amigo');
		} else {
			$urls = array(
				'Volver a intentar' => site_url('nocomprometido/login')
				);
			$url_cerrar = site_url('nocomprometido/login');
			$mensaje = $this->load->view('mensajes/ventana', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
			$datos = array('mensaje' => $mensaje);
			$this->load->view('header');
			$this->load->view('mensajes/ventana_error_login', $datos);
		}
	}

	public function _cargar_vista($mensaje)
	{
		$datos = array('mensaje' => $mensaje);
		$this->load->view('nocomprometido/login');
	}


}
?>