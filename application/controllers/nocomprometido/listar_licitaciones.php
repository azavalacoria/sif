<?php

/**
* 
*/
class Listar_Licitaciones extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$sesion = $this->session->userdata('sesion_amigo');
		if($sesion < 1)
		{
			redirect('amigo');
		}

		$this->load->view('header');
		$this->form_validation->set_message('required', 'Debe elegir alguna opción de resultados');
		$this->form_validation->set_rules('opcion', 'opción', 'required');
		if ($this->form_validation->run()) {
			$inicio_pagina = $this->_calcular_paginado( $this->input->post('opcion') );
			$this->_obtener_datos($inicio_pagina, 30);
		} else {
			$this->_obtener_datos(0, 30);
		}
	}

	private function _obtener_datos($inicio, $limite)
	{

		$this->load->model('nocomprometido/Licitaciones','licitaciones', TRUE);
		$id_amigo = $this->session->userdata('id_amigo');
		$licitaciones = $this->licitaciones->obtener_licitaciones($inicio, $limite, $id_amigo);

		if (sizeof($licitaciones) == 0) {
			$urls = array(
						'Agregar nuevo registro' => site_url('nocomprometido/agregar_licitacion'),
						'Cerrar Sesión' => site_url('login/salir_amigo')
					);
			$url_cerrar = site_url('login/salir_amigo');

			$mensaje = $this->load->view('mensajes/ventana', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);

			$datos = array('mensaje' => $mensaje);
			$this->load->view('mensajes/imprimir_mensaje', $datos);
		} else {
			$datos = array('licitaciones' => $licitaciones, 'url_servicio' => '', 'modelo' => '');
			//$datos = array('url_servicio' => site_url('servicios/cambiar_estado_elemento'),'modelo' => 'Puesto','puestos' => $puestos);
			$this->_establecer_paginado($inicio);
			$this->load->view('nocomprometido/listar_licitaciones', $datos);
		}
	}

	private function _establecer_paginado($inicio_pagina)
	{
		$this->session->set_userdata(
				array('inicio_pagina' => $inicio_pagina)
			);
	}

	private function _calcular_paginado($opcion)
	{
		$inicio_pagina = $this->session->userdata('inicio_pagina');
		if ($opcion == 'dec') {
			$inicio_pagina = $inicio_pagina - 30;
			if ($inicio_pagina < 0) {
				$inicio_pagina = 0;
			}
		} else {
			$inicio_pagina = $inicio_pagina + 30;
		}
		return $inicio_pagina;
	}
}
?>