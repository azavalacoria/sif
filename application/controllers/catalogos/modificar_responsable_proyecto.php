<?php

/**
* 
*/
class Modificar_Responsable_Proyecto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('id_responsable','responsable','required');

		if ($this->form_validation->run()) {
			$this->_cargar_formulario_inicial($this->input->post('id_responsable'));
		} else {
			redirect('listados/listar_responsables_proyectos');
		}
		
	}

	public function aplicar_cambios()
	{
		$this->form_validation->set_rules('persona', 'Nombre del Responsable de Proyecto','required');
		$this->form_validation->set_rules('organizacion','Organizacion','required');

		if ($this->form_validation->run()) {

			$id_responsable = $this->input->post('id_responsable');
			$responsable = array(
					'persona' => $this->input->post('persona'),'organizacion' => $this->input->post('organizacion')
					);

			$repetido = $this->_verificar_cambios($responsable);

			if ($repetido == 0) {
				$actualizados = $this->_guardar_cambios($responsable, $id_responsable);
				$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-checkmark\"></span>',
                                title: 'Modificación realizada',
                                width: 500,
                                padding: 10,
                                content: '<h2>Se han actualizado los datos del responsable del proyecto correctamente</h2><button onclick=window.location.assign(\"../../listados/listar_responsables_proyectos\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_responsables_proyectos";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			} else {
				$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-cancel-2\"></span>',
                                title: 'Registro repetido',
                                width: 500,
                                padding: 10,
                                content: '<h2>Ya existe un responsable de proyecto con los mismos datos</h2><button onclick=window.location.assign(\"../../listados/listar_responsables_proyectos\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_responsables_proyectos";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			}
		} else {
			redirect('listados/listar_responsables_proyectos');
		}
		
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	public function _cargar_formulario_inicial($id)
	{
		$this->load->model('Responsable_Proyecto', 'responsable', TRUE);
		$responsable = $this->responsable->obtener_responsable($id);

		$datos = array(
				'responsable' => $responsable,
				'url_org_servicio' => site_url('servicios/obtener_organizaciones'),
				'url_servicio' => site_url('servicios/obtener_personas')
			);
		$this->load->view('header');
		$this->load->view('catalogos/modificar_responsable_proyecto', $datos);
	}

	public function _verificar_cambios($datos)
	{
		$this->load->model('Responsable_Proyecto', 'responsable', TRUE);
		$repetido = $this->responsable->comprobar_registro_repetido($datos);
		return $repetido;
	}

	public function _guardar_cambios($datos, $id_intermediario)
	{
		$this->load->model('Responsable_Proyecto', 'responsable', TRUE);
		$actualizados = $this->responsable->actualizar_responsable_proyecto($datos, $id_intermediario);
		return $actualizados;
	}
}
?>