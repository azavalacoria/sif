<?php

/**
* 
*/
class Modificar_Tipo_Boveda extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('id_tipo_boveda', 'boveda', 'required');

		if ($this->form_validation->run()) {
			$this->_cargar_vista_modificacion($this->input->post('id_tipo_boveda'));
		} else {
			redirect('listados/listar_tipos_bovedas');
		}
	}

	public function aplicar_cambios()
	{
		$this->form_validation->set_rules('nombre_boveda','nombre de la bóveda','trim|required');

		if ($this->form_validation->run()) {
			$id_tipo_boveda = $this->input->post('id_tipo_boveda');
			$boveda = array('nombre_boveda' => $this->input->post('nombre_boveda'));

			$this->_verificar_y_aplicar_cambios($boveda, $id_tipo_boveda);
			
		} else {
			$id_tipo_boveda = $this->input->post('id_tipo_boveda');
			if ($id_tipo_boveda != 0) {
				//echo "aqui recargaremos $id_tipo_boveda" ;
				$datos = array('id_tipo_boveda' => $id_tipo_boveda);
				$this->load->view('header');
				$this->load->view('catalogos/modificar_tipo_boveda_recargada', $datos);
			} else {
				redirect('listados/listar_tipos_bovedas');
			}
			
		}
		
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	private function _cargar_vista_modificacion($id_tipo_boveda)
	{
		$this->load->model('Boveda', 'boveda', TRUE);
		$boveda = $this->boveda->obtener_tipo_boveda($id_tipo_boveda);
		
		$datos = array('boveda' => $boveda);
		$this->load->view('header');
		$this->load->view('catalogos/modificar_tipo_boveda', $datos);
	}

	private function _verificar_y_aplicar_cambios($datos, $id_tipo_boveda)
	{
		$this->load->model('Boveda', 'boveda', TRUE);
		$repetido = $this->boveda->verificar_repetido($datos, $id_tipo_boveda);
		if ($repetido > 0) {
			$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-cancel-2\"></span>',
                                title: 'Registro repetido',
                                width: 500,
                                padding: 10,
                                content: '<h2>Ya existe una bóveda con los mismos datos</h2><button onclick=window.location.assign(\"../../listados/listar_tipos_bovedas\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_tipos_bovedas";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
		} else {
			$actualizados = $this->boveda->actualizar_boveda($id_tipo_boveda, $datos);
			if ($actualizados == 0) {
				$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-cancel-2\"></span>',
                                title: 'Registro repetido',
                                width: 500,
                                padding: 10,
                                content: '<h2>No se pudieron aplicar los cambios</h2><button onclick=window.location.assign(\"../../listados/listar_tipos_bovedas\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_tipos_bovedas";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			} else {
				$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-checkmark\"></span>',
                                title: 'Modificación realizada',
                                width: 500,
                                padding: 10,
                                content: '<h2>Se han actualizado los datos de la bóveda</h2><button onclick=window.location.assign(\"../../listados/listar_tipos_bovedas\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_tipos_bovedas";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			}
			
		}
		
	}
}
?>