<?php

/**
* 
*/
class Agregar_Nuevo_Estado_Concepto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('nombre_estado_concepto','nombre del estado','required|trim');

		if ($this->form_validation->run()) {
			$datos = array('nombre_estado_concepto' => $this->input->post('nombre_estado_concepto'));

			$this->load->model('Estado_Proyecto', 'estado', TRUE);
			$afectadas = $this->estado->agregar_nuevo_estado($datos);

			$url_cerrar = site_url('usuario');

			if ($afectadas != 0) {
				$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_estado_concepto'),
						'Volver al catálogo' => site_url('listados/listar_estados_concepto')
					);
				$this->_mostrar_mensaje('mensajes/ventana_registro_exito', $urls, $url_cerrar);

			} else {
				$urls = array(
						'Intentar con otro registro' => site_url('catalogos/agregar_nueva_forma_pago'),
						'Volver al catálogo' => site_url('listados/listar_formas_pago')
					);
				$this->_mostrar_mensaje('mensajes/ventana_registro_fallido', $urls, $url_cerrar);
			}
		} else {
			$this->_cargar_vista();
		}
		
	}

	public function _mostrar_mensaje($vista, $urls, $url_cerrar)
	{
		$mensaje = $this->load->view($vista, array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
		$datos = array('mensaje' => $mensaje);
		$this->load->view('mensajes/imprimir_mensaje', $datos);
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	private function _cargar_vista()
	{
		$this->load->view('catalogos/agregar_nuevo_estado_concepto');
	}
}
?>