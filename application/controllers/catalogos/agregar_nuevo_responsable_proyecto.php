<?php
/**
* 
*/
class Agregar_Nuevo_Responsable_Proyecto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('persona','responsable','required|trim');
		$this->form_validation->set_rules('organizacion','organizacion','required|trim');
		if ($this->form_validation->run()) {
			$datos = array(
					'persona' => $this->input->post('persona'),
					'organizacion' => $this->input->post('organizacion')
				);
			$this->load->model('Responsable_Proyecto','responsable',TRUE);
			$agregadas = $this->responsable->agregar_nuevo_responsable($datos);
			if ($agregadas != 0) {
				
				$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_responsable_proyecto'),
						'Volver al catálogo' => site_url('listados/listar_responsables_proyectos')
					);
				$url_cerrar = site_url();
				$mensaje = $this->load->view('mensajes/ventana_registro_exito', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
				$datos = array('mensaje' => $mensaje);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
				
			} else {
				
				$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_responsable_proyecto'),
						'Volver al catálogo' => site_url('listados/listar_responsables_proyectos')
					);
				$url_cerrar = site_url();
				$mensaje = $this->load->view('mensajes/ventana_registro_fallido', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
				$datos = array('mensaje' => $mensaje);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			}
		} else {
			$this->_cargar_vista();
		}
		
		
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	private function _cargar_vista()
	{
		$datos = array(
				'personas' => array('' => 'Seleccione...'),
				'organizaciones' => array('' => 'Seleccione...'),
				'url_org_servicio' => site_url('servicios/obtener_organizaciones'),
				'url_servicio' => site_url('servicios/obtener_personas')
			);
		$this->load->view('catalogos/agregar_nuevo_responsable_proyecto', $datos);
	}
}
?>