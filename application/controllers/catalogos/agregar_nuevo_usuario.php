<?php

/**
* 
*/
class Agregar_Nuevo_Usuario extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->view('header');
	}

	public function index()
	{
		$this->form_validation->set_rules('nombre_usuario','nombre de usuario','required|trim');
		$this->form_validation->set_rules('contrasena','contraseña','required|trim');
		$this->form_validation->set_rules('persona','persona','required|trim');
		$this->form_validation->set_rules('puesto','puesto','required|trim');
		if ($this->form_validation->run()) {
			$this->_agregar_datos(
					$this->input->post('nombre_usuario'),
					$this->input->post('contrasena'),
					$this->input->post('persona'),
					$this->input->post('puesto'),
					$this->input->post('amigo')
				);
		} else {
			$this->_cargar_vista();
		}
		
	}

	private function _cargar_vista()
	{
		
		$this->load->model('Persona','persona',TRUE);
		$personas = $this->persona->obtener_todas_personas();
		//print_r($personas);
		$this->load->model('Puesto','puesto',TRUE);
		$puestos = $this->puesto->obtener_todos_puestos();
		$datos = array('personas' => $personas, 'puestos' => $puestos);
		$this->load->view('catalogos/agregar_nuevo_usuario', $datos);
	}

	private function _agregar_datos($nombre_usuario, $contrasena, $persona, $puesto, $amigo)
	{
		if (!isset($amigo)) {
			$amigo = 0;
		}
		
		$datos = array(
				'nombre_usuario' => $nombre_usuario,
				'contrasena' => sha1($contrasena),
				'persona' => $persona,
				'permisos' => 1,
				'puesto' => $puesto
			);
		$this->load->model('Usuario', 'usuario', TRUE);
		$agregados = $this->usuario->agregar_nuevo_usuario($datos);
		if ($agregados != 0) {

			$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_usuario'),
						'Volver al catálogo' => site_url('listados/listar_usuarios')
					);
				$url_cerrar = site_url();
				$mensaje = $this->load->view('mensajes/ventana_registro_exito', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
				$datos = array('mensaje' => $mensaje);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
				
		} else {

			$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_usuario'),
						'Volver al catálogo' => site_url('listados/listar_usuarios')
					);
			$url_cerrar = site_url();
			$mensaje = $this->load->view('mensajes/ventana_registro_fallido', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
			$datos = array('mensaje' => $mensaje);
		}
		
	}
}
?>