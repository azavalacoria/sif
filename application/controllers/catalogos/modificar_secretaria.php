<?php

/**
* 
*/
class Modificar_Secretaria extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('id_secretaria','secretaria','required');
		if ($this->form_validation->run()) {
			$this->_cargar_vista_modificacion($this->input->post('id_secretaria'));
		} else {
			redirect('listados/listar_secretarias');
		}
	}

	public function aplicar_cambios()
	{
		$this->form_validation->set_rules('nombre' , 'nombre de la secretaria', 'trim|required');
		$this->form_validation->set_rules('siglas' , 'siglas', 'trim|required');
		$this->form_validation->set_rules('direccion' , 'direccion', 'trim|required');
		$this->form_validation->set_rules('entidad_federativa' , 'entidad federativa', 'trim|required');
		$this->form_validation->set_rules('municipio' , 'municipio', 'trim|required');
		$this->form_validation->set_rules('localidad' , 'localidad', 'trim|required');

		if ($this->form_validation->run()) {
			$id_secretaria = $this->input->post('id_secretaria');

			$datos = array(
					'nombre_secretaria' => $this->input->post('nombre'),
					'siglas' => $this->input->post('siglas'),
					'direccion' => $this->input->post('direccion'),
					'entidad_federativa' => $this->input->post('entidad_federativa'),
					'municipio' => $this->input->post('municipio'),
					'localidad' => $this->input->post('localidad')
				);
			$this->_verificar_y_aplicar_cambios($id_secretaria, $datos);
		} else {
			$id_secretaria = $this->input->post('id_secretaria');

			if ($id_secretaria != 0) {
				$this->_cargar_vista_modificacion_recargada(
						$id_secretaria,
						$this->input->post('entidad_federativa'),
						$this->input->post('municipio'),
						$this->input->post('localidad')
					);

			} else {
				redirect('listados/listar_secretarias');
			}
			
		}
		
	}

	private function _cargar_vista_modificacion($id_secretaria)
	{
		$this->load->model('Secretaria','secretaria',TRUE);
		$secretaria = $this->secretaria->obtener_secretaria_modificacion($id_secretaria);

		$entidades_federativas = $this->_obtener_entidades_federativas();
		$municipios = $this->_obtener_municipios($secretaria['entidad_federativa']);
		$localidades = $this->_obtener_localidades($secretaria['municipio']);

		$datos = array(
				'secretaria' => $secretaria,
				'entidades' => $entidades_federativas,
				'municipios' => $municipios,
				'localidades' => $localidades,
				'url_servicio' => site_url('servicios/obtener_municipios')
			);
		$this->load->view('header');
		$this->load->view('catalogos/modificar_secretaria', $datos);
	}

	private function _cargar_vista_modificacion_recargada($id_secretaria, $entidad_federativa, $municipio, $localidad)
	{
		$entidades_federativas = $this->_obtener_entidades_federativas();

		if ($municipio != 0) {
			$municipios = $this->_obtener_municipios($entidad_federativa);
		} else {
			$municipios = array();
		}
		
		if ($localidad != 0) {
			$localidades = $this->_obtener_localidades($municipio);
		} else {
			$localidades = array();
		}
		
		$datos = array(
				'id_secretaria' => $id_secretaria,
				'entidades' => $entidades_federativas,
				'municipios' => $municipios,
				'localidades' => $localidades,
				'url_servicio' => site_url('servicios/obtener_municipios')
			);
		$this->load->view('header');
		$this->load->view('catalogos/modificar_secretaria_recargada', $datos);
	}

	private function _verificar_y_aplicar_cambios($id_secretaria, $datos)
	{
		$this->load->model('Secretaria', 'secretaria', TRUE);
		$repetido = $this->secretaria->verificar_datos_repetidos($datos);
		if ($repetido == 0) {
			$actualizados = $this->secretaria->actualizar_secretaria($id_secretaria, $datos);
			if ($actualizados > 0) {
				$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-checkmark\"></span>',
                                title: 'Modificación realizada',
                                width: 500,
                                padding: 10,
                                content: '<h2>Se han actualizado los datos de la secretaría</h2><button onclick=window.location.assign(\"../../listados/listar_secretarias\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_secretarias";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			} else {
				$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-cancel-2\"></span>',
                                title: 'Registro repetido',
                                width: 500,
                                padding: 10,
                                content: '<h2>No se pudieron aplicar los cambios</h2><button onclick=window.location.assign(\"../../listados/listar_secretarias\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_secretarias";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			}
		} else {
			$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-cancel-2\"></span>',
                                title: 'Registro repetido',
                                width: 500,
                                padding: 10,
                                content: '<h2>Ya existe una secretaría con los mismos datos</h2><button onclick=window.location.assign(\"../../listados/listar_secretarias\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_secretarias";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
		}
		
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	private function _obtener_entidades_federativas()
	{
		$this->load->model('Entidad', 'entidad', TRUE);
		$entidades = $this->entidad->obtener_todas_entidades();
		return $entidades;
	}

	private function _obtener_municipios($entidad_federativa)
	{
		$this->load->model('Municipio', 'municipio', TRUE);
		$municipios = $this->municipio->enlistar_municipios_entidad($entidad_federativa);
		return $municipios;
	}

	private function _obtener_localidades($municipio)
	{
		$this->load->model('Localidad', 'localidad', TRUE);
		$localidades = $this->localidad->enlistar_localidades_municipio($municipio);
		return $localidades;
	}
}
?>