<?php

/**
* 
*/
class Agregar_Nuevo_Tipo_Concepto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('nombre_concepto','nombre del concepto','required|trim');
		$this->form_validation->set_rules('descripcion_concepto','descripcion del concepto','required|trim');
		$this->form_validation->set_rules('tipo_cuenta','tipo de cuenta','required|trim');

		if ($this->form_validation->run()) {
			
			$datos = array(
					'descripcion_tipo_concepto' => $this->input->post('descripcion_concepto'),
					'nombre_tipo_concepto' => $this->input->post('nombre_concepto'),
					'tipo_cuenta' => $this->input->post('tipo_cuenta')
				);
			
			$this->load->model('Tipo_Concepto', 'concepto', TRUE);
			$agregadas = $this->concepto->agregar_nuevo_tipo_concepto($datos);
			
			$url_cerrar = site_url('usuario');
			$urls = null;
			$vista = '';

			if ($agregadas != 0) {
				$urls = array(
						'Intentar con otro registro' => site_url('catalogos/agregar_nuevo_tipo_concepto'),
						'Volver al índice' => site_url('listados/listar_tipos_concepto')
					);
				$vista = 'mensajes/ventana_registro_exito';
			} else {
				$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_tipo_concepto'),
						'Volver al índice' => site_url('usuario')
					);
				$vista = 'mensajes/ventana_registro_fallido';
			}
			$this->_mostrar_mensaje($vista, $urls, $url_cerrar);
		} else {
			$this->_cargar_vista();
		}
		
	}

	public function _mostrar_mensaje($vista,$urls, $url_cerrar)
	{
		$direcciones = array('urls' => $urls, 'url_cerrar' => $url_cerrar);
		$mensaje = $this->load->view($vista, $direcciones, TRUE);
		$datos = array('mensaje' => $mensaje);
		$this->load->view('mensajes/imprimir_mensaje', $datos);
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	public function _cargar_vista()
	{
		$this->load->model('Tipo_Cuenta', 'cuenta', TRUE);
		$cuentas = $this->cuenta->listar_tipos_cuenta();

		$datos = array(
			'cuentas' => $cuentas,
			'url_servicio_catalogos' => site_url('servicios/servicio_tipos_concepto')
			);

		$this->load->view('catalogos/agregar_nuevo_tipo_concepto', $datos);
	}
}
?>