<?php

/**
* 
*/
class Modificar_Forma_Pago extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();

	}

	public function index()
	{
		$this->form_validation->set_rules('id_forma_pago','forma de pago', 'required');

		if ($this->form_validation->run()) {
			$id_forma_pago = $this->input->post('id_forma_pago');

			$this->_cargar_vista_modificacion($id_forma_pago);
		} else {
			redirect('listados/listar_formas_pago');
		}
	}

	public function aplicar_cambios()
	{
		$this->form_validation->set_rules('descripcion','descripcion', 'trim|required');
		$this->form_validation->set_rules('tipo_boveda','tipo de bóveda', 'required');

		if ($this->form_validation->run()) {
			$datos = array(
					'nombre_forma_pago' => $this->input->post('descripcion'),
					'tipo_boveda' => $this->input->post('tipo_boveda')
				);
			$this->_verificar_cambios($datos, $this->input->post('id_forma_pago'));
		} else {
			$this->_recargar_vista_modificacion($this->input->post('id_forma_pago'));
		}
		
	}

	public function _cargar_vista_modificacion($id_forma_pago)
	{
		$this->load->model('Forma_Pago', 'forma', TRUE);
		$forma = $this->forma->obtener_forma_pago($id_forma_pago);

		$bovedas = $this->_obtener_bovedas();
		$datos = array('bovedas' => $bovedas, 'forma' => $forma);

		$this->load->view('header');
		$this->load->view('catalogos/modificar_forma_pago', $datos);
	}

	private function _recargar_vista_modificacion($id_forma_pago)
	{
		if ($id_forma_pago != 0) {

			$bovedas = $this->_obtener_bovedas();

			$datos = array('bovedas' => $bovedas, 'id_forma_pago' => $id_forma_pago);

			$this->load->view('header');
			$this->load->view('catalogos/modificar_forma_pago_recargada', $datos);
		} else {
			redirect('listados/listar_formas_pago');
		}
	}

	private function _verificar_cambios($datos, $id_forma_pago)
	{
		$this->load->model('Forma_Pago', 'forma', TRUE);
		$repetido = $this->forma->comprobar_registro_repetido($datos);
		if ($repetido > 0) {
			$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-cancel-2\"></span>',
                                title: 'Registro repetido',
                                width: 500,
                                padding: 10,
                                content: '<h2>La forma de pago se encuentra repetida</h2><button onclick=window.location.assign(\"../../listados/listar_formas_pago\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_formas_pago";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
		} else {
			$actualizados = $this->forma->actualizar_forma_pago($datos, $id_forma_pago);

			if ($actualizados > 0) {
					$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-checkmark\"></span>',
                                title: 'Modificación realizada',
                                width: 500,
                                padding: 10,
                                content: '<h2>Se han actualizado los datos de la forma de pago correctamente</h2><button onclick=window.location.assign(\"../../listados/listar_formas_pago\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_formas_pago";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			} else {
				echo "hubo un error al actualizar";
			}
			
		}
		
	}

	private function _cargar_cabecera()
		{
			$this->load->view('header');
		}

	private function _obtener_bovedas()
	{
		$this->load->model('Boveda', 'boveda', TRUE);
		$bovedas = $this->boveda->obtener_todas_bovedas();
		return $bovedas;
	}
}
?>