<?php

/**
* 
*/
class Agregar_Nuevo_Puesto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('descripcion_puesto','descripcion del puesto','required|trim');
		if ($this->form_validation->run()) {
			$datos = array('nombre_puesto' => $this->input->post('descripcion_puesto'));
			$this->load->model('Puesto', 'puesto', TRUE);
			$agregadas = $this->puesto->agregar_nuevo_puesto($datos);
			if ($agregadas != 0) {
				
				$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_puesto'),
						'Volver al catálogo' => site_url('listados/listar_puestos')
					);
				$url_cerrar = site_url();
				$mensaje = $this->load->view('mensajes/ventana_registro_exito', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
				$datos = array('mensaje' => $mensaje);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
				
			} else {
				$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_puesto'),
						'Volver al catálogo' => site_url('listados/listar_puestos')
					);
				$url_cerrar = site_url();
				$mensaje = $this->load->view('mensajes/ventana_registro_fallido', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
				$datos = array('mensaje' => $mensaje);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			}
		} else {
			$this->_cargar_vista();
		}
		
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	public function _cargar_vista()
	{
		$this->load->view('catalogos/agregar_nuevo_puesto');
	}
}
?>