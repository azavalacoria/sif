<?php

/**
* 
*/
class Agregar_Nuevo_Tipo_Movimiento extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('nombre_tipo_cuenta','nombre del tipo de movimiento','required|trim');
		if ($this->form_validation->run()) {
			$aumenta_capital = $this->input->post('aumenta_capital');

			if ($aumenta_capital != 1) {
				$aumenta_capital = 0;
			}

			$datos = array(
					'nombre_tipo_cuenta' => $this->input->post('nombre_tipo_cuenta'),
					'aumenta_capital' => $aumenta_capital
				);

			$this->load->model('Tipo_Cuenta', 'cuenta', TRUE);
			$agregadas = $this->cuenta->agregar_nuevo_tipo_cuenta($datos);
			$this->_mostrar_mensaje($agregadas);
		} else {
			$this->_cargar_vista();
		}
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	public function _cargar_vista()
	{
		$datos = array(
						'url_catalogo_servicio' => site_url('servicios/servicio_tipos_movimientos')
			);
		$this->load->view('catalogos/agregar_nuevo_tipo_movimiento', $datos);
	}

	public function _mostrar_mensaje($agregadas)
	{
		if ($agregadas != 0) {
			$urls = array(
					'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_tipo_movimiento'),
					'Volver al listado' => site_url('listados/listar_tipos_movimientos')
				);
			$url_cerrar = site_url('usuario');
			$mensaje = $this->load->view(
										'mensajes/ventana_registro_exito', 
										array('urls'=> $urls, 'url_cerrar' => $url_cerrar), 
										TRUE);
			$datos = array('mensaje' => $mensaje);
			$this->load->view('mensajes/imprimir_mensaje', $datos);
		} else {
			$urls = array(
					'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_tipo_movimiento'),
					'Volver al índice' => site_url('listados/listar_tipos_movimientos')
				);
			$url_cerrar = site_url('amigo');
			$mensaje = $this->load->view(
									'mensajes/ventana_registro_fallido', 
									array('urls'=> $urls, 'url_cerrar' => $url_cerrar), 
									TRUE);
			$datos = array('mensaje' => $mensaje);
			$this->load->view('mensajes/imprimir_mensaje', $datos);
		}
	}
}
?>