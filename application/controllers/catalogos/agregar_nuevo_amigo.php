<?php

/**
* 
*/
class Agregar_Nuevo_Amigo extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('persona','nombre del amigo','required|trim');
		$this->form_validation->set_rules('secretaria','secretaria','required|trim');
		$this->form_validation->set_rules('nickname','nombre de usuario','required|trim');
		$this->form_validation->set_rules('password1','contraseña','required|trim');
		$this->form_validation->set_rules('password2','confirmar contraseña','required|trim');
		if ($this->form_validation->run()) {
			$datos = array(
					'nickname' => $this->input->post('nickname'),
					'password' => sha1($this->input->post('password1')),
					'persona' => $this->input->post('persona'),
					'secretaria_amigo' => $this->input->post('secretaria')
				);
			$this->load->model('Amigo','amigo',TRUE);
			$agregadas = $this->amigo->agregar_nuevo_amigo($datos);
			if ($agregadas != 0) {

				$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_amigo'),
						'Volver al catálogo' => site_url('listados/listar_amigos')
					);
				$url_cerrar = site_url();
				$mensaje = $this->load->view('mensajes/ventana_registro_exito', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
				$datos = array('mensaje' => $mensaje);
				$this->load->view('mensajes/imprimir_mensaje', $datos);

			} else {
				
				$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_intermediario'),
						'Volver al catálogo' => site_url('listados/listar_intermediarios')
					);
				$url_cerrar = site_url();
				$mensaje = $this->load->view('mensajes/ventana_registro_fallido', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
				$datos = array('mensaje' => $mensaje);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
				
			}
		} else {
			$this->_cargar_vista();
		}
		
		
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	private function _cargar_vista()
	{
		$datos = array(
				'personas' => array('' => 'Seleccione...'),
				'secretarias' => array('' => 'Seleccione...'),
				'url_ent_servicio' => site_url('servicios/obtener_secretarias'),
				'url_servicio' => site_url('servicios/obtener_personas'),
				'url_catalogo_servicio' => site_url('servicios/servicio_amigos/')
			);
		$this->load->view('catalogos/agregar_nuevo_amigo', $datos);
	}
}
?>