<?php

/**
* 
*/
class Modificar_Estado_Concepto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('id_estado_concepto','estado del concepto', 'required');

		if ($this->form_validation->run()) {
			$id_estado_concepto = $this->input->post('id_estado_concepto');

			$this->_cargar_vista_modificacion($id_estado_concepto);
		} else {
			redirect('listados/listar_estados_concepto');
		}
	}

	public function aplicar_cambios()
	{
		$this->form_validation->set_rules('nombre_estado_concepto','nombre del estado del concepto', 'trim|required');

		if ($this->form_validation->run()) {
			$datos = array(
					'nombre_estado_concepto' => $this->input->post('nombre_estado_concepto'),
				);
			$this->_verificar_cambios($datos, $this->input->post('id_estado_concepto'));
		} else {
			$this->_recargar_vista_modificacion($this->input->post('id_estado_concepto'));
		}
		
	}

	public function _cargar_vista_modificacion($id_estado_concepto)
	{
		$this->load->model('Estado_Proyecto', 'estado', TRUE);
		$estado = $this->estado->obtener_estado_para_modificacion($id_estado_concepto);

		$datos = array(
			'estado' => $estado,
			'url_servicio_catalogos' => site_url('servicios/servicio_estados_concepto')
			);

		//$this->load->view('header');
		$this->load->view('catalogos/modificar_estado_concepto', $datos);
	}

	private function _recargar_vista_modificacion($id_estado_concepto)
	{
		if ($id_estado_concepto != 0) {

			$bovedas = $this->_obtener_bovedas();

			$datos = array('bovedas' => $bovedas, 'id_estado_concepto' => $id_estado_concespto);

			$this->load->view('header');
			$this->load->view('catalogos/modificar_forma_pago_recargada', $datos);
		} else {
			redirect('listados/listar_estados_concepto');
		}
	}

	private function _verificar_cambios($datos, $id_estado_concepto)
	{
		$this->load->model('Estado_Proyecto', 'estado', TRUE);
		$repetido = $this->estado->verificar_repetido($id_estado_concepto, $datos['nombre_estado_concepto']);

		$url_cerrar = site_url('usuario');

		if ($repetido > 0) {
			$urls = array(
					'id_estado_concepto' => $id_estado_concepto,
					'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_amigo'),
					'Volver al catálogo' => site_url('listados/listar_amigos')
					);
			$this->_mostrar_mensaje('mensajes/ventana_registro_repetido', $urls, $url_cerrar);
		} else {
			$actualizados = $this->estado->actualizar_estado_concepto($datos, $id_estado_concepto);

			if (is_array($actualizados)) {
				$urls = array(
					'id_estado_concepto' => $id_estado_concepto,
					'Volver al catálogo' => site_url('listados/listar_estados_concepto')
					);
				$this->_mostrar_mensaje('mensajes/ventana_registro_error', $urls, $url_cerrar);
			} else {
				$urls = array('Actualizar' => site_url('listados/listar_estados_concepto'));
				$this->_mostrar_mensaje('mensajes/ventana_registro_exito', $urls, $url_cerrar);
			}
		}
		
	}

	public function _mostrar_mensaje($vista, $urls, $url_cerrar)
	{

		$mensaje = $this->load->view($vista, array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
		$datos = array('mensaje' => $mensaje);
		$this->load->view($vista, $datos);
	}


	private function _cargar_cabecera()
		{
			$this->load->view('header');
		}

	private function _obtener_bovedas()
	{
		$this->load->model('Boveda', 'boveda', TRUE);
		$bovedas = $this->boveda->obtener_todas_bovedas();
		return $bovedas;
	}
}
?>