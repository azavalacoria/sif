<?php

/**
* 
*/
class Agregar_Nueva_Forma_Pago extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('descripcion_forma_pago','descripción','required|trim');
		$this->form_validation->set_rules('boveda','bóveda','required|trim');

		if ($this->form_validation->run()) {
			$datos = array(
					'nombre_forma_pago' => $this->input->post('descripcion_forma_pago'),
					'tipo_boveda' => $this->input->post('boveda')
				);
			$this->load->model('Forma_Pago','forma',TRUE);
			$afectadas = $this->forma->agregar_nueva_forma_pago($datos);
			$url_cerrar = site_url('usuario');
			if ($afectadas != 0) {
				$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nueva_forma_pago'),
						'Volver al catálogo' => site_url('listados/listar_formas_pago')
					);
				$this->_mostrar_mensaje('mensajes/ventana_registro_exito', $urls, $url_cerrar);

			} else {
				$urls = array(
						'Agregar nuevo registro' => site_url('catalogos/agregar_nueva_forma_pago'),
						'Volver al catálogo' => site_url('listados/listar_formas_pago')
					);
				$this->_mostrar_mensaje('mensajes/ventana_registro_fallido', $urls, $url_cerrar);
			}
		} else {
			$this->_cargar_vista();
		}
		
	}

	public function _mostrar_mensaje($vista, $urls, $url_cerrar)
	{
		$mensaje = $this->load->view($vista, array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
		$datos = array('mensaje' => $mensaje);
		$this->load->view('mensajes/imprimir_mensaje', $datos);
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	private function _cargar_vista()
	{
		$this->load->model('Boveda','boveda', TRUE);
		$bovedas = $this->boveda->obtener_todas_bovedas();
		$datos = array('bovedas' => $bovedas);

		$this->load->view('catalogos/agregar_nueva_forma_pago', $datos);
	}
}
?>