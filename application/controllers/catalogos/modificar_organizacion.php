<?php

/**
* 
*/
class Modificar_Organizacion extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();

	}

	public function index()
	{
		$this->form_validation->set_rules('id_organizacion', 'organizacion', 'required');

		if ($this->form_validation->run()) {
			$id_organizacion = $this->input->post('id_organizacion');
			$this->_cargar_vista_modificacion($id_organizacion);
		} else {
			redirect('listados/listar_organizaciones');
		}
	}

	public function aplicar_cambios()
	{
		$this->form_validation->set_rules('nombre','nombre','required|trim');
		$this->form_validation->set_rules('siglas','siglas','trim');
		$this->form_validation->set_rules('descripcion','descripcion','trim|required');
		$this->form_validation->set_rules('direccion','direccion','trim|required');
		$this->form_validation->set_rules('entidad_federativa','entidad federativa','trim|required');
		$this->form_validation->set_rules('municipio','municipio','trim|required');
		$this->form_validation->set_rules('localidad','localidad','trim|required');

		if ($this->form_validation->run()) {
			$id_organizacion = $this->input->post('id_organizacion');
			$datos = array(
					'nombre_organizacion' => $this->input->post('nombre'),
					'siglas' => $this->input->post('siglas'),
					'descripcion' => $this->input->post('descripcion'),
					'direccion' => $this->input->post('direccion'),
					'entidad_federativa' => $this->input->post('entidad_federativa'),
					'municipio' => $this->input->post('municipio'),
					'localidad' => $this->input->post('localidad')
				);
			$this->_verificar_y_aplicar_cambios($id_organizacion, $datos);
		} else {
			$id_organizacion = $this->input->post('id_organizacion');

			if ($id_organizacion != 0) {
				$this->_cargar_vista_modificacion_recargada(
					$id_organizacion,
					$this->input->post('entidad_federativa'),
					$this->input->post('municipio'),
					$this->input->post('localidad')
				);
			} else {
				redirect('listados/listar_organizaciones');
			}
		}
		
	}

	private function _cargar_vista_modificacion($id_organizacion)
	{
		$this->load->model('Organizacion', 'organizacion', TRUE);
		$organizacion = $this->organizacion->obtener_organizacion_modificacion($id_organizacion);

		$entidades_federativas = $this->_obtener_entidades_federativas();
		$municipios = $this->_obtener_municipios($organizacion['entidad_federativa']);
		$localidades = $this->_obtener_localidades($organizacion['municipio']);

		$datos = array(
				'organizacion' => $organizacion,
				'entidades_federativas' => $entidades_federativas,
				'municipios' => $municipios,
				'localidades' => $localidades,
				'url_servicio' => site_url('servicios/obtener_municipios')
			);
		$this->load->view('header');
		$this->load->view('catalogos/modificar_organizacion', $datos);
	}

	private function _cargar_vista_modificacion_recargada($id_organizacion, $entidad_federativa, $municipio, $localidad)
	{
		$entidades_federativas = $this->_obtener_entidades_federativas();

		if ($municipio != 0) {
			$municipios = $this->_obtener_municipios($entidad_federativa);
		} else {
			$municipios = array();
		}
		
		if ($localidad != 0) {
			$localidades = $this->_obtener_localidades($localidad);
		} else {
			$localidades = array();
		}
		
		$datos = array(
				'id_organizacion' => $id_organizacion,
				'entidades_federativas' => $entidades_federativas,
				'municipios' => $municipios,
				'localidades' => $localidades,
				'url_servicio' => site_url('servicios/obtener_municipios')
			);
		$this->load->view('header');
		$this->load->view('catalogos/modificar_organizacion_recargada', $datos);
	}

	private function _verificar_y_aplicar_cambios($id_organizacion, $datos)
	{
		$this->load->model('Organizacion','organizacion', TRUE);
		$repetido = $this->organizacion->comprobar_registro_repetido($datos);
		if ($repetido == 0) {
			$actualizados = $this->organizacion->actualizar_datos($datos, $id_organizacion);
			if ($actualizados > 0){
				$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-checkmark\"></span>',
                                title: 'Modificación realizada',
                                width: 500,
                                padding: 10,
                                content: '<h2>Se modifico correctamente la organización</h2><button onclick=window.location.assign(\"../../listados/listar_organizaciones\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_organizaciones";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			} else {
				echo "nada";
			} 
		} else {
			$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-cancel-2\"></span>',
                                title: 'Registro Repetido',
                                width: 500,
                                padding: 10,
                                content: '<h2>La organización se encuentra repetida</h2><button onclick=window.location.assign(\"../../listados/listar_organizaciones\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_organizaciones";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
		}
		
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	private function _obtener_entidades_federativas()
	{
		$this->load->model('Entidad', 'entidad', TRUE);
		$entidades = $this->entidad->obtener_todas_entidades();
		return $entidades;
	}

	private function _obtener_municipios($entidad_federativa)
	{
		$this->load->model('Municipio', 'municipio', TRUE);
		$municipios = $this->municipio->enlistar_municipios_entidad($entidad_federativa);
		return $municipios;
	}

	private function _obtener_localidades($municipio)
	{
		$this->load->model('Localidad', 'localidad', TRUE);
		$localidades = $this->localidad->enlistar_localidades_municipio($municipio);
		return $localidades;
	}
}
?>