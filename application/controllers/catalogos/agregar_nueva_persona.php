<?php

/**
* 
*/
class Agregar_Nueva_Persona extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->view('header');
	}

	public function index()
	{
		$this->form_validation->set_rules('nombres','nombres','trim|required');
		$this->form_validation->set_rules('apellido_paterno','apellido paterno','trim|required');
		$this->form_validation->set_rules('apellido_materno','apellido materno','trim|required');
		$this->form_validation->set_rules('sexo','sexo','trim|required');
		$this->form_validation->set_rules('direccion','dirección','trim|required');
		$this->form_validation->set_rules('entidad_federativa','entidad federativa','trim|required');
		$this->form_validation->set_rules('municipio','municipio','trim|required');
		$this->form_validation->set_rules('localidad','localidad','trim|required');
		$this->form_validation->set_rules('codigo_postal','código postal','trim|required');
		$this->form_validation->set_rules('curp','CURP','trim|required');
		$this->form_validation->set_rules('telefono','teléfono','trim|required');
		$this->form_validation->set_rules('seccion_electoral','sección electoral','trim|required');
		if ($this->form_validation->run()) {
			$datos = array(
					'nombres' => $this->input->post('nombres'),
					'apellido_paterno' => $this->input->post('apellido_paterno'),
					'apellido_materno' => $this->input->post('apellido_materno'),
					'sexo' => $this->input->post('sexo'),
					'direccion' => $this->input->post('direccion'),
					'codigo_postal' => $this->input->post('codigo_postal'),
					'telefono' => $this->input->post('telefono'),
					'curp' => $this->input->post('curp'),
					'seccion_electoral' => $this->input->post('seccion_electoral'),
					'entidad_federativa' => $this->input->post('entidad_federativa'),
					'municipio' => $this->input->post('municipio'),
					'localidad' => $this->input->post('localidad')
				);
			$this->load->model('Persona','',TRUE);			
			$agregadas = $this->Persona->agregar_nueva_persona($datos);
			$url_cerrar = site_url('usuario');
			if ($agregadas != 0) {
				$urls = array(
					'Agregar nuevo registro' => site_url('catalogos/agregar_nueva_persona'),
					'Volver al catálogo' => site_url('listados/listar_personas')
					);
				$this->_imprimir_mensaje($urls, $url_cerrar, 'mensajes/ventana_registro_exito');
			} else {
				$urls = array(
					'Agregar nuevo registro' => site_url('catalogos/agregar_nueva_persona'),
					'Volver al catálogo' => site_url('listados/listar_personas')
					);
				$this->_imprimir_mensaje($urls, $url_cerrar, 'mensajes/ventana_registro_fallido');
			}
		} else {
			$this->_cargar_vista();
		}
	}

	private function _imprimir_mensaje($urls, $url_cerrar, $vista)
	{
		$mensaje = $this->load->view($vista, array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
		$datos = array('mensaje' => $mensaje);
		$this->load->view('mensajes/imprimir_mensaje', $datos);
	}

	private function _cargar_vista()
	{
		$this->load->model('Entidad','',TRUE);
		$entidades = $this->Entidad->obtener_todas_entidades();
		$municipios = array('' => 'Seleccione...');
		if (isset($_POST['municipio'])) {
			$this->load->model('Municipio', 'municipio', TRUE);
			$municipios = $this->municipio->enlistar_municipios_entidad($_POST['entidad_federativa']);
		}
		$localidades = array('' => 'Seleccione...');
		if (isset($_POST['localidad'])) {
			$this->load->model('Localidad', 'localidad', TRUE);
			$localidades = $this->localidad->enlistar_localidades_municipio($_POST['municipio']);
		}

		$datos = array(
						'entidades' => $entidades, 
						'municipios' => $municipios, 
						'localidades' => $localidades, 
						'url_servicio' => site_url('servicios/obtener_municipios')
						);
		$this->load->view('catalogos/agregar_nueva_persona', $datos);
	}
}

?>