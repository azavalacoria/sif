<?php

/**
* 
*/
class Modificar_Usuario extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();

	}

	public function index()
	{
		$this->form_validation->set_rules('id_usuario','opción','required');
		if ($this->form_validation->run()) {
			
			$this->_obtener_usuario($this->input->post('id_usuario'), 0);
		} else {
			redirect('listados/listar_usuarios');
		}
	}

	public function _obtener_usuario($id_usuario, $recargado)
	{
		if ($id_usuario == 0) {
			redirect('listados/listar_usuarios');
		}
		$usuario = null;
		if ($recargado == 0) {
			$this->load->model('Usuario', 'usuario', TRUE);
			$usuario = $this->usuario->obtener_usuario_modificacion($id_usuario);
		} 
		

		$this->load->model('Puesto', 'puesto', TRUE);
		$puestos = $this->puesto->obtener_todos_puestos();
		$datos = array(
			'usuario' => $usuario, 
			'puestos' => $puestos,
			'url_servicio' => site_url('servicios/obtener_personas')
			);
		//print_r($datos);
		$this->load->view('header');
		$this->load->view('catalogos/modificar_usuario', $datos);
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	public function aplicar_cambios()
	{
		//$this->form_validation->set_rules('id_usuario','opción','required');
		$this->form_validation->set_rules('nombre_usuario','nombre de usuario','trim|required');
		$this->form_validation->set_rules('persona','nombre de la persona','trim|required');
		$this->form_validation->set_rules('puesto','puesto','trim|required');
		$this->form_validation->set_rules('password_confirmado','contraseña','trim|matches[password]');
		$this->form_validation->set_message('matches', 'Los dos campos de contraseña deben coincidir');
		if ($this->form_validation->run()) {
			$id_usuario = $this->input->post('id_usuario');
			$password = $this->input->post('password');
			if ($id_usuario > 0) {
				$usuario_modificado = null;
				if (strlen($password) > 0) {
					$usuario_modificado = array(
						'nombre_usuario' => $this->input->post('nombre_usuario'),
						'persona' => $this->input->post('persona'),
						'puesto' => $this->input->post('puesto'),
						'contrasena' => sha1($password)
						);
				} else {
					$usuario_modificado = array(
						'nombre_usuario' => $this->input->post('nombre_usuario'),
						'persona' => $this->input->post('persona'),
						'puesto' => $this->input->post('puesto')
						);
				}

				$this->load->model('Usuario', 'usuario', TRUE);
				$afectadas = $this->usuario->actualizar_datos_usuario($usuario_modificado, $id_usuario);

				if ($afectadas == 0) {
					$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-cancel-2\"></span>',
                                title: 'Modificación fallida',
                                width: 500,
                                padding: 10,
                                content: '<h2>No se aplico ningún cambio</h2><button onclick=window.location.assign(\"../../listados/listar_usuarios\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_usuarios";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
				} else if ($afectadas <= 1) {
					$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-checkmark\"></span>',
                                title: 'Modificación realizada',
                                width: 500,
                                padding: 10,
                                content: '<h2>Se modifico correctamente los datos del usuario</h2><button onclick=window.location.assign(\"../../listados/listar_usuarios\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_usuarios";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
				} else {
					echo "$afectadas";
				}
			}
			
			//print_r($usuario_modificado);
		} else {
			$this->_obtener_usuario($this->input->post('id_usuario'), 1);
		}
	}
}
?>