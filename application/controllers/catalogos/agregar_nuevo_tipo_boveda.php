<?php

/**
* 
*/
class Agregar_Nuevo_Tipo_Boveda extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('descripcion_boveda','descripcion de la bóveda','required|trim');
		$this->form_validation->set_rules('saldo_inicial','saldo inicial','required|trim|decimal');
		if ($this->form_validation->run()) {
			$saldo_inicial = $this->input->post('saldo_inicial');
			$nombre_boveda = $this->input->post('descripcion_boveda');

			$agregadas = $this->_verificar_y_guardar_boveda($nombre_boveda, $saldo_inicial);
		} else {
			$this->_cargar_vista();
		}
		
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	public function _cargar_vista()
	{
		$this->load->view('catalogos/agregar_nuevo_tipo_boveda');
	}

	private function _verificar_y_guardar_boveda($nombre_boveda, $saldo_actual)
	{
		$datos = array('nombre_boveda' => $nombre_boveda);

		$this->load->model('Boveda', 'boveda', TRUE);
		$repetido = $this->boveda->verificar_repetido($datos);
		$url_cerrar = site_url('usuario');

		if ($repetido == 0) {
			
			$id = $this->boveda->agregar_y_regresar_boveda($datos);
			$this->load->model('Estado_Boveda', 'estado', TRUE);
			$datos = array('tipo_boveda' => $id, 'saldo_actual' => $saldo_actual);
			$agregados = $this->estado->agregar_estado($datos);

			
			if ($agregados > 0) {
				$urls = array('Regresar al listado' => site_url('listados/listar_tipos_bovedas'));
				$this->_mostrar_mensaje($urls, $url_cerrar, 'mensajes/ventana_registro_exito');
			} else {
				$urls = array(
					'No se pudo agregar su bóveda <br> Intentar de nuevo' => site_url('catalogos/agregar_nuevo_tipo_boveda')
					);
				$this->_mostrar_mensaje($urls, $url_cerrar, 'mensajes/ventana');
			}	
		} else {
			$urls = array(
				'Registro Repetido <br> Intentar de nuevo' => site_url('catalogos/agregar_nuevo_tipo_boveda'),
				'Volver al índice' => site_url()
				);
			$this->_mostrar_mensaje($urls, $url_cerrar, 'mensajes/ventana');
		}
	}

	private function _mostrar_mensaje($urls, $url_cerrar, $ventana)
	{
		$datos = array('urls'=> $urls, 'url_cerrar' => $url_cerrar);

		$mensaje = $this->load->view($ventana, $datos, TRUE);

		$datos = array('mensaje' => $mensaje);
		$this->load->view('mensajes/imprimir_mensaje', $datos);
	}
}
?>