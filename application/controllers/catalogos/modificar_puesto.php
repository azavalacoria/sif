<?php

/**
* 
*/
class Modificar_Puesto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->form_validation->set_rules('id_puesto', 'puesto', 'required');

		if ($this->form_validation->run()) {
			$this->_cargar_vista_modificacion($this->input->post('id_puesto'));
		} else {
			redirect('listados/listar_puestos');
		}
		
	}

	public function aplicar_cambios()
	{
		$this->form_validation->set_rules('descripcion', 'descripcion', 'required');

		if ($this->form_validation->run()) {
			$id_puesto = $this->input->post('id_puesto');
			$datos = array('nombre_puesto' => $this->input->post('descripcion'));
			$this->_verificar_y_aplicar_cambios($id_puesto, $datos);
		} else {
			$id_puesto = $this->input->post('id_puesto');

			if ($id_puesto != 0) {
				$this->_cargar_vista_modificacion_recargada($id_puesto);
			} else {
				redirect('listados/listar_puestos');
			}
			
		}
		
	}

	private function _cargar_vista_modificacion($id_puesto)
	{
		$this->load->model('Puesto','puesto', TRUE);
		$puesto = $this->puesto->obtener_puesto($id_puesto);
		$datos = array('puesto' => $puesto);

		$this->load->view('header');
		$this->load->view('catalogos/modificar_puesto', $datos);
	}

	public function _cargar_vista_modificacion_recargada($id_puesto)
	{
		$datos = array('id_puesto' => $id_puesto);

		$this->load->view('header');
		$this->load->view('catalogos/modificar_puesto_recargado', $datos);
	}

	private function _verificar_y_aplicar_cambios($id_puesto, $datos)
	{
		$this->load->model('Puesto', 'puesto', TRUE);

		$repetido = $this->puesto->verificar_dato_repetido($datos);

		if ($repetido == 0) {
			$actualizados = $this->puesto->actualizar_puesto($id_puesto, $datos);

			if ($actualizados > 0) {
				echo "$actualizados registros han sido actualizados";
			} else {
				echo "no se pudo actualizar";
			}
		} else {
			echo "ya existe un dato con esas caracteristicas";
		}
		
	}
}
?>