<?php

/**
* 
*/
class Agregar_Nueva_Organizacion extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('nombre_organizacion','nombre de la organizacion','required|trim');
		$this->form_validation->set_rules('siglas_organizacion','siglas de la organizacion','required|trim');
		$this->form_validation->set_rules('direccion_organizacion','dirección de la organizacion','required|trim');
		$this->form_validation->set_rules('descripcion_organizacion','descripcion de la organizacion','required|trim');
		$this->form_validation->set_rules('entidad_federativa','entidad federativa','required|trim');
		$this->form_validation->set_rules('municipio','municipio','required|trim');
		$this->form_validation->set_rules('localidad','localidad','required|trim');

		if ($this->form_validation->run()) {
			$datos = array(
					'nombre_organizacion' => $this->input->post('nombre_organizacion'),
					'siglas' => $this->input->post('siglas_organizacion'),
					'descripcion' => $this->input->post('descripcion_organizacion'),
					'direccion' => $this->input->post('direccion_organizacion'),
					'entidad_federativa' => $this->input->post('entidad_federativa'),
					'municipio' => $this->input->post('municipio'),
					'localidad' => $this->input->post('localidad')
				);

			$this->load->model('Organizacion','organizacion',TRUE);
			$agregadas = $this->organizacion->agregar_nueva_organizacion($datos);
			$url_cerrar = site_url('usuario');
			if ($agregadas != 0) {
				$urls = array(
					'Agregar nuevo registro' => site_url('catalogos/agregar_nueva_organizacion'),
					'Volver al catálogo' => site_url('listados/listar_organizaciones')
					);
				$this->_mostrar_mensaje('mensajes/ventana_registro_exito', $urls, $url_cerrar);
			} else {
				$urls = array(
					'Agregar nuevo registro' => site_url('catalogos/agregar_nueva_organizacion'),
					'Volver al catálogo' => site_url('listados/listar_organizaciones')
					);
				$this->_mostrar_mensaje('mensajes/ventana_registro_fallido', $urls, $url_cerrar);
				
			}
		} else {
			$this->_cargar_vista();
		}
	}

	public function _mostrar_mensaje($vista, $urls, $url_cerrar)
	{
		$mensaje = $this->load->view($vista, array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
		$datos = array('mensaje' => $mensaje);
		$this->load->view('mensajes/imprimir_mensaje', $datos);
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	private function _cargar_vista()
	{
		$this->load->model('Entidad','',TRUE);
		$datos['entidades'] = $this->Entidad->obtener_todas_entidades();
		$datos['url_servicio'] = site_url('servicios/obtener_municipios');
		$this->load->view('catalogos/agregar_nueva_organizacion',$datos);
	}
}
?>