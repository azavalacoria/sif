<?php

/**
* 
*/
class Agregar_Nuevo_Ente_Generador extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('persona','responsable','required|trim');
		$this->form_validation->set_rules('secretaria','secretaria','required|trim');
		if ($this->form_validation->run()) {
			$datos = array(
					'titular' => $this->input->post('persona'),
					'secretaria' => $this->input->post('secretaria')
				);
			$this->load->model('Ente_Generador','ente',TRUE);
			$agregadas = $this->ente->agregar_nuevo_ente($datos);
			$this->_mostrar_mensaje($agregadas);
		} else {
			$this->_cargar_vista();
		}
		
		
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	private function _cargar_vista()
	{
		$datos = array(
				'personas' => array('' => 'Seleccione...'),
				'secretarias' => array('' => 'Seleccione...'),
				'url_ent_servicio' => site_url('servicios/obtener_secretarias'),
				'url_personas_servicio' => site_url('servicios/obtener_personas'),
				'url_catalogo_servicio' => site_url('servicios/servicio_entes_generadores')
			);
		$this->load->view('catalogos/agregar_nuevo_ente_generador', $datos);
	}

	public function _mostrar_mensaje($agregadas)
	{
		if ($agregadas != 0) {
				
			$urls = array(
					'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_ente_generador'),
					'Volver al índice' => site_url('listados/listar_entes_generadores')
				);
			$url_cerrar = site_url('usuario');
			$mensaje = $this->load->view(
										'mensajes/ventana_registro_exito', 
										array('urls'=> $urls, 'url_cerrar' => $url_cerrar), 
										TRUE);
			$datos = array('mensaje' => $mensaje);
			$this->load->view('mensajes/imprimir_mensaje', $datos);

		} else {
			
			$urls = array(
					'Agregar nuevo registro' => site_url('catalogos/agregar_nuevo_ente_generador'),
					'Volver al catálogo' => site_url('listados/listar_entes_generadores')
				);
			$url_cerrar = site_url();
			$mensaje = $this->load->view(
										'mensajes/ventana_registro_fallido', 
										array('urls'=> $urls, 'url_cerrar' => $url_cerrar), 
										TRUE);
			$datos = array('mensaje' => $mensaje);
			$this->load->view('mensajes/imprimir_mensaje', $datos);

		}
	}
}
?>