<?php

/**
* 
*/
class Agregar_Nueva_Secretaria extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('nombre_secretaria','nombre de la secretaria','required|trim');
		$this->form_validation->set_rules('siglas_secretaria','siglas de la secretaria','required|trim');
		$this->form_validation->set_rules('direccion_secretaria','dirección de la secretaria','required|trim');
		$this->form_validation->set_rules('entidad_federativa','entidad federativa','required|trim');
		$this->form_validation->set_rules('municipio','municipio','required|trim');
		$this->form_validation->set_rules('localidad','localidad','required|trim');

		if ($this->form_validation->run()) {
			$datos = array(
					'nombre_secretaria' => $this->input->post('nombre_secretaria'),
					'siglas' => $this->input->post('siglas_secretaria'),
					'direccion' => $this->input->post('direccion_secretaria'),
					'entidad_federativa' => $this->input->post('entidad_federativa'),
					'municipio' => $this->input->post('municipio'),
					'localidad' => $this->input->post('localidad')
				);
			$this->load->model('Secretaria','secretaria',TRUE);
			$agregadas = $this->secretaria->agregar_nueva_secretaria($datos);
			$url_cerrar = site_url('usuario');
			if ($agregadas != 0) {
				$urls = array(
					'Agregar nuevo registro' => site_url('catalogos/agregar_nueva_secretaria'),
					'Volver al catálogo' => site_url('listados/listar_secretarias')
					);
				$this->_imprimir_mensaje($urls, $url_cerrar, 'mensajes/ventana_registro_exito');
			} else {

				$urls = array(
					'Agregar nuevo registro' => site_url('catalogos/agregar_nueva_secretaria'),
					'Volver al catálogo' => site_url('listados/listar_secretarias')
					);
				$this->_imprimir_mensaje($urls, $url_cerrar, 'mensajes/ventana_registro_fallido');
				
			}
		} else {
			$this->_cargar_vista();
		}
	}

	private function _imprimir_mensaje($urls, $url_cerrar, $vista)
	{
		$mensaje = $this->load->view($vista, array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
		$datos = array('mensaje' => $mensaje);
		$this->load->view('mensajes/imprimir_mensaje', $datos);
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	private function _cargar_vista()
	{
		$this->load->model('Entidad','',TRUE);
		$datos['entidades'] = $this->Entidad->obtener_todas_entidades();
		$datos['url_servicio'] = site_url('servicios/obtener_municipios');
		$this->load->view('catalogos/agregar_nueva_secretaria',$datos);
	}
}
?>