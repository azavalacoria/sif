<?php

/**
* 
*/
class Modificar_Intermediario extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_cargar_cabecera();
	}

	public function index()
	{
		$this->form_validation->set_rules('id_intermediario','intermediario','required');

		if ($this->form_validation->run()) {
			$this->_cargar_formulario_inicial($this->input->post('id_intermediario'));
		} else {
			redirect('listados/listar_intermediarios');
		}
		
	}

	public function aplicar_cambios()
	{
		$this->form_validation->set_rules('persona', 'Nombre del Intermediario','required');
		$this->form_validation->set_rules('organizacion','organizacion a la que representa','required');

		if ($this->form_validation->run()) {

			$id_intermediario = $this->input->post('id_intermediario');
			$intermediario = array(
					'persona' => $this->input->post('persona'),'organizacion' => $this->input->post('organizacion')
					);

			$repetido = $this->_verificar_cambios($intermediario);

			if ($repetido == 0) {
				$actualizados = $this->_guardar_cambios($intermediario, $id_intermediario);
				$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-checkmark\"></span>',
                                title: 'Modificación realizada',
                                width: 500,
                                padding: 10,
                                content: '<h2>Se han actualizado los datos del intermediario correctamente</h2><button onclick=window.location.assign(\"../../listados/listar_intermediarios\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_intermediarios";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			} else {
				$mensaje = "<script>
                    $(function(){
                            $.Dialog({
                                icon: '<span class=\"icon-cancel-2\"></span>',
                                title: 'Registro repetido',
                                width: 500,
                                padding: 10,
                                content: '<h2>Ya existe un intermediario con los mismos datos</h2><button onclick=window.location.assign(\"../../listados/listar_intermediarios\")>Regresar</button>'
                            });
                    });
                </script>";
				$url ="../../listados/listar_intermediarios";
				$datos = array(
					'mensaje' => $mensaje,
					'url' => $url
				);
				$this->load->view('mensajes/imprimir_mensaje', $datos);
			}
		} else {
			redirect('listados/listar_intermediarios');
		}
		
	}

	private function _cargar_cabecera()
	{
		$this->load->view('header');
	}

	public function _cargar_formulario_inicial($id)
	{
		$this->load->model('Intermediario', 'intermediario', TRUE);
		$intermediario = $this->intermediario->obtener_intermediario($id);

		$datos = array(
				'intermediario' => $intermediario,
				'url_org_servicio' => site_url('servicios/obtener_organizaciones'),
				'url_servicio' => site_url('servicios/obtener_personas')
			);
		$this->load->view('header');
		$this->load->view('catalogos/modificar_intermediario', $datos);
	}

	public function _verificar_cambios($datos)
	{
		$this->load->model('Intermediario', 'intermediario', TRUE);
		$repetido = $this->intermediario->comprobar_registro_repetido($datos);
		return $repetido;
	}

	public function _guardar_cambios($datos, $id_intermediario)
	{
		$this->load->model('Intermediario', 'intermediario', TRUE);
		$actualizados = $this->intermediario->actualizar_intermediario($datos, $id_intermediario);
		return $actualizados;
	}
}
?>