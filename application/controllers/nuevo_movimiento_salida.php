<?php

/**
* 
*/
class Nuevo_Movimiento_Salida extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->view('header');
	}

	public function index()
	{
		$this->form_validation->set_rules('tipo_cuenta','tipo de cuenta','trim|required');
		$this->form_validation->set_rules('fecha','fecha de pago','trim|required');
		$this->form_validation->set_rules('concepto','concepto','trim|required');
		$this->form_validation->set_rules('monto','importe','trim|required|decimal');
		$this->form_validation->set_rules('forma_pago','forma de pago','trim|required');

		if ($this->form_validation->run()) {
			$fecha = new DateTime($this->input->post('fecha'));
			$hora = date('H:i:s');
			$fecha_pago = $fecha->format('Y-m-d')." ".$hora;
			$gasto = $this->_verificar_gasto($this->input->post('gasto_comprobar'));
			$datos = array(
					'fecha_pago' => $fecha_pago,
					'monto' => $this->input->post('monto'),
					'concepto' => $this->input->post('concepto'),
					'forma_pago' => $this->input->post('forma_pago'),
					'tipo_cuenta' => $this->input->post('tipo_cuenta'),
					'gasto_comprobar' => $gasto,
					'responsable' => $this->input->post('persona')
				);
			$this->_agregar_nuevo_movimiento($datos);
		} else {
			$this->_cargar_vista();
		}
	}

	private function _agregar_nuevo_movimiento($datos)
	{
		$this->load->model('Movimiento','movimiento', TRUE);
		$agregados = $this->movimiento->agregar_nuevo_movimiento($datos);

		$id_boveda = $this->_obtener_tipo_boveda($datos['forma_pago']);
		$aumenta_capital = $this->_obtener_tipo_operacion($datos['tipo_cuenta']);

		$actualizados = $this->_actualizar_boveda($datos['monto'], $id_boveda, $aumenta_capital);
		if ($actualizados == 0) {
		 	$urls = array(
						'Intentar de nuevo' => site_url('nuevo_movimiento_salida'),
						'Volver al índice' => site_url('usuario')
					);
		 	$url_cerrar = site_url('usuario');
		 	$this->_mostrar_mensaje('mensajes/ventana_registro_fallido', $urls, $url_cerrar);
		} else {
		 	$urls = array(
						'Realizar otro movimiento' => site_url('nuevo_movimiento_salida'),
						'Volver al índice' => site_url('usuario')
					);
		 	$url_cerrar = site_url('usuario');
		 	$this->_mostrar_mensaje('mensajes/ventana_registro_exito', $urls, $url_cerrar);
		}
		
	}

	private function _mostrar_mensaje($vista, $urls, $url_cerrar)
	{
		$mensaje = $this->load->view('mensajes/ventana_registro_exito', array('urls'=> $urls, 'url_cerrar' => $url_cerrar), TRUE);
		$datos = array('mensaje' => $mensaje);
		$this->load->view('mensajes/imprimir_mensaje', $datos);
	}

	public function _actualizar_boveda($monto , $id_boveda, $aumenta_capital)
	{
		$id = $this->_obtener_tipo_boveda($id_boveda);
		$this->load->model('Estado_Boveda','estado',TRUE);

		$actualizadas = $this->estado->actualizar_saldo($id_boveda, $monto, $aumenta_capital);

		return $actualizadas;
	}

	public function _obtener_tipo_boveda($forma_pago)
	{
		$this->load->model('Forma_Pago', 'forma', TRUE);
		return $this->forma->obtener_id_boveda($forma_pago);
	}

	public function _obtener_tipo_operacion($id_tipo_cuenta)
	{
		$this->load->model('Tipo_Cuenta', 'cuenta', TRUE );
		$aumenta = $this->cuenta->aumenta_capital($id_tipo_cuenta);
		return $aumenta;
	}

	public function _verificar_gasto($gasto)
	{
		if (isset($gasto)) {
			return $gasto;
		} else {
			return '';
		}
	}

	private function _cargar_vista()
	{
		$this->load->view('header');

		$this->load->model('Concepto', 'concepto', TRUE);
		$cuenta = $this->input->post('tipo_cuenta');

		if (isset($cuenta)) {
			$this->load->model('Concepto', 'concepto', TRUE);
			$conceptos = $this->concepto->listar_conceptos_por_tipo_cuenta($cuenta);
		} else {
			$conceptos = array('' => 'Seleccione...');
		}

		$this->load->model('Forma_Pago','formas', TRUE);
		$formas = $this->formas->obtener_todas_formas_pago();
		$this->load->model('Tipo_Cuenta', 'cuenta', TRUE);
		$cuentas = $this->cuenta->listar_tipos_cuenta_salida();

		$datos = array(
			'conceptos' => $conceptos, 
			'formas' => $formas, 
			'cuentas' => $cuentas, 
			'url_servicio_pagos' => site_url('servicios/obtener_conceptos'),
			'url_servicio' => site_url('servicios/obtener_personas')
			);
		$this->load->view('nuevo_movimiento_salida', $datos);
	}

}
?>