<body class="metro">
	<div class="grid" style="padding-left: 15%;">
		<div class="row">
			<h1 class="tile-area-title fg-white">Listado de Movimientos</h1>
			<br>
			<div class="span2"></div>
				<div class="span8">
					<?php echo form_open('modificar_movimiento'); ?>
					<div class="text-right">
						<?php echo form_button(
									array(
										'class'=>'icon-plus-2 large success',
										'value' => 'Agregar',
										'title' => 'Agregar',
										'onclick' => "window.location.assign('".site_url('usuario')."');"
									)
								); ?>
						<?php echo form_button(
									array(
										'class'=>'large icon-pencil btn warning',
										'value' => 'Modificar',
										'title' => 'Modificar',
										'onclick' => "window.location.assign('".site_url('usuario')."');"
									)
								); ?>
						<?php echo form_button(
									array(
										'name' => 'cancelar',
										'class'=>'large  icon-home danger cancelar',
										'value' => 'Cancelar',
										'title' => 'Inicio',
										'onclick' => "window.location.assign('".site_url('usuario')."');"
									)
								); ?>
							<br><br>
					</div>
					<table class="table striped bordered hovered">
						<thead>
							<tr>
								<th>Opcion</th>
								<th>Concepto</th>
								<th>Fecha del Movimiento</th>
								<th>Monto de la Operación</th>
								<th>Tipo de Movimiento</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($movimientos as $movimiento) { ?>
							<tr>
								<td>
									<div class="input-control radio">
										<label>
											<?php echo form_radio(
														array(
															'name' => 'id_movimiento',
															'value' => $movimiento['id_movimiento'],
															'group' => 'movimientos'
															)
														);
													?>
											<span class="check"></span>
										</label>
									</div>
								</td>
								<td><?php echo $movimiento['concepto']; ?></td>
								<td>
									<p class="text-center">
										<?php echo $movimiento['fecha']; ?>
									</p>
								</td>
								<td>
									<p class="text-center">
										<?php printf('$ %.2f', $movimiento['monto']) ; ?>
									</p>
								</td>
								<td>
									<p class="text-center">
										<?php echo $movimiento['cuenta']; ?>
									</p>
									
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
					<?php echo form_close(); ?>
					<div>
						<?php echo form_open('listados/listar_movimientos'); ?>

						<?php echo form_button(
										array(
											'name' => 'opcion',
											'class' => 'success',
											'value' => 'dec',
											'type' => 'submit',
											'content' => 'Anteriores'
										)
									); ?>
						<?php echo form_button(
										array(
											'name' => 'opcion',
											'class' => 'inverse',
											'value' => 'inc',
											'type' => 'submit',
											'content' => 'Siguientes'
										)
									); ?>
						<?php echo form_close(); ?>
					</div>
				</div><!-- fin span8-->
			<div class="span2"></div>
		</div><!-- fin row-->
	</div><!-- fin grid-->
</body>