<body class="metro">
	<script type="text/javascript" src="<?php echo base_url('js/inventarios/cambiar_estado_elemento_catalogo.js') ?>"></script>
	<div class="grid" style="padding-left: 15%;">
		<div class="row">
			<h1 class="tile-area-title fg-white">Listado de Organizaciones</h1>
			<br>
			<div class="span2"></div>
				<div class="span8">
					<?php echo form_open('catalogos/modificar_organizacion'); ?>
					<div class="text-right">
						<?php echo form_input(
									array(
										'id' => 'url_servicio',
										'name' => 'url_servicio',
										'type' => 'hidden',
										'value' => $url_servicio,
									)
								); ?>
						<?php echo form_input(
									array(
										'id' => 'modelo',
										'name' => 'modelo',
										'type' => 'hidden',
										'value' => $modelo,
									)
								); ?>
						<?php echo form_button(
								array(
									'class'=>'icon-plus-2 large success',
									'value' => 'Agregar',
									'title' => 'Agregar',
									'onclick' => "window.location.assign('".site_url('catalogos/agregar_nueva_organizacion')."');"
								)
							); ?>
						<?php echo form_button(
								array(
									'class'=>'large icon-pencil btn warning',
									'value' => 'Modificar',
									'title' => 'Modificar',
									'type' => 'submit'
									)
								);
								?>
						<?php echo form_button(
								array(
									'name' => 'cancelar',
									'class'=>'large  icon-home danger cancelar',
									'value' => 'Cancelar',
									'title' => 'Inicio',
									'onclick' => "window.location.assign('".site_url('usuario')."');"
									)
								);
							?><br><br>
					</div>
						<table class="table striped bordered hovered">
							<tr>
								<th>Número</th>
								<th>Nombre</th>
								<th>Descripcion</th>
								<th>Dirección</th>
								<th>Activo</th>
							</tr>
						<?php foreach ($organizaciones as $organizacion) { ?>
							<tr>
								<td>
									<div class="input-control radio">
										<label>
											<?php echo form_radio(
												array(
													'name' => 'id_organizacion',
													'value' => $organizacion['id_organizacion'],
													'group' => 'organizacion'
													)
												);
											?>
											<span class="check"></span>
										</label>
									</div>
								</td>
								<td>
									<?php
									if ( strlen( $organizacion['siglas'] ) > 1 ) {
										echo $organizacion['nombre'].' ('.$organizacion['siglas'].')';
									} else {
										echo $organizacion['nombre'];
									}
									?>
								</td>
								<td><?php echo $organizacion['descripcion']; ?></td>
								<td><?php echo $organizacion['direccion']; ?></td>
								<th>
									<?php
									if ($organizacion['activo'] == 1) {
										echo form_checkbox(
												$datos = array(
													'id' => $organizacion['id_organizacion'],
													'name' => 'activo',
													'value' => '0',
													'checked' => TRUE,
													'onClick' => 'cambia_estado(this.id, this.value);'
												)
											);
									} else {
										echo form_checkbox(
												$datos = array(
													'id' => $organizacion['id_organizacion'],
													'name' => 'activo',
													'value' => '1',
													'checked' => FALSE,
													'onClick' => 'cambia_estado(this.id, this.value);'
												)
											);
									}
									?>
								</th>
							</tr>
						<?php } ?>
						</table>
						<?php echo form_close(); ?>
					<div>
						<?php echo form_open('listados/listar_organizaciones') ?>
						<?php
						echo form_button(
								array(
										'name' => 'opcion',
										'class' => 'success',
										'value' => 'dec',
										'type' => 'submit',
										'content' => 'Anteriores'
									)
							);
						?>
						<?php
						echo form_button(
								array(
										'name' => 'opcion',
										'class' => 'inverse',
										'value' => 'inc',
										'type' => 'submit',
										'content' => 'Siguientes'
									)
							);
						?>
					</div>
					<?php echo form_close(); ?>
			</div><!-- fin span8-->
				<div class="span2"></div>
			</div><!-- fin row-->
	</div><!-- fin grid-->
</body>