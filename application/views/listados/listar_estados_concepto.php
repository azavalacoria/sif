<body class="metro">
	<script src="<?php echo base_url('js/inventarios/cambiar_estado_elemento_catalogo.js') ?>"></script>
	<div class="tile-area tile-alrea-darkTeal">
		<div class="grid fluid">
			<!-- inicio row -->
			<div class="row"> 
				<div class="span8 offset2">
					<p class="header fg-white">
						Listado de Estados de Concepto
					</p>
				</div>
				<div class="span3">
					<div class="user-id">
						<div class="user-id-image">
							<span class="icon-user no-display1"></span>
							<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
						</div>
						<div class="user-id-name">
							<span class="first-name">Usuario</span>
							<span class="last-name">Rol</span>
						</div>
					</div>
				</div>
			</div>
			<!-- fin row -->
			
			<div class="row">
				<ul>
					<?php echo validation_errors('<li class="error-li">','</li>'); ?>
				</ul>
			</div>
			
			<div class="row">
				<?php echo form_input(
					array(
						'id' => 'url_servicio',
						'name' => 'url_servicio',
						'type' => 'hidden',
						'value' => $url_servicio,
					)); ?>
				<?php echo form_input(
					array(
						'id' => 'modelo',
						'name' => 'modelo',
						'type' => 'hidden',
						'value' => $modelo,
					)); ?>
				<?php echo form_open('catalogos/modificar_estado_concepto', array('id' => 'modify-form')); ?>
				<div class="span8 offset2">
					<div class="text-right">
						<?php echo form_button(
								array(
									'class'=>'icon-plus-2 large success',
									'value' => 'Agregar',
									'title' => 'Agregar',
									'onclick' => "window.location.assign('".site_url('catalogos/agregar_nuevo_estado_concepto')."');"
									)
								); ?>
						<?php echo form_button(
								array(
									'class'=>'large icon-pencil btn warning',
									'value' => 'Modificar',
									'title' => 'Modificar',
									'type' => 'submit'
									)
								); ?>
						<?php echo form_button(
							array(
								'name' => 'cancelar',
								'class'=>'large  icon-home danger cancelar',
								'value' => 'Cancelar',
								'title' => 'Inicio',
								'onclick' => "window.location.assign('".site_url('usuario')."');"
								)
							); ?><br><br>
					</div>
					<table class="table striped bordered hovered">
						<tr>
								<th>Opcion</th>
								<th>Nombre del estado</th>
								<th>Activo</th>
							</tr>
							<?php foreach ($estados as $estado) { ?>
							<tr>
								<td class="text-center">
									<div class="input-control radio default-style">
										<label>
											<?php echo form_radio(
												array(
													'name' => 'id_estado_concepto',
													'value' => $estado['id_estado_concepto'],
													'group' => 'estados'
													)
												);
											?>
											<span class="check"></span>
										</label>
									</div>
								</td>
								<td>
									<p class="readable-text offset1">
										<?php echo $estado['nombre_estado_concepto']; ?>
									</p>
								</td>
								<td class="text-center">
									<div class="input-control checkbox">
										<label class="text-left">
											<?php
											if ($estado['activo'] == 1) {
												echo form_checkbox(
														array(
															'id' => $estado['id_estado_concepto'],
															'name' => 'activo',
															'value' => '0',
															'checked' => TRUE,
															'onClick' => 'cambia_estado(this.id, this.value);'
														)
													);
											} else {
												echo form_checkbox(
														array(
															'id' => $estado['id_estado_concepto'],
															'name' => 'activo',
															'value' => '1',
															'checked' => FALSE,
															'onClick' => 'cambia_estado(this.id, this.value);'
														)
													);
											}
											?>
											<span class="check"></span>
											
										</label>
									</div>
								</td>
							</tr>
							<?php } ?>
					</table>
					<?php echo form_close(); ?>
				</div><!-- fin span8-->
			</div>
			<!-- fin row tabla-->

			<!-- inicio row botones paginacion -->
			<div class="row">
				<div class="span8 offset2">
					<?php echo form_open('listados/listar_formas_pago'); ?>
					<?php echo form_button(
						array(
							'name' => 'opcion',
							'class' => 'success',
							'value' => 'dec',
							'type' => 'submit',
							'content' => 'Anteriores'
						)
						); ?>
					<?php echo form_button(
						array(
							'name' => 'opcion',
							'class' => 'inverse',
							'value' => 'inc',
							'type' => 'submit',
							'content' => 'Siguientes'
						)
						); ?>
					<?php echo form_close(); ?>
				</div>
			</div>
			<!-- fin row botones paginacion -->
	</div><!-- fin grid-->
	</div>

</body>