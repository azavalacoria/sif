<body class="metro">
	<div class="grid" style="padding-left: 15%;">
		<div class="row">
			<h1 class="tile-area-title fg-white">Listado de Personas</h1>
			<br>
			<div class="span2"></div>
				<div class="span8">
					<div class="text-right">
					<?php echo form_button(
									array(
										'class'=>'icon-plus-2 large success',
										'value' => 'Agregar',
										'title' => 'Agregar',
										'onclick' => "window.location.assign('".site_url('catalogos/agregar_nueva_persona')."');"
									)
								); ?>
							<?php echo form_button(
								array(
									'name' => 'cancelar',
									'class'=>'large  icon-home danger cancelar',
									'value' => 'Cancelar',
									'title' => 'Inicio',
									'onclick' => "window.location.assign('".site_url('usuario')."');"
									)
								);
							?><br><br>
						</div>
							<table class="table striped bordered hovered">
								<tr>
									<th>Opcion</th>
									<th>Nombre de la Persona</th>
								</tr>
								<?php foreach ($personas as $persona) { ?>
								<tr>
									<td class="text-center">
										<div class="input-control radio default-style">
										<label>
											<?php echo form_radio(
												array(
													'name' => 'id_organizacion',
													'value' => $persona['id_persona'],
													'group' => 'organizacion'
													)
												);
											?>
											<span class="check"></span>
										</label>
									</td>
									<td>
										<p class="readable-text">
											<?php echo $persona['nombres']; ?>
										</p>
									</td>
								</tr>
								<?php } ?>
							</table>

						<div>
							<?php echo form_open('listados/listar_personas'); ?>
							<?php
							echo form_button(
									array(
											'name' => 'opcion',
											'class' => 'success',
											'value' => 'dec',
											'type' => 'submit',
											'content' => 'Anteriores'
										)
								);
							?>
							<?php
							echo form_button(
									array(
											'name' => 'opcion',
											'class' => 'inverse',
											'value' => 'inc',
											'type' => 'submit',
											'content' => 'Siguientes'
										)
								);
							?>
							<?php echo form_close(); ?>
						</div>
						</div><!-- fin span8-->
											<div class="span2"></div>
										</div><!-- fin row-->
								</div><!-- fin grid-->

</body>