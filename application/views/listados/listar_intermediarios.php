<body class="metro">
	<script type="text/javascript" src="<?php echo base_url('js/inventarios/cambiar_estado_elemento_catalogo.js') ?>"></script>
	<div class="grid" style="padding-left: 15%;">
		<div class="row">
			<h1 class="tile-area-title fg-white">Listado de Intermediarios</h1>
			<br>
			<div class="span2"></div>
				<div class="span8" id="table-grid">
					<div class="text-right">
						<?php echo form_open('catalogos/modificar_intermediario', array('id'=> 'listado')); ?>
						<?php echo form_input(
									array(
										'id' => 'url_servicio',
										'name' => 'url_servicio',
										'type' => 'hidden',
										'value' => $url_servicio,
									)
								); ?>
						<?php echo form_input(
									array(
										'id' => 'modelo',
										'name' => 'modelo',
										'type' => 'hidden',
										'value' => $modelo,
									)
								); ?>
						<?php echo form_button(
							array(
								'class'=>'icon-plus-2 large success',
								'value' => 'Agregar',
								'title' => 'Agregar',
								'onclick' => "window.location.assign('".site_url('catalogos/agregar_nuevo_intermediario')."');"
								)
							);
						?>
						<?php echo form_button(
							array(
								'class'=>'large icon-pencil btn warning',
								'value' => 'Modificar',
								'title' => 'Modificar',
								'type' => 'submit'
								)
							);
						?>
						<?php echo form_button(
							array(
								'name' => 'cancelar',
								'class'=>'large  icon-home danger cancelar',
								'value' => 'Cancelar',
								'title' => 'Inicio',
								'onclick' => "window.location.assign('".site_url('usuario')."');"
								)
							);
						?><br><br>
					</div>
					<table class="table striped bordered hovered">
						
						<thead>
							<tr>
								<th>Opcion</th>
								<th>Nombre del Intermediario</th>
								<th>Organizacion</th>
								<th>Activo</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($intermediarios as $intermediario) { ?>
							<tr>
								<td>
									<div class="input-control radio">
										<label>
											<?php echo form_radio(
												array(
													'name' => 'id_intermediario',
													'value' => $intermediario['id_intermediario'],
													'group' => 'intermediarios'
													)
												);
											?>
											<span class="check"></span>
										</label>
									</div>
								</td>
								<td>
									<?php echo $intermediario['nombre']; ?>
								</td>
								<td>
									<?php echo $intermediario['organizacion']; ?>
								</td>
								<td>
									<?php
									if ($intermediario['activo'] == 1) {
										echo form_checkbox(
												$datos = array(
													'id' => $intermediario['id_intermediario'],
													'name' => 'activo',
													'value' => '0',
													'checked' => TRUE,
													'onClick' => 'cambia_estado(this.id, this.value);'
												)
											);
									} else {
										echo form_checkbox(
												$datos = array(
													'id' => $intermediario['id_intermediario'],
													'name' => 'activo',
													'value' => '1',
													'checked' => FALSE,
													'onClick' => 'cambia_estado(this.id, this.value);'
												)
											);
									}
									?>
								</td>
							</tr>
							<?php } ?>
						</tbody>
						<?php echo form_close(); ?>
					</table>
					<div>
						<?php echo form_open('listados/listar_intermediarios'); ?>
						<?php echo form_button(
							array(
								'name' => 'opcion',
								'class' => 'success',
								'value' => 'dec',
								'type' => 'submit',
								'content' => 'Anteriores'
								)
							);
						?>
						<?php echo form_button(
							array(
								'name' => 'opcion',
								'class' => 'inverse',
								'value' => 'inc',
								'type' => 'submit',
								'content' => 'Siguientes'
								)
							);
						?>
						<?php echo form_close(); ?>
					</div>
				</div><!-- fin span8-->
				<div class="span2"></div>
			</div><!-- fin row-->
		</div><!-- fin grid-->
</body>