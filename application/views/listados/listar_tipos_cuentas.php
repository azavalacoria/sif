<body class="metro">
	<script type="text/javascript" src="<?php echo base_url('js/inventarios/cambiar_estado_elemento_catalogo.js') ?>"></script>
	<div class="grid" style="padding-left: 15%;">
		<div class="row">
			<h1 class="tile-area-title fg-white">Listado de Tipos de Cuentas</h1>
			<br>
			<div class="span2"></div>
				<div class="span8">
					<div class="text-right">
						<?php echo form_open('catalogos/modificar_tipo_cuenta', array('id' => 'modify-form')); ?>
						<?php echo form_input(
							array(
								'id' => 'url_catalogo_servicio',
								'name' => 'url_catalogo_servicio',
								'type' => 'hidden',
								'value' => $url_catalogo_servicio,
								)
							); ?>
						<?php echo form_input(
							array(
								'id' => 'modelo',
								'name' => 'modelo',
								'type' => 'hidden',
								'value' => $modelo,
								)
							); ?>
						<?php echo form_button(
							array(
								'class'=>'icon-plus-2 large success',
								'value' => 'Agregar',
								'title' => 'Agregar',
								'onclick' => "window.location.assign('".site_url('catalogos/agregar_nuevo_tipo_concepto')."');"
								)
							); ?>
						<?php echo form_button(
							array(
								'class'=>'large icon-pencil btn warning',
								'value' => 'Modificar',
								'title' => 'Modificar',
								'type' => 'submit'
								)
							); ?>
						<?php echo form_button(
							array(
								'name' => 'cancelar',
								'class'=>'large  icon-home danger cancelar',
								'value' => 'Cancelar',
								'title' => 'Inicio',
								'onclick' => "window.location.assign('".site_url('usuario')."');"
								)
							); ?><br><br>
						</div>
						<table class="table striped bordered hovered">
							<tr>
								<th>Opcion</th>
								<th>Nombre</th>
								<th>Aumenta Capital</th>
								<th>Activo</th>
							</tr>
							<?php foreach ($cuentas as $cuenta) { ?>
							<tr>
								<td>
									<div class="input-control radio">
										<label>
											<?php echo form_radio(
												array(
													'name' => 'id_tipo_cuenta',
													'value' => $cuenta['id_tipo_cuenta'],
													'group' => 'cuentas'
													)
												);
											?>
											<span class="check"></span>
										</label>
									</div>
								</td>
								<td><?php echo $cuenta['descripcion']; ?></td>
								<td>
									<?php 
									if ($cuenta['aumenta_capital'] == 1) {
										echo "SÍ";
									} else {
										echo "NO";
									}
									
									?>
								</td>
								<td>
									<?php
									if ($cuenta['activo'] == 1) {
										echo form_checkbox(
											array(
												'id' => $cuenta['id_tipo_cuenta'],
												'name' => 'activo',
												'value' => '0',
												'checked' => TRUE,
												'onClick' => 'cambia_estado(this.id, this.value);'
												)
											);
									} else {
										echo form_checkbox(
											$datos = array(
												'id' => $cuenta['id_tipo_cuenta'],
												'name' => 'activo',
												'value' => '1',
												'checked' => FALSE,
												'onClick' => 'cambia_estado(this.id, this.value);'
												)
											);
									} ?>
								</td>
							</tr>
							<?php } ?>
						</table>
						<?php echo form_close(); ?>
						<div>
							<?php echo form_open('listados/listar_tipos_cuentas'); ?>
							<?php echo form_button(
											array(
												'name' => 'opcion',
												'class' => 'success',
												'value' => 'dec',
												'type' => 'submit',
												'content' => 'Anteriores'
											)
										); ?>
							<?php echo form_button(
											array(
												'name' => 'opcion',
												'class' => 'inverse',
												'value' => 'inc',
												'type' => 'submit',
												'content' => 'Siguientes'
											)
										); ?>
							<?php echo form_close(); ?>
						</div>
					</div><!-- fin span8-->
				<div class="span2"></div>
			</div><!-- fin row-->
	</div><!-- fin grid-->

</body>