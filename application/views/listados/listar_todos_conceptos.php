<body class="metro">
	<div class="grid" style="padding-left: 15%;">
		<div class="row">
			<h1 class="tile-area-title fg-white">Listado de Conceptos</h1>
			<br>
			<div class="span11">
				<div class="text-right">
					<?php echo form_button(
							array(
								'class'=>'icon-plus-2 large success',
								'value' => 'Agregar',
								'title' => 'Agregar',
								'onclick' => "window.location.assign('".site_url('agregar_nuevo_concepto')."');"
							)
						); ?>
					<?php echo form_button(
							array(
								'name' => 'cancelar',
								'class'=>'large  icon-home danger cancelar',
								'value' => 'Cancelar',
								'title' => 'Inicio',
								'onclick' => "window.location.assign('".site_url('usuario')."');"
							)
						); ?>
						<br><br>
					</div>
					<table class="table striped bordered hovered">
						<tr>
							<th>Opcion</th>
							<th>Nombre del Concepto</th>
							<th>Monto Autorizado</th>
							<th>Monto Acordado</th>
							<th>Responsable</th>
						</tr>
						<?php foreach ($conceptos as $concepto) { ?>
						<tr>
							<td>
								<div class="input-control radio">
									<label>
										<?php echo form_radio(
											array(
												'name' => 'id_concepto',
												'value' => $concepto['id_concepto'],
												'group' => 'conceptos'
												)
											); ?>
										<span class="check"></span>
									</label>
								</div>
							</td>
							<td><?php echo $concepto['nombre_concepto']; ?></td>
							<td><?php echo $concepto['costo_autorizado']; ?></td>
							<td><?php echo $concepto['monto_acordado']; ?></td>
							<td><?php echo $concepto['responsable']; ?></td>
						</tr>
						<?php } ?>
					</table>

					<div>
						<?php echo form_open('listados/listar_todos_conceptos'); ?>
						<?php
						echo form_button(
							array(
								'name' => 'opcion',
								'class' => 'success',
								'value' => 'dec',
								'type' => 'submit',
								'content' => 'Anteriores'
								)
							); ?>
						<?php
						echo form_button(
							array(
								'name' => 'opcion',
								'class' => 'inverse',
								'value' => 'inc',
								'type' => 'submit',
								'content' => 'Siguientes'
								)
							); ?>
						<?php echo form_close(); ?>
					</div>
				</div><!-- fin span8-->
				<div class="span2">
				</div>
			</div><!-- fin row-->
		</div><!-- fin grid-->
</body>