<body class="metro">
	<script src="<?php echo base_url('js/inventarios/cambiar_estado_elemento_catalogo.js') ?>"></script>
	<div class="tile-area tile-alrea-darkTeal">
	<div class="grid fluid">
		<!-- inicio clase row -->
		<div class="row"> 
			<div class="span8 offset2">
				<p class="header fg-white">
					Listado Tipos de Concepto
				</p>
			</div>
			<div class="span3">
				<div class="user-id">
					<div class="user-id-image">
						<span class="icon-user no-display1"></span>
						<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
					</div>
					<div class="user-id-name">
						<span class="first-name">Usuario</span>
						<span class="last-name">Rol</span>
					</div>
				</div>
			</div>
		</div>
		<!-- fin row -->

		<!-- inicio clase row -->
		<div class="row">
			<ul>
				<?php echo validation_errors('<li class="error-li">','</li>'); ?>
			</ul>
		</div>
		<!-- inicio final row -->


		<div class="row">
			<?php echo form_input(
						array(
							'id' => 'url_servicio',
							'name' => 'url_servicio',
							'type' => 'hidden',
							'value' => $url_servicio,
						)
					); ?>
			<?php echo form_input(
						array(
							'id' => 'modelo',
							'name' => 'modelo',
							'type' => 'hidden',
							'value' => $modelo,
						)
					); ?>
			<?php echo form_open('catalogos/modificar_tipo_concepto', array('id' => 'modify-form')); ?>
			<div class="span8 offset2">
				<div class="text-right">
					<?php echo form_button(
						array(
							'class'=>'icon-plus-2 large success',
							'value' => 'Agregar',
							'title' => 'Agregar',
							'onclick' => "window.location.assign('".site_url('catalogos/agregar_nuevo_tipo_concepto')."');"
								)
						); ?>
					<?php echo form_button(
						array(
							'class'=>'large icon-pencil btn warning',
							'value' => 'Modificar',
							'title' => 'Modificar',
							'type' => 'submit'
							)
						); ?>
					<?php echo form_button(
						array(
							'name' => 'cancelar',
							'class'=>'large  icon-home danger cancelar',
							'value' => 'Cancelar',
							'title' => 'Inicio',
							'onclick' => "window.location.assign('".site_url('usuario')."');"
							)
						); ?>
					<br><br>
				</div>
				<table class="table striped bordered hovered">
							<tr>
								<th>Opcion</th>
								<th>Nombre</th>
								<th>Activo</th>
							</tr>
							<?php foreach ($tipos as $tipo) { ?>
							<tr>
								<td>
									<div class="text-center">
										<div class="input-control radio default-style">
											<label>
												<?php echo form_radio(
													array(
														'name' => 'id_tipo_concepto',
														'value' => $tipo['id_tipo_concepto'],
														'group' => 'tipos'
														)
													);
												?>
												<span class="check"></span>
											</label>
										</div>
									</div>
								</td>
								<td>
									<div class="readable-text offset1">
										<?php echo $tipo['nombre']; ?>
									</div>
								</td>
								<td class="text-center">
									<div class="input-control checkbox">
										<label class="text-left">
											<?php
											if ($tipo['activo'] == 1) {
												echo form_checkbox(
														$datos = array(
															'id' => $tipo['id_tipo_concepto'],
															'name' => 'activo',
															'value' => '0',
															'checked' => TRUE,
															'onClick' => 'cambia_estado(this.id, this.value);',
														)
													);
											} else {
												echo form_checkbox(
														$datos = array(
															'id' => $tipo['id_tipo_concepto'],
															'name' => 'activo',
															'value' => '1',
															'checked' => FALSE,
															'onClick' => 'cambia_estado(this.id, this.value);',
														)
													);
											}
											?>
											<span class="check"></span>
										</label>
									</div>
								</td>
							</tr>
							<?php } ?>
						</table>
						<?php echo form_close(); ?>
						<div>
							<?php echo form_open('listados/listar_tipos_concepto'); ?>
							<?php echo form_button(
											array(
												'name' => 'opcion',
												'class' => 'success',
												'value' => 'dec',
												'type' => 'submit',
												'content' => 'Anteriores'
											)
										); ?>
							<?php echo form_button(
											array(
												'name' => 'opcion',
												'class' => 'inverse',
												'value' => 'inc',
												'type' => 'submit',
												'content' => 'Siguientes'
											)
										); ?>
							<?php echo form_close(); ?>
						</div>
					</div><!-- fin span8-->
			</div><!-- fin row-->
	</div><!-- fin grid-->
	</div>
</body>