<body class="metro">
	<script src="<?php echo base_url('js/catalogos/agregar_nuevo_tipo_concepto.js'); ?>"></script>
	<div class="tile-area tile-area-darkTeal">
		<!-- inicio clase grid -->
		<div class="grid fluid">
			<?php echo form_open('catalogos/agregar_nuevo_tipo_concepto', array('id' => 'form_nuevo_tipo_concepto')); ?>
			<!-- inicio clase row -->
			<div class="row">
				<div class="span10 offset1">
					<p class="header fg-white">
						Agregar Nuevo Tipo de Concepto
					</p>
				</div>

				<div class="span3">
					<div class="user-id">
						<div class="user-id-image">
							<span class="icon-user no-display1"></span>
							<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
						</div>
						<div class="user-id-name">
							<span class="first-name">Usuario</span>
							<span class="last-name">Rol</span>
						</div>
					</div>
				</div>
			</div>
			<!-- fin clase row -->

			<!-- inicio clase row -->
			<div class="row">
				<div>
					<ul>
						<?php echo validation_errors('<li class="error-li">','</li>'); ?>
					</ul>
				</div>
			</div>
			<!-- fin clase row -->

			<!-- inicio clase row -->
			<div class="row">
				<div class="span5 offset3">
					<div>
						<h3 class="fg-white"><?php echo form_label('Nombre'); ?></h3>
						<div class="input-control text">
							<?php echo form_input(
								array(
									'id' => 'url_servicio_catalogos',
									'name' => 'url_servicio_catalogos',
									'type' => 'hidden',
									'value' => $url_servicio_catalogos
									)
									); ?>
							<?php echo form_input(
								array(
									'id' => 'nombre_concepto',
									'name' => 'nombre_concepto',
									'value' => set_value('nombre_concepto')
									)
									); ?>
							<button class="btn-clear"></button>
						</div>
					</div>
				</div>
				<div class="span1">
					<?php echo form_submit(
						array(
							'id' => 'agregar_nuevo_usuario',
							'class'=>'icon-floppy shortcut success',
							'name' => 'agregar_nuevo_usuario',
							'value' => 'Agregar'
						)
						); ?>
				</div>
			</div>
			<!-- fin clase row -->
			
			<!-- inicio clase row -->
			<div class="row">
				<div class="span5 offset3">
					<div>
						<h3 class="fg-white"><?php echo form_label('Descripcion'); ?></h3>
						<div class="input-control text">
							<?php echo form_input(
								array(
									'id' => 'descripcion_concepto',
									'name' => 'descripcion_concepto',
									'value' => set_value('descripcion_concepto')
									)
									); ?>
							<button class="btn-clear"></button>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Tipo de cuenta'); ?></h3>
						<div class="input-control select">
							<?php echo form_dropdown('tipo_cuenta', $cuentas, set_value('tipo_cuenta')); ?>
						</div>
					</div>
				</div>
				<div class="span1">
					<?php echo form_button(
						array(
							'name' => 'cancelar',
							'class'=>'shortcut  icon-cancel danger cancelar',
							'value' => 'Cancelar',
							'onclick' => "window.location.assign('".site_url('listados/listar_tipos_concepto')."');"
							)
						); ?>
				</div>
			</div>
			<!-- fin clase row -->

			<?php echo form_close(); ?>
		</div>
		<!-- fin clase grid-->
	</div><!-- /tile-area -->
</body>
</html>