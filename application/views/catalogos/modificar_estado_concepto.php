<body class="metro">
	<script src="<?php echo base_url('js/catalogos/modificar_estado_concepto.js'); ?>"></script>

	<div class="tile-area tile-area-darkTeal">
		<?php echo form_open('catalogos/modificar_estado_concepto/aplicar_cambios', array('id' => 'form_modificar_estado_concepto')); ?>

		<!-- inicio clase grid -->
		<div class="grid fluid">
			<!-- Inicio clase row -->
			<div class="row">
				<div class="span10 offset1">
					<p class="header fg-white">
						Modificar Estado Concepto
					</p>
				</div>

				<div class="span3">
					<div class="user-id">
						<div class="user-id-image">
							<span class="icon-user no-display1"></span>
							<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
						</div>
						<div class="user-id-name">
							<span class="first-name">Usuario</span>
							<span class="last-name">Rol</span>
						</div>
					</div>
				</div>
			</div>
			<!-- Fin clase row -->

			<!-- Inicio clase row -->
			<div class="row">
				<div>
					<ul><?php echo validation_errors('<li class="error-li">','</li>'); ?></ul>
				</div>
			</div>
			<!-- Fin clase row -->

			<!-- Inicio clase row -->
			<div class="row">
				<div class="span5 offset3">
					<h3 class="fg-white"><?php echo form_label('Nombre del estado'); ?></h3>
					<?php echo form_input(
						array(
							'id' => 'url_servicio_catalogos',
							'name' => 'url_servicio_catalogos',
							'type' => 'hidden',
							'value' => $url_servicio_catalogos
							)
							); ?>
					<?php echo form_input(
						array(
							'id' => 'id_estado_concepto',
							'name' => 'id_estado_concepto',
							'type' => 'hidden',
							'value' => $estado['id_estado_concepto']
							)
							); ?>
					<div class="input-control text">
						<?php echo form_input(
							array(
								'id' => 'nombre_estado_concepto',
								'name' => 'nombre_estado_concepto',
								'value' => $estado['nombre_estado_concepto']
								)
								); ?>
						<button class="btn-clear"></button>
					</div>
				</div>
				<div class="span1">
					<?php echo form_submit(
						array(
							'id' => 'modificar_estado_concepto',
							'class'=>'icon-floppy shortcut warning',
							'name' => 'modificar_estado_concepto',
							'value' => 'Modificar'
							)
							); ?>
				</div>
			</div>
			<!-- Fin clase row -->

			<!-- inicio clase row -->
			<div class="row">
				<div class="span1 offset8">
					<?php echo form_button(
						array(
							'name' => 'cancelar',
							'class'=>'shortcut  icon-cancel danger cancelar',
							'value' => 'Cancelar',
							'onclick' => "window.location.assign('".site_url('listados/listar_estados_concepto')."');"
							)
						);
						?>
				</div>
			</div>
			<!-- fin clase row -->
		</div>
		<!-- fin clase grid -->
		<?php echo form_close(); ?>
	</div>
	<!-- /tile-area -->
</body>
</html>