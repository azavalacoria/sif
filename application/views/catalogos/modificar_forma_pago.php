<body class="metro">
	<div class="tile-area tile-area-darkTeal">
			<h1 class="tile-area-title fg-white">Modificar Forma de Pago</h1>
			        <div class="user-id">
			            <div class="user-id-image">
			                <span class="icon-user no-display1"></span>
			                <img src="docs/images/Battlefield_4_Icon.png" class="no-display">
			            </div>
			            <div class="user-id-name">
			                <span class="first-name">Usuario</span>
			                <span class="last-name">Rol</span>
			            </div>
			        </div>

			<div class="grid" style="padding-left: 15%;">
		    <div class="row">
		    	<div class="span1"></div>
		    	<div class="span7">
		    		<div>
						<ul>
							<?php echo validation_errors('<li class="error-li">','</li>'); ?>
						</ul>
					</div>
					<div>
						<?php echo form_open('catalogos/modificar_forma_pago/aplicar_cambios', array('id' => 'update-form')); ?>
						<div>
							<h3 class="fg-white"><?php echo form_label('Descripcion'); ?></h3>
							<?php echo form_input(
									array(
										'id' => 'id_forma_pago',
										'name' => 'id_forma_pago',
										'type' => 'hidden',
										'value' => $forma['id_forma_pago']
									)
								); ?>
							<div class="input-control text">
							<?php echo form_input(
									array(
										'id' => 'descripcion',
										'name' => 'descripcion',
										'value' => $forma['descripcion']
									)
								); ?>
								<button class="btn-clear"></button>
								</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Bóveda'); ?></h3>
							<div class="input-control select">
								<?php echo form_dropdown('tipo_boveda', $bovedas, $forma['tipo_boveda']); ?>
							</div>
						</div>
					</div>
		    	</div>
		    	<div class="span4">
							<?php echo form_submit(
									array(
										'id' => 'agregar_nuevo_usuario',
										'class'=>'icon-floppy shortcut success',

										'name' => 'agregar_nuevo_usuario',
										'value' => 'Agregar'
									)
								); ?>
								<?php echo form_button(
						array(
							'name' => 'cancelar',
							'class'=>'shortcut  icon-cancel danger cancelar',
							'value' => 'Cancelar',
							'onclick' => 'window.location.assign('."'".site_url()."/listados/listar_formas_pago"."'".');'

						)
					);
				?>
						</div>
						<?php echo form_close(); ?>
		    </div><!-- /row -->
		    </div><!-- /grid -->


	</div><!-- /tile-area -->





</body>
</html>