<body class="metro">
	<div class="tile-area tile-area-darkTeal">
			<h1 class="tile-area-title fg-white">Agregar Nuevo Tipo Bóveda</h1>
				<div class="user-id">
					<div class="user-id-image">
						<span class="icon-user no-display1"></span>
						<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
					</div>
					<div class="user-id-name">
						<span class="first-name">Usuario</span>
						<span class="last-name">Rol</span>
					</div>
				</div>
				<div class="grid" style="padding-left: 15%;">
					<div class="row">
						<div class="span1"></div>
						<div class="span7">
							<div>
								<ul>
									<?php echo validation_errors('<li class="error-li">','</li>'); ?>
								</ul>
							</div>
							<?php echo form_open('catalogos/modificar_tipo_boveda/aplicar_cambios', array('id'=> 'update-form')); ?>
							<?php foreach ($boveda as $b) { ?>
							<div>
								<div>
									<?php
									echo form_input(array(
											'id' => 'id_tipo_boveda',
											'name' => 'id_tipo_boveda',
											'type' => 'hidden',
											'value' => $b['id_tipo_boveda']
										));
									?>
									<h3 class="fg-white"><?php echo form_label('Nombre de la bóveda'); ?></h3>
									<div class="input-control text">
										<?php echo form_input(
											array(
												'id' => 'nombre_boveda',
												'name' => 'nombre_boveda',
												'value' => $b['nombre_boveda']
											)
										); ?>
										<button class="btn-clear"></button>
									</div>
								</div>
							</div>
							<?php }?>
						</div>
						<div class="span4">
							<?php echo form_submit(
									array(
										'id' => 'agregar_nuevo_usuario',
										'class'=>'icon-pencil shortcut warning',
										'name' => 'agregar_nuevo_usuario',
										'value' => 'Modificar'
									)
								); ?>
							<?php echo form_button(
								array(
									'name' => 'cancelar',
									'class'=>'shortcut  icon-cancel danger cancelar',
									'value' => 'Cancelar',
									'onclick' => "window.location.assign('".site_url('listados/listar_tipos_bovedas')."');"
									)
								);
							?>
						</div>
						<?php echo form_close(); ?>
					</div><!-- /row -->
				</div><!-- /grid -->
			</div><!-- /tile-area -->
	</body>
</html>