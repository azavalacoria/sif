<body class="metro">
	<script type="text/javascript" src="<?php echo base_url('js/inventarios/catalogos-buscar-persona.js'); ?>"></script>
	<div class="tile-area tile-area-darkTeal">
			<h1 class="tile-area-title fg-white">
				<?php
				$nombre_usuario = '';
				if (strlen($usuario['nombre_usuario']) > 0) {
					$nombre_usuario = ' '.strtoupper($usuario['nombre_usuario']);
				}
				?>
				Modificar Usuario <?php echo $nombre_usuario; ?>
			</h1>
			<div class="user-id">
				<div class="user-id-image">
					<span class="icon-user no-display1"></span>
					<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
				</div>
				<div class="user-id-name">
					<span class="first-name">Usuario</span>
					<span class="last-name">Rol</span>
				</div>
			</div>
			<?php
			echo form_input(
				array(
						'id' => 'url_servicio',
						'name' => 'url_servicio',
						'type' => 'hidden',
						'value' => $url_servicio
					)
				);
			?>
			<div class="grid" id="form-grid" style="padding-left: 15%;">
		    <div class="row">
		    	<div class="span1"></div>
		    	<div class="span7">

		    		<div>
						<ul>
							<?php echo validation_errors('<li class="error-li">','</li>'); ?>
						</ul>
					</div>
					<div>
						<?php echo form_open('catalogos/modificar_usuario/aplicar_cambios'); ?>
						<div>
							<?php
							$id_usuario = 0;
							if (isset($usuario['id_usuario'])) {
								$id_usuario = $usuario['id_usuario'];
							} else {
								$id_usuario = $_POST['id_usuario'];
							}
							
							echo form_input(
								array(
									'id' => 'id_usuario',
									'name' => 'id_usuario',
									'type' => 'hidden',
									'value' => $id_usuario
								)
							); 
							?>
							<h3 class="fg-white"><?php echo form_label('Nombre del usuario'); ?></h3>
							<div class="input-control text">
							<?php
							if (isset($usuario['nombre_usuario'])) {
								echo form_input(
									array(
										'id' => 'nombre_usuario',
										'name' => 'nombre_usuario',
										'value' => $usuario['nombre_usuario']
									)
								); 
							} else {
								echo form_input(
									array(
										'id' => 'nombre_usuario',
										'name' => 'nombre_usuario',
										'value' => set_value('nombre_usuario')
									)
								); 
							}
							?>
								<button class="btn-clear"></button>
								</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Contraseña'); ?></h3>
							<div class="input-control password">
							<?php
							if (isset($_POST['password'])) {
								echo form_password(
									array(
										'id' => 'password',
										'name' => 'password',
										'value' => $_POST['password']
									)
								);
							} else {
								echo form_password(
									array(
										'id' => 'password',
										'name' => 'password',
										'value' => set_value('password')
									)
								);
							}
							?>
								<button class="btn-reveal"></button>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Confirmar contraseña'); ?></h3>
							<div class="input-control password">
							<?php
							if (isset($_POST['password_confirmado'])) {
								echo form_password(
									array(
										'id' => 'password_confirmado',
										'name' => 'password_confirmado',
										'value' => $_POST['password_confirmado']
									)
								);
							} else {
								echo form_password(
									array(
										'id' => 'password_confirmado',
										'name' => 'password_confirmado',
										'value' => set_value('password_confirmado')
									)
								);
							}

							?>
								<button class="btn-reveal"></button>
							</div>
						</div>
						<div>
									<h3 class="fg-white"><?php echo form_label('Nombre de la persona'); ?></h3>
									<?php

									if (isset($usuario['persona'])) {
										echo form_input(
											array(
													'id' => 'persona',
													'name' => 'persona',
													'type' => 'hidden',
													'value' => $usuario['persona']
												)
										);
									} else {
										echo form_input(
											array(
													'id' => 'persona',
													'name' => 'persona',
													'type' => 'hidden',
													'value' => set_value('persona')
												)
										);
									}
									?>
									<div class="input-control text">
										<?php

										if (isset($usuario['nombre_persona'])) {
											echo form_input(
												array(
														'id' => 'persona_nombre',
														'name' => 'persona_nombre',
														'onClick' => 'desplegar_formulario_busqueda();',
														'value' => $usuario['nombre_persona']
													)
											);
										} else {
											echo form_input(
												array(
														'id' => 'persona_nombre',
														'name' => 'persona_nombre',
														'onClick' => 'desplegar_formulario_busqueda();',
														'value' => $_POST['persona_nombre']
													)
											);
										}
										?>
									</div>
								</div>

						<div>
							<h3 class="fg-white"><?php echo form_label('Puesto'); ?></h3>
							<div class="input-control select">
							<?php 
							if (isset($usuario['puesto'])) {
								echo form_dropdown('puesto', $puestos, $usuario['puesto']);
							} else {
								echo form_dropdown('puesto', $puestos, set_value('puesto'));
							} ?>
							</div>
						</div>
						<p>Generado en <strong>{elapsed_time}</strong> segundos</p>
					</div>
		    	</div>
		    	<div class="span4">
							<?php echo form_submit(
									array(
										'id' => 'agregar_nuevo_usuario',
										'class'=>'icon-floppy shortcut success',

										'name' => 'agregar_nuevo_usuario',
										'value' => 'Agregar'
									)
								); ?>
								<?php echo form_button(
						array(
							'name' => 'cancelar',
							'class'=>'shortcut  icon-cancel danger cancelar',
							'value' => 'Cancelar',
							'onclick' => 'window.location.assign('."'".site_url()."/listados/listar_usuarios"."'".');'
						)
					);
				?>
						</div>
						<?php echo form_close(); ?>
		    </div><!-- /row -->
		</div><!-- /grid -->
		<div class="grid" id="search-grid">
					<div class="search-grid-cell">
						<h3 class="fg-white"><?php echo form_label('Apellido Paterno'); ?></h3>
						<?php
						
						echo form_input(array(
								'id' => 'apellido_paterno',
								'name' => 'apellido_paterno',
								'value' => set_value('apellido_paterno')
							));
						?>
					</div>
					<div class="search-grid-cell">
						<h3 class="fg-white"><?php echo form_label('Apellido Materno'); ?></h3>
						<?php
						echo form_input(array(
								'id' => 'apellido_materno',
								'name' => 'apellido_materno',
								'value' => set_value('apellido_materno')
							));
						?>
					</div>
					<div class="search-grid-cell">
						<h3 class="fg-white"><?php echo form_label('Nombres'); ?></h3>
						<?php
						echo form_input(array(
								'id' => 'nombres',
								'name' => 'nombres',
								'value' => set_value('nombres')
							));
						?>
					</div>
					<div><button id="buscar-persona" onClick="buscar_persona();">Buscar</button></div>
					<div>
						<form id="personas-form">
							<table id="table-results-grid">
							</table>

						</form>
						<button id="seleccionar-persona">Seleccionar</button>
						<button id="cancelar-busqueda" onClick="cancelar_busqueda_persona()">Cancelar búsqueda</button>
					</div>
				</div>

	</div><!-- /tile-area -->





</body>
</html>