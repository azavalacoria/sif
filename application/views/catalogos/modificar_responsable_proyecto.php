<body class="metro">
	<script type="text/javascript" src="<?php echo base_url('js/inventarios/catalogos-buscar-persona.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('js/inventarios/catalogos-buscar-organizacion.js') ?>"></script>
	<div class="tile-area tile-area-darkTeal">
			<h1 class="tile-area-title fg-white">Modificar Responsable de Proyecto</h1>
				<div class="user-id">
					<div class="user-id-image">
						<span class="icon-user no-display1"></span>
						<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
					</div>
					<div class="user-id-name">
						<span class="first-name">Usuario</span>
						<span class="last-name">Rol</span>
					</div>
				</div>
				<?php
				echo form_input(
					array(
							'id' => 'url_servicio',
							'name' => 'url_servicio',
							'type' => 'hidden',
							'value' => $url_servicio
						)
					);

				echo form_input(
					array(
							'id' => 'url_org_servicio',
							'name' => 'url_org_servicio',
							'type' => 'hidden',
							'value' => $url_org_servicio
						)
					);
				?>
				<div class="grid" id="search-org-grid">
					<div class="search-grid-cell">
						<h3 class="fg-white"><?php echo form_label('Nombre de la organización'); ?></h3>
						<?php
						
						echo form_input(array(
								'id' => 'nombre_organizacion',
								'name' => 'nombre_organizacion',
								'value' => set_value('nombre_organizacion')
							));
						?>
					</div>
					<div><button id="buscar-organizacion" onClick="buscar_organizacion();">Buscar</button></div>
					<div>
						<form id="organizaciones-form">
							<table id="table-org-results-grid">
							</table>

						</form>
						<button id="seleccionar-organizacion" onClick="asignar_organizacion();">Seleccionar Organización</button>
						<button id="cancelar-busqueda" onClick="cancelar_busqueda_organizacion()">Cancelar búsqueda</button>

					</div>
				</div>
				<div class="grid" id="search-grid">
					<div class="search-grid-cell">
						<h3 class="fg-white"><?php echo form_label('Apellido Paterno'); ?></h3>
						<?php
						
						echo form_input(array(
								'id' => 'apellido_paterno',
								'name' => 'apellido_paterno',
								'value' => set_value('apellido_paterno')
							));
						?>
					</div>
					<div class="search-grid-cell">
						<h3 class="fg-white"><?php echo form_label('Apellido Materno'); ?></h3>
						<?php
						echo form_input(array(
								'id' => 'apellido_materno',
								'name' => 'apellido_materno',
								'value' => set_value('apellido_materno')
							));
						?>
					</div>
					<div class="search-grid-cell">
						<h3 class="fg-white"><?php echo form_label('Nombres'); ?></h3>
						<?php
						echo form_input(array(
								'id' => 'nombres',
								'name' => 'nombres',
								'value' => set_value('nombres')
							));
						?>
					</div>
					<div><button id="buscar-persona" onClick="buscar_persona();">Buscar</button></div>
					<div>
						<form id="personas-form">
							<table id="table-results-grid">
							</table>

						</form>
						<button id="seleccionar-persona">Seleccionar Persona</button>
						<button id="cancelar-busqueda" onClick="cancelar_busqueda_persona()">Cancelar búsqueda</button>
					</div>
				</div>
				<div class="grid" id="form-grid" style="padding-left: 15%;">
					<div class="row">
						<div class="span1"></div>
						<div class="span7">
							<div>
								<ul>
									<?php echo validation_errors('<li class="error-li">','</li>'); ?>
								</ul>
							</div>
							<div>
								<?php echo form_open('catalogos/modificar_responsable_proyecto/aplicar_cambios'); ?>
								<?php foreach ($responsable as $r) { ?>
								<div>
									<?php echo form_input(
											array(
													'id' => 'id_responsable',
													'name' => 'id_responsable',
													'type' => 'hidden',
													'value' => $r['id_responsable']
												)
										);
									?>
									<h3 class="fg-white"><?php echo form_label('Nombre del Responsable de Proyecto'); ?></h3>
									<?php
									echo form_input(
											array(
													'id' => 'persona',
													'name' => 'persona',
													'type' => 'hidden',
													'value' => $r['persona']
												)
										);
									?>
									<div class="input-control text">
										<?php
										echo form_input(
												array(
														'id' => 'persona_nombre',
														'name' => 'persona_nombre',
														'onClick' => 'desplegar_formulario_busqueda();',
														'value' => $r['persona_nombre']
													)
											);
										?>
									</div>
								</div>
								<div>
									<h3 class="fg-white"><?php echo form_label('Organización a la que representa'); ?></h3>
									<?php
									echo form_input(
											array(
													'id' => 'organizacion',
													'name' => 'organizacion',
													'type' => 'hidden',
													'value' => $r['organizacion']
												)
										);
									?>
									<div class="input-control text">
										<?php
										echo form_input(
												array(
														'id' => 'organizacion_nombre',
														'name' => 'organizacion_nombre',
														'onClick' => 'desplegar_formulario_busqueda_organizacion();',
														'value' => $r['organizacion_nombre']
													)
											);
										?>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
						<div class="span4">
							<?php echo form_button(
									array(
										'id' => 'agregar_nuevo_usuario',
										'class'=>'icon-floppy shortcut btn warning',
										'type' => 'submit',
										'name' => 'agregar_nuevo_usuario',
										'value' => 'Agregar'
									)
								); ?>
							<?php echo form_button(
								array(
									'name' => 'cancelar',
									'class'=>'shortcut  icon-cancel danger cancelar',
									'value' => 'Cancelar',
									'onclick' => 'window.location.assign('."'".site_url()."/listados/listar_responsables_proyectos"."'".');'
									)
								);
							?>
						</div>
						<?php echo form_close(); ?>
					</div><!-- /row -->
				</div><!-- /grid -->
			</div><!-- /tile-area -->
	</body>
</html>