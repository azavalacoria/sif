<body class="metro">
	<div class="tile-area tile-area-darkTeal">
		<h1 class="tile-area-title fg-white">Agregar Nuevo Tipo de Movimiento</h1>
		<div class="user-id">
			<div class="user-id-image">
				<span class="icon-user no-display1"></span>
				<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
			</div>
			<div class="user-id-name">
				<span class="first-name">Usuario</span>
				<span class="last-name">Rol</span>
			</div>
		</div>
		<script type="text/javascript" src="<?php echo base_url('js/catalogos/agregar_nuevo_tipo_movimiento') ?>"></script>
		<div class="grid" style="padding-left: 15%;">
			<div class="row">
				<div class="span1"></div>
				<div class="span7">
					<ul>
						<?php echo validation_errors('<li>','</li>'); ?>
					</ul>
					<div>
						<?php $atributos = array('id' => 'form_nuevo_tipo_movimiento'); ?>
						<?php echo form_open('catalogos/agregar_nuevo_tipo_movimiento', $atributos); ?>
						<div>
							<h3 class="fg-white"><?php echo form_label('Nombre del Tipo de Movimiento'); ?></h3>
							<div class="input-control text">
								<?php echo form_input(
										array(
											'id' => 'nombre_tipo_cuenta',
											'class' => 'required',
											'name' => 'nombre_tipo_cuenta',
											'value' => set_value('nombre_tipo_cuenta')
										)
									); ?>
								<button class="btn-clear"></button>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Aumenta Capital'); ?></h3>
							<div class="input-control checkbox">
								<label>
									<?php echo form_checkbox(
												array(
														'id' => 'aumenta_capital',
														'name' => 'aumenta_capital',
														'checked' => false,
														'value' => 1
													)
											); ?>
									<span class="check"></span>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="span4">
					<?php echo form_submit(
						array(
							'id' => 'agregar_nuevo_tipo_cuenta',
							'class'=>'icon-floppy shortcut success',
							'name' => 'agregar_nuevo_tipo_cuenta',
							'value' => 'Agregar'
							)
							); ?>
					<?php echo form_button(
						array(
							'name' => 'cancelar',
							'class'=>'shortcut  icon-cancel danger cancelar',
							'value' => 'Cancelar',
							'onclick' => "window.location.assign('".site_url('listados/listar_tipos_movimientos')."');"
							)
						); ?>
				</div>
				<?php echo form_close(); ?>
			</div><!-- /row -->
		</div><!-- /grid -->

	</div><!-- /tile-area -->
</body>
</html>