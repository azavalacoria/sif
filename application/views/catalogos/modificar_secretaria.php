<body class="metro">
	<div class="tile-area tile-area-darkTeal">
			<h1 class="tile-area-title fg-white">Agregar Nueva Secretaría</h1>
				<div class="user-id">
					<div class="user-id-image">
						<span class="icon-user no-display1"></span>
						<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
					</div>
					<div class="user-id-name">
						<span class="first-name">Usuario</span>
						<span class="last-name">Rol</span>
					</div>
				</div>
				<div class="grid" style="padding-left: 15%;">
					<div class="row">
						<div class="span1"></div>
						<div class="span7">
							<div>
								<?php echo form_open('catalogos/modificar_secretaria/aplicar_cambios'); ?>
								<div>
									<?php echo form_input(
											array(
												'id' => 'url_servicio',
												'name' => 'url_servicio',
												'type' => 'hidden',
												'value' => $url_servicio,
											)
										);
									?>
									<?php echo form_input(
											array(
												'id' => 'id_secretaria',
												'name' => 'id_secretaria',
												'type' => 'hidden',
												'value' => $secretaria['id_secretaria'],
											)
										);
									?>
									<h3 class="fg-white"><?php echo form_label('Nombre de la secretaría'); ?></h3>
									<div class="input-control text">
										<?php echo form_input(
											array(
												'id' => 'nombre',
												'name' => 'nombre',
												'value' => $secretaria['nombre']
											)
										); ?>
										<button class="btn-clear"></button>
									</div>
								</div>

								<div>
									<h3 class="fg-white"><?php echo form_label('Siglas'); ?></h3>
									<div class="input-control text">
										<?php echo form_input(
											array(
												'id' => 'siglas',
												'name' => 'siglas',
												'value' => $secretaria['siglas']
											)
										); ?>
										<button class="btn-clear"></button>
									</div>
								</div>


								<div>
									<h3 class="fg-white"><?php echo form_label('Dirección'); ?></h3>
									<div class="input-control text">
										<?php echo form_input(
											array(
												'id' => 'direccion',
												'name' => 'direccion',
												'value' => $secretaria['direccion']
											)
										); ?>
										<button class="btn-clear"></button>
									</div>
								</div>

								<div>
									<h3 class="fg-white"><?php echo form_label('Entidad Federativa'); ?></h3>
									<?php $js = 'id="entidad_federativa" onChange="obtener_municipios();"'; ?>
									<div class="input-control select">
									<?php echo form_dropdown(
											'entidad_federativa',
											$entidades,
											$secretaria['entidad_federativa'],
											$js
										);
									?>
									</div>
								</div>
								<div>
									<h3 class="fg-white"><?php echo form_label('Municipio'); ?></h3>
									<?php $js = 'id="municipio" onChange="obtener_localidades();"'; ?>
									<div class="input-control select">
									<?php echo form_dropdown(
											'municipio',
											$municipios,
											$secretaria['municipio'],
											$js
										);
									?>
									</div>
								</div>
								<div>
									<h3 class="fg-white"><?php echo form_label('Localidad'); ?></h3>
									<?php $js = 'id="localidad"'; ?>
									<div class="input-control select">
									<?php echo form_dropdown(
											'localidad',
											$localidades,
											$secretaria['localidad'],
											$js
										);
									?>
									</div>
								</div>


							</div>
						</div>
						<div class="span4">
							<?php echo form_submit(
									array(
										'id' => 'agregar_nuevo_usuario',
										'class'=>'icon-floppy shortcut success',

										'name' => 'agregar_nuevo_usuario',
										'value' => 'Agregar'
									)
								); ?>
							<?php echo form_button(
								array(
									'name' => 'cancelar',
									'class'=>'shortcut  icon-cancel danger cancelar',
									'value' => 'Cancelar',
									'onclick' => 'window.location.assign('."'".site_url()."/listados/listar_secretarias"."'".');'
									)
								);
							?>
						</div>
						<?php echo form_close(); ?>
					</div><!-- /row -->
				</div><!-- /grid -->
			</div><!-- /tile-area -->
	</body>
</html>