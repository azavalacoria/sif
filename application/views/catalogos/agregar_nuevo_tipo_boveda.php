<body class="metro">
	<div class="tile-area tile-area-darkTeal">
			<h1 class="tile-area-title fg-white">Agregar Nuevo Tipo Bóveda</h1>
				<div class="user-id">
					<div class="user-id-image">
						<span class="icon-user no-display1"></span>
						<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
					</div>
					<div class="user-id-name">
						<span class="first-name">Usuario</span>
						<span class="last-name">Rol</span>
					</div>
				</div>
				<div class="grid" style="padding-left: 15%;">
					<div class="row">
						<div class="span1"></div>
						<div class="span7">
							<div>
								<ul>
									<?php echo validation_errors('<li class="error-li">','</li>'); ?>
								</ul>
							</div>
							<div>
								<?php $atributos = array('id' => 'form_nuevo_tipo_boveda'); ?>
								<?php echo form_open('catalogos/agregar_nuevo_tipo_boveda', $atributos); ?>
								<div>
									<h3 class="fg-white"><?php echo form_label('Descripcion'); ?></h3>
									<div class="input-control text">
										<?php echo form_input(
											array(
												'id' => 'descripcion_boveda',
												'name' => 'descripcion_boveda',
												'value' => set_value('descripcion_boveda')
											)
										); ?>
										<button class="btn-clear"></button>
									</div>
								</div>
								<div>
									<h3 class="fg-white"><?php echo form_label('Saldo inicial'); ?></h3>
									<div class="input-control text">
										<?php echo form_input(
											array(
												'id' => 'saldo_inicial',
												'name' => 'saldo_inicial',
												'value' => set_value('saldo_inicial')
											)
										); ?>
										<button class="btn-clear"></button>
									</div>
								</div>
							</div>
						</div>
						<div class="span4">
							<?php echo form_submit(
									array(
										'id' => 'agregar_nuevo_usuario',
										'class'=>'icon-floppy shortcut success',

										'name' => 'agregar_nuevo_usuario',
										'value' => 'Agregar'
									)
								); ?>
							<?php echo form_button(
								array(
									'name' => 'cancelar',
									'class'=>'shortcut  icon-cancel danger cancelar',
									'value' => 'Cancelar',
									'onclick' => 'window.location.assign('."'".site_url()."/listados/listar_tipos_bovedas"."'".');'
									)
								);
							?>
						</div>
						<?php echo form_close(); ?>
					</div><!-- /row -->
				</div><!-- /grid -->
			</div><!-- /tile-area -->
	</body>
</html>