<body class="metro">
	
	<div class="tile-area tile-area-darkTeal">
			<h1 class="tile-area-title fg-white">Agregar Nuevo Ente Generador</h1>
				<div class="user-id">
					<div class="user-id-image">
						<span class="icon-user no-display1"></span>
						<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
					</div>
					<div class="user-id-name">
						<span class="first-name">Usuario</span>
						<span class="last-name">Rol</span>
					</div>
				</div>
				<style type="text/css">
				#search-grid { display: none;}
				#search-grid .search-grid-cell { display: inline-block; width: 33%;}
				#search-grid input { height: 40px; width: 90%;}
				#table-results-grid { margin: auto; width: 90%;border: 1px solid #000;}

				#search-org-grid { display: none;}
				#search-org-grid .search-grid-cell { display: inline-block; width: 33%;}
				#search-org-grid input { height: 40px; width: 90%;}
				#table-org-results-grid { margin: auto; width: 90%;border: 1px solid #000;}
				</style>
				<?php
				echo form_input(
					array(
							'id' => 'url_servicio',
							'name' => 'url_personas_servicio',
							'type' => 'hidden',
							'value' => $url_personas_servicio
						)
					);
				echo form_input(
					array(
							'id' => 'url_catalogo_servicio',
							'name' => 'url_catalogo_servicio',
							'type' => 'hidden',
							'value' => $url_catalogo_servicio,
							)
					);
				echo form_input(
					array(
							'id' => 'url_org_servicio',
							'name' => 'url_org_servicio',
							'type' => 'hidden',
							'value' => $url_ent_servicio
						)
					);
				?>
				<script type="text/javascript" src="<?php echo base_url('js/catalogos/agregar_nuevo_ente_generador.js'); ?>"></script>
				<script type="text/javascript" src="<?php echo base_url('js/inventarios/catalogos-buscar-persona.js') ?>"></script>
				<script type="text/javascript" src="<?php echo base_url('js/inventarios/catalogos-buscar-ente.js') ?>"></script>
				<div class="grid" id="search-org-grid">
					<div class="search-grid-cell">
						<h3 class="fg-white"><?php echo form_label('Nombre de la Secretaria'); ?></h3>
						<?php
						
						echo form_input(array(
								'id' => 'nombre_ente',
								'name' => 'nombre_ente',
								'value' => set_value('nombre_ente')
							));
						?>
					</div>
					<div><button id="buscar-ente" onClick="buscar_ente();">Buscar</button></div>
					<div>
						<form id="secretarias-form">
							<table id="table-org-results-grid">
							</table>

						</form>
						<button id="seleccionar-ente" onClick="asignar_ente();">Seleccionar</button>
					</div>
				</div>
				<div class="grid" id="search-grid">
					<div class="search-grid-cell">
						<h3 class="fg-white"><?php echo form_label('Apellido Paterno'); ?></h3>
						<?php
						
						echo form_input(array(
								'id' => 'apellido_paterno',
								'name' => 'apellido_paterno',
								'value' => set_value('apellido_paterno')
							));
						?>
					</div>
					<div class="search-grid-cell">
						<h3 class="fg-white"><?php echo form_label('Apellido Materno'); ?></h3>
						<?php
						echo form_input(array(
								'id' => 'apellido_materno',
								'name' => 'apellido_materno',
								'value' => set_value('apellido_materno')
							));
						?>
					</div>
					<div class="search-grid-cell">
						<h3 class="fg-white"><?php echo form_label('Nombres'); ?></h3>
						<?php
						echo form_input(array(
								'id' => 'nombres',
								'name' => 'nombres',
								'value' => set_value('nombres')
							));
						?>
					</div>
					<div><button id="buscar-persona" onClick="buscar_persona();">Buscar</button></div>
					<div>
						<form id="personas-form">
							<table id="table-results-grid">
							</table>

						</form>
						<button id="seleccionar-persona">Seleccionar</button>
					</div>
				</div>
				<div class="grid" id="form-grid" style="padding-left: 15%;">
					<div class="row">
						<div class="span1"></div>
						<div class="span7">
							<div>
								<ul>
									<?php echo validation_errors('<li class="error-li">','</li>'); ?>
								</ul>
							</div>
							<?php $atributos = array('id' => 'form_nuevo_ente_generador'); ?>
							<?php echo form_open('catalogos/agregar_nuevo_ente_generador', $atributos); ?>
							<div>
								<h3 class="fg-white"><?php echo form_label('Responsable'); ?></h3>
									<?php
									echo form_input(
											array(
													'id' => 'persona',
													'name' => 'persona',
													'type' => 'hidden',
													'value' => set_value('persona'),
												)
										);
									?>
									<div class="input-control text">
										<?php
										echo form_input(
												array(
														'id' => 'persona_nombre',
														'name' => 'persona_nombre',
														'onClick' => 'desplegar_formulario_busqueda();',
														'value' => set_value('persona_nombre'),
													)
											);
										?>
									</div>
								<div>
									
									<h3 class="fg-white"><?php echo form_label('Secretaria'); ?></h3>
									<?php
									echo form_input(
											array(
													'id' => 'secretaria',
													'name' => 'secretaria',
													'type' => 'hidden',
													'value' => set_value('secretaria'),
												)
										);
									?>
									<div class="input-control text">
										<?php
										echo form_input(
												array(
														'id' => 'secretaria_nombre',
														'name' => 'secretaria_nombre',
														'onClick' => 'desplegar_formulario_busqueda_secretaria();',
														'value' => set_value('secretaria_nombre'),
													)
											);
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="span4">
							<?php echo form_submit(
									array(
										'id' => 'agregar_nuevo_usuario',
										'class'=>'icon-floppy shortcut success',

										'name' => 'agregar_nuevo_usuario',
										'value' => 'Agregar'
									)
								); ?>
							<?php echo form_button(
								array(
									'name' => 'cancelar',
									'class'=>'shortcut  icon-cancel danger cancelar',
									'value' => 'Cancelar',
									'onclick' => "window.location.assign('".site_url('listados/listar_entes_generadores')."');"
									)
								);
							?>
						</div>
						<?php echo form_close(); ?>
					</div><!-- /row -->
				</div><!-- /grid -->
			</div><!-- /tile-area -->
	</body>
</html>