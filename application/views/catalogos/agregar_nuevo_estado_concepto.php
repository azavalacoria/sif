<body class="metro">
	<div class="tile-area tile-area-darkTeal">
		<?php echo form_open('catalogos/agregar_nuevo_estado_concepto', array('id' => 'form_nuevo_estado_concepto')); ?>

		<div class="grid fluid">
			<!-- inicio clase row -->
			<div class="row">
				<div class="span10 offset1">
					<p class="header fg-white">
						Agregar Nuevo Estado de Concepto
					</p>
				</div>

				<div class="span3">
					<div class="user-id">
						<div class="user-id-image">
							<span class="icon-user no-display1"></span>
							<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
						</div>
						<div class="user-id-name">
							<span class="first-name">Usuario</span>
							<span class="last-name">Rol</span>
						</div>
					</div>
				</div>
			</div>
			<!-- fin clase row -->

			<!-- inicio clase row -->
			<div class="row">
				<div class="span8 offset3">
					<ul><?php echo validation_errors('<li class="error-li">','</li>'); ?></ul>
				</div>
			</div>
			<!-- fin clase row -->

			<!-- inicio clase row -->

			<div class="row">
				<div class="span5 offset3">
					<div>
						<h3 class="fg-white"><?php echo form_label('Nombre del Estado'); ?></h3>
						<div class="input-control text">
							<?php echo form_input(
								array(
									'id' => 'nombre_estado_concepto',
									'class' => 'required',
									'name' => 'nombre_estado_concepto',
									'value' => set_value('nombre_estado_concepto')
								)
								); ?>
							<button class="btn-clear"></button>
						</div>
					</div>
				</div>
				<div class="span1">
					<?php echo form_submit(
						array(
							'id' => 'agregar_nuevo_estado_concepto',
							'class'=>'icon-floppy shortcut success',
							'name' => 'agregar_nuevo_estado_concepto',
							'value' => 'Agregar'
						)
					); ?>
				</div>
			</div>
			<!-- fin clase row -->

			<!-- inicio clase row -->
			<div class="row">
				<div class="span5 offset3">
				</div>
				<div class="span1">
					<?php echo form_button(
						array(
							'name' => 'cancelar',
							'class'=>'shortcut  icon-cancel danger cancelar',
							'value' => 'Cancelar',
							'onclick' => "window.location.assign('".site_url('listados/listar_estados_concepto')."');"
							)
						);
					?>
				</div>
			</div>
			<!-- fin clase row -->
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /tile-area -->
</body>
</html>