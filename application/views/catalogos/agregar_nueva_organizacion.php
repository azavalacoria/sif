<body class="metro">
	<div class="tile-area tile-area-darkTeal">
			<h1 class="tile-area-title fg-white">Agregar Nueva Organizacion</h1>
				<div class="user-id">
					<div class="user-id-image">
						<span class="icon-user no-display1"></span>
						<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
					</div>
					<div class="user-id-name">
						<span class="first-name">Usuario</span>
						<span class="last-name">Rol</span>
					</div>
				</div>
				<div class="grid" style="padding-left: 15%;">
					<div class="row">
						<div class="span1"></div>
						<div class="span7">
							<div>
								<ul>
									<?php echo validation_errors('<li class="error-li">','</li>'); ?>
								</ul>
							</div>
							<div>
								<?php $atributos = array('id' => 'form_nueva_organizacion'); ?>
								<?php echo form_open('catalogos/agregar_nueva_organizacion', $atributos); ?>
								<div>
									<?php echo form_input(
											array(
												'id' => 'url_servicio',
												'name' => 'url_servicio',
												'type' => 'hidden',
												'value' => $url_servicio,
											)
										);
									?>
									<h3 class="fg-white"><?php echo form_label('Nombre de la organización'); ?></h3>
									<div class="input-control text">
										<?php echo form_input(
											array(
												'id' => 'nombre_organizacion',
												'name' => 'nombre_organizacion',
												'value' => set_value('nombre_organizacion')
											)
										); ?>
										<button class="btn-clear"></button>
									</div>
								</div>

								<div>
									<h3 class="fg-white"><?php echo form_label('Siglas'); ?></h3>
									<div class="input-control text">
										<?php echo form_input(
											array(
												'id' => 'siglas_organizacion',
												'name' => 'siglas_organizacion',
												'value' => set_value('siglas_organizacion')
											)
										); ?>
										<button class="btn-clear"></button>
									</div>
								</div>

								<div>
									<h3 class="fg-white"><?php echo form_label('Descripcion'); ?></h3>
									<div class="input-control text">
										<?php echo form_input(
											array(
												'id' => 'descripcion_organizacion',
												'name' => 'descripcion_organizacion',
												'value' => set_value('direccion_organizacion')
											)
										); ?>
										 <button class="btn-clear"></button>
									</div>
								</div>


								<div>
									<h3 class="fg-white"><?php echo form_label('Dirección'); ?></h3>
									<div class="input-control text">
										<?php echo form_input(
											array(
												'id' => 'direccion_organizacion',
												'name' => 'direccion_organizacion',
												'value' => set_value('direccion_organizacion')
											)
										); ?>
										 <button class="btn-clear"></button>
									</div>
								</div>

								<div>
									<h3 class="fg-white"><?php echo form_label('Entidad Federativa'); ?></h3>
									<?php $js = 'id="entidad_federativa" onChange="obtener_municipios();"'; ?>
									<div class="input-control select">
									<?php echo form_dropdown(
											'entidad_federativa',
											$entidades,
											set_value('entidad_federativa'),
											$js
										);
									?>
									</div>
								</div>
								<div>
									<h3 class="fg-white"><?php echo form_label('Municipio'); ?></h3>
									<?php $js = 'id="municipio" onChange="obtener_localidades();"'; ?>
									<div class="input-control select">
									<?php echo form_dropdown(
											'municipio',
											array(),
											set_value('municipio'),
											$js
										);
									?>
									</div>
								</div>
								<div>
									<h3 class="fg-white"><?php echo form_label('Localidad'); ?></h3>
									<?php $js = 'id="localidad"'; ?>
									<div class="input-control select">
									<?php echo form_dropdown(
											'localidad',
											array(),
											set_value('localidad'),
											$js
										);
									?>
									</div>
								</div>


							</div>
						</div>
						<div class="span4">
							<?php echo form_submit(
									array(
										'id' => 'agregar_nuevo_usuario',
										'class'=>'icon-floppy shortcut success',

										'name' => 'agregar_nuevo_usuario',
										'value' => 'Agregar'
									)
								); ?>
							<?php echo form_button(
								array(
									'name' => 'cancelar',
									'class'=>'shortcut  icon-cancel danger cancelar',
									'value' => 'Cancelar',
									'onclick' => 'window.location.assign('."'".site_url()."/listados/listar_organizaciones"."'".');'
									)
								);
							?>
						</div>
						<?php echo form_close(); ?>
					</div><!-- /row -->
				</div><!-- /grid -->
			</div><!-- /tile-area -->
	</body>
</html>