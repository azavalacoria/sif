<body class="metro">

	<div class="tile-area tile-area-darkTeal">
		<h1 class="tile-area-title fg-white">Registro de Personas</h1>
		<div class="user-id">
			<div class="user-id-image">
				<span class="icon-user no-display1"></span>
				<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
			</div>
			<div class="user-id-name">
				<span class="first-name">Usuario</span>
				<span class="last-name">Rol</span>
			</div>
		</div>

		<div class="grid" style="padding-left: 15%;">
			<script src="<?php echo base_url('js/jquery/jquery.number.min.js') ?>"></script>
			<script src="<?php echo base_url('js/catalogos/agregar_nueva_persona.js') ?>"></script>
		    <div class="row">
		    	<div class="span1"></div>
		    	<div class="span7">
					<div>
						<ul>
							<?php echo validation_errors('<li class="error-li">','</li>'); ?>
						</ul>
					</div>
					<div>
						<?php $atributos = array('id' => 'form_nueva_persona'); ?>
						<?php echo form_open('catalogos/agregar_nueva_persona', $atributos); ?>
							<?php //echo form_hidden('url_servicio', $url_servicio); ?>
							<?php echo form_input(
									array(
										'id' => 'url_servicio',
										'name' => 'url_servicio',
										'type' => 'hidden',
										'value' => $url_servicio,
									)
								);
							?>
						<div>
							<h3 class="fg-white"><?php echo form_label('Nombres'); ?></h3>
							<div class="input-control text">
							<?php echo form_input(
									array(
											'name'=>'nombres',
											'value'=>set_value('nombres'),
											'id' => 'nombres'
										)
								);
							?>
							<button class="btn-clear"></button>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Apellido Paterno'); ?></h3>
							<div class="input-control text">
							<?php echo form_input(
									array(
											'name'=>'apellido_paterno',
											'value'=>set_value('apellido_paterno'),
											'id' => 'apellido_paterno'
										)
								);
							?>
							<button class="btn-clear"></button>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Apellido Materno'); ?></h3>
							<div class="input-control text">
							<?php echo form_input(
									array(
											'name'=>'apellido_materno',
											'value'=>set_value('apellido_materno'),
											'id' => 'apellido_materno'
										)
								);
							?>
							<button class="btn-clear"></button>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('CURP'); ?></h3>
							<div class="input-control text">
								<?php echo form_input(
									array(
											'name'=>'curp',
											'value'=>set_value('curp'),
											'id' => 'curp'
										)
									);
								?>
							<button class="btn-clear"></button>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Sexo'); ?></h3>
							<div class="input-control select">
							<?php echo form_dropdown(
									'sexo',
									array('M' => 'Masculino', 'F'=>'Femenino'),
									set_value('sexo')
								);
							?>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Direccion'); ?></h3>
							<div class="input-control text">
							<?php echo form_input(
									array(
											'name'=>'direccion',
											'value'=>set_value('direccion'),
											'id' => 'direccion'
										)
								);
							?>
							<button class="btn-clear"></button>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Código Postal'); ?></h3>
							<div class="input-control text">
								<?php echo form_input(
										array(
											'name'=>'codigo_postal',
											'value'=>set_value('codigo_postal'),
											'id' => 'codigo_postal'
										)
									);
								?>
								<button class="btn-clear"></button>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Teléfono'); ?></h3>
							<div class="input-control text">
								<?php echo form_input(
									array(
										'name'=>'telefono',
										'value'=>set_value('telefono'),
										'id' => 'telefono'
										)
									);
								?>
								<button class="btn-clear"></button>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Sección Electoral'); ?></h3>
							<div class="input-control text">
							<?php echo form_input(
									array(
											'name'=>'seccion_electoral',
											'value'=>set_value('seccion_electoral'),
											'id' => 'seccion_electoral'
										)
								);
							?>
							<button class="btn-clear"></button>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Entidad Federativa'); ?></h3>
							<?php $js = 'id="entidad_federativa" onChange="obtener_municipios();"'; ?>
							<div class="input-control select">
							<?php echo form_dropdown(
									'entidad_federativa',
									$entidades,
									set_value('entidad_federativa'),
									$js
								);
							?>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Municipio'); ?></h3>
							<?php $js = 'id="municipio" onChange="obtener_localidades();"'; ?>
							<div class="input-control select">
							<?php echo form_dropdown(
									'municipio',
									array('' => 'Seleccione...'),
									set_value('municipio'),
									$js
								);
							?>
							</div>
						</div>
						<div>
							<h3 class="fg-white"><?php echo form_label('Localidad'); ?></h3>
							<?php $js = 'id="localidad"'; ?>
							<div class="input-control select">
							<?php echo form_dropdown(
									'localidad',
									array('' => 'Seleccione...'),
									set_value('localidad'),
									$js
								);
							?>
							</div>
						</div>
						
					</div>
		    	</div><!--/span7-->
		    	<div class="span4">
		    		<?php echo form_submit(
		    			array(
		    				'name' => 'agregar',
		    				'class'=>'icon-floppy shortcut success',
		    				'value' => 'Agregar'
		    				)
		    			);
		    			?>
					<?php echo form_button(
						array(
							'name' => 'cancelar',
							'class'=>'shortcut  icon-cancel danger cancelar',
							'value' => 'Cancelar',
							'onclick' => "window.location.assign('".site_url("listados/listar_personas")."');"
						)
					);
				?>
						</div>
						<?php echo form_close(); ?>

		    </div><!-- /row -->
		    </div><!-- /grid -->

	</div><!-- /tile-area -->
	





</body>
</html>