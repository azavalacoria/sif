<body class="metro">
<div class="tile-area tile-area-darkTeal">
	<script src="<?php echo base_url('js/metro/metro-listview.js') ?>"></script>
	<script src="<?php echo base_url('js/inventarios/catalogos-tipos-cuenta.js') ?>"></script>
	<script src="<?php echo base_url('js/jquery/jquery.number.min.js') ?>"></script>
	<script src="<?php echo base_url('js/inventarios/nuevo_concepto_entrada_formatos.js'); ?>"></script>
	<script src="<?php echo base_url('js/catalogos/agregar_nuevo_concepto_entrada.js') ?>"></script>
	<?php echo form_open('agregar_nuevo_concepto', array('id' => 'form_nuevo_concepto')); ?>

	<div class="grid fluid">

		<!-- inicio clase row -->
		<div class="row">
			<div class="span10 offset1">
				<p class="header fg-white">
					Agregar Concepto de Entrada
				</p>
			</div>

			<div class="span3">
				<div class="user-id">
					<div class="user-id-image">
						<span class="icon-user no-display1"></span>
						<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
					</div>
					<div class="user-id-name">
						<span class="first-name">Usuario</span>
						<span class="last-name">Rol</span>
					</div>
				</div>
			</div>
		</div>
		<!-- fin clase row -->

		<!-- inicio clase row -->
		<div class="row">
			<div>
				<ul><?php echo validation_errors('<li class="error-li">','</li>'); ?></ul>
			</div>
		</div>
		<!-- fin clase row -->

		<!-- inicio clase row -->
		<div class="row">
			<div class="span9 offset1">
				<?php echo form_input(
					array(
						'id' => 'url_servicio',
						'name' => 'url_servicio',
						'type' => 'hidden',
						'value' => $url_servicio,
					)
					); ?>
				<?php echo form_input(
					array(
						'id' => 'url_servicio_tipos_concepto',
						'name' => 'url_servicio_tipos_concepto',
						'type' => 'hidden',
						'value' => $url_servicio_tipos_concepto,
					)
					); ?>
				<div class="span5">
					<h3 class="fg-white"><?php echo form_label('Tipo de Cuenta'); ?></h3>
					<div class="input-control select">
						<?php $js = 'id="tipo_cuenta" onChange="obtener_tipos_concepto();"' ?>
						<?php echo form_dropdown('tipo_cuenta', $cuentas, set_value('tipo_cuenta'), $js); ?>
					</div>
				</div>
			</div>
			<div class="span1">
				<?php echo form_submit(
					array(
						'name' => 'agregar',
						'class'=>'icon-floppy shortcut success',
						'value' => 'Agregar'
						)
					); ?>
			</div>
		</div>
		<!-- fin clase row -->

		<!-- inicio clase row -->
		<div class="row">
			<div class="span9 offset1">
				<div id="entrada">
					<div>
						<h3 class="fg-white"><?php echo form_label('Nombre'); ?></h3>
						<div>
							<div class="input-control text">
								<?php echo form_input(
									array(
										'name' => 'nombre',
										'value' => set_value('nombre')
										)
										); ?>
								<button class="btn-clear"></button>
							</div>
						</div>
						
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Descripcion') ?></h3>
						<div class="input-control textarea">
							<?php echo form_textarea(
								array(
									'name' => 'descripcion',
									'value' => set_value('descripcion')
								)
								); ?>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Tipo de Concepto'); ?></h3>
						<div class="input-control select">
							<?php $js = 'id="tipo_concepto"'?>
							<?php echo form_dropdown('tipo_concepto', $tipos, set_value('tipo_concepto'), $js) ?>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Estado del Concepto'); ?></h3>
						<div class="input-control select">
							<?php echo form_dropdown('estado', $estados, set_value('estado')) ?>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Duracion en meses'); ?></h3>
						<div class="input-control text">
							<?php  echo form_input(
								array(
									'id' => 'duracion_meses',
									'name' => 'duracion_meses',
									'value' => set_value('duracion_meses')
								)
								); ?>
							<button class="btn-clear"></button>
						</div>
					</div>

					<div>
						<h3 class="fg-white"><?php echo form_label('Costo Publicado'); ?></h3>
						<div class="input-control text">
							<?php echo form_input(
										array(
											'id' => 'costo_publicado',
											'name' => 'costo_publicado',
											'value' => set_value('costo_publicado')
										)
									);
							?>
							<button class="btn-clear"></button>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Costo Autorizado'); ?></h3>
						<div class="input-control text">
							<?php echo form_input(
								array(
									'id' => 'costo_autorizado',
									'name' => 'costo_autorizado',
									'value' => set_value('costo_autorizado')
								)
								); ?>
							<button class="btn-clear"></button>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Costo Incrementado'); ?></h3>
						<div class="input-control text">
							<?php echo form_input(
								array(
									'id' => 'costo_incrementado',
									'name' => 'costo_incrementado',
									'value' => set_value('costo_incrementado')
								)
								); ?>
							<button class="btn-clear"></button>
						</div>
					</div>

					<div>
						<h3 class="fg-white"><?php echo form_label('Monto Acordado'); ?></h3>
						<div class="input-control text">
							<?php echo form_input(
								array(
									'id' => 'monto_acordado',
									'name' => 'monto_acordado',
									'value' => set_value('monto_acordado')
									)
									); ?>
							<button class="btn-clear"></button>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Forma de Pago'); ?></h3>
						<div class="input-control select">
							<?php echo form_dropdown('forma_pago', $formas, set_value('forma_pago')); ?>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Número de Pagos'); ?></h3>
						<div class="input-control text">
							<?php echo form_input(
								array(
									'id' => 'numero_pagos',
									'name' => 'numero_pagos',
									'value' => set_value('numero_pagos')
								)
								); ?>
							<button class="btn-clear"></button>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Secretaria Licitante'); ?></h3>
						<div class="input-control select">
							<?php echo form_dropdown('ente', $entes, set_value('ente')) ?>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Empresa Responsable de la Obra'); ?></h3>
						<div class="input-control select">
							<?php echo form_dropdown('responsable', $responsables, set_value('responsable')) ?>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Intermediario'); ?></h3>
						<div class="input-control select">
							<?php echo form_dropdown('intermediario', $intermediarios, set_value('intermediario')) ?>
						</div>
					</div>

					<div>
						<h3 class="fg-white"><?php echo form_label('Entidad Federativa'); ?></h3>
						<?php $js = 'id="entidad_federativa" onChange="obtener_municipios();"'; ?>
						<div class="input-control select">
							<?php echo form_dropdown('entidad_federativa' ,$entidades, '31', $js);?>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Municipio'); ?></h3>
						<?php $js = 'id="municipio" onChange="obtener_localidades();"'; ?>
						<div class="input-control select">
							<?php echo form_dropdown('municipio', $municipios, set_value('municipio'), $js); ?>
						</div>
					</div>
					<div>
						<h3 class="fg-white"><?php echo form_label('Localidad'); ?></h3>
						<?php $js = 'id="localidad"'; ?>
						<div class="input-control select">
							<?php echo form_dropdown('localidad', array(), set_value('localidad'), $js); ?>
						</div>
					</div>
			</div>

			<div id="salida" style="display: none">
			</div>
		</div> <!--/span7-->
		<div class="span1">
				<?php echo form_button(
					array(
						'name' => 'cancelar',
						'class'=>'shortcut  icon-cancel danger cancelar',
						'value' => 'Cancelar',
						'onclick' => "window.location.assign('".site_url('listados/listar_todos_conceptos')."');"
						)
						); 
				?>
		</div>	
	</div>
		<?php echo form_close(); ?>
	</div><!--/row-->
</div><!--/grid-->
</body>
</html>