<!DOCTYPE html>
<head>
<meta charset="UTF-8"> 
<title>
	<?php if (isset($titulo)) {
		echo "$titulo";
	} else {
		echo "Sistema de Registro de Información";
	}
	 ?>	
</title>
<?php echo link_tag('css/estilo.css'); ?>
<?php echo link_tag('css/tema-metro/css/metro-bootstrap.css'); ?>
<?php echo link_tag('css/estilo-busqueda-personas.css'); ?>
<script src="<?php echo base_url(); ?>js/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery/jquery.widget.min.js"></script>

<script src="<?php echo base_url(); ?>js/metro.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/validaciones.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/inventarios/generales.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/inventarios/catalogos-universo-yucatan.js'); ?>"></script>
</head>