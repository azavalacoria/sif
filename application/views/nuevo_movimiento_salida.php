<body class="metro">
	<div class="tile-area tile-area-darkTeal">
		<script src="<?php echo base_url('js/inventarios/pagos.js') ?>"></script>
		<script src="<?php echo base_url('js/inventarios/catalogos-buscar-persona.js'); ?>"></script>
		<div class="grid fluid">
			<!--- Inicio clase row -->
			<div class="row">
				<div class="span8 offset2">
					<p class="header fg-white" id="page-main-title">
						Nuevo Movimiento
					</p>
				</div>
				<div class="span3">
					<div class="user-id">
						<div class="user-id-image">
							<span class="icon-user no-display1"></span>
							<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
						</div>
						<div class="user-id-name">
							<span class="first-name">Usuario</span>
							<span class="last-name">Rol</span>
						</div>
					</div>
				</div>
			</div>

		<!-- fin clase row -->
		<!-- inicia clase row -->

		<div class="row">
			<div>
				<ul>
					<ul><?php echo validation_errors('<li class="error-li">','</li>'); ?></ul>
				</ul>
			</div>
		</div>

		<!-- fin clase row -->
		<?php echo form_input(
				array(
					'id' => 'url_servicio',
					'name' => 'url_servicio',
					'type' => 'hidden',
					'value' => "$url_servicio",
					)
				);
				?>
		<?php echo form_input(
				array(
					'id' => 'url_servicio_pagos',
					'name' => 'url_servicio_pagos',
					'type' => 'hidden',
					'value' => "$url_servicio_pagos",
					)
				);
				?>
		
		

		<!-- inicia clase row -->
		<div class="row">
			<div class="grid" id="search-grid">
				<div class="search-grid-cell">
					<h3 class="fg-white"><?php echo form_label('Apellido Paterno'); ?></h3>
					<?php
					
					echo form_input(array(
							'id' => 'apellido_paterno',
							'name' => 'apellido_paterno',
							'value' => set_value('apellido_paterno')
						));
					?>
				</div>
				<div class="search-grid-cell">
					<h3 class="fg-white"><?php echo form_label('Apellido Materno'); ?></h3>
					<?php
					echo form_input(array(
							'id' => 'apellido_materno',
							'name' => 'apellido_materno',
							'value' => set_value('apellido_materno')
						));
					?>
				</div>
				<div class="search-grid-cell">
					<h3 class="fg-white"><?php echo form_label('Nombres'); ?></h3>
					<?php
					echo form_input(array(
							'id' => 'nombres',
							'name' => 'nombres',
							'value' => set_value('nombres')
						));
					?>
				</div>
				<div>
					<button id="buscar-persona" onClick="buscar_persona();">Buscar</button>
					<button id="cancelar-busqueda" onClick="cancelar_busqueda_persona()">Cancelar búsqueda</button>
				</div>
				<div>
					<form id="personas-form">
						<table id="table-results-grid">
						</table>

					</form>
					<button id="seleccionar-persona">Seleccionar</button>
					<button id="cancelar-busqueda" onClick="cancelar_busqueda_persona()">Cancelar búsqueda</button>
				</div>
			</div>
		</div>
		<!-- fin clase row -->
		<div id="form-grid">
		<?php echo form_open('nuevo_movimiento_salida'); ?>
		<!-- inicia clase row -->
		<div class="row">
			<div class="span7 offset2">
				<div>
					<h3 class="fg-white"><?php echo form_label('Tipo de cuenta'); ?></h3>
					<div class="input-control select">
						<?php $js = 'id="tipo_cuenta" onChange="buscar_conceptos_por_tipo_cuenta();"'; ?>
						<?php echo form_dropdown('tipo_cuenta', $cuentas, set_value('tipo_cuenta'), $js); ?>
					</div>
				</div>
			</div>
			<div class="span1">
				<?php echo form_submit(
						array(
							'name' => 'agregar',
							'class'=>'icon-floppy shortcut success',
							'value' => 'Agregar'
						)
					);
				?>
			</div>
		</div>
		<!-- fin clase row -->
		<!-- inicia clase row -->
		
		<div class="row">
			<div class="span7 offset2">
				<div>
					<h3 class="fg-white"><?php echo form_label('Fecha en que se realiza el cobro') ?></h3>
					<div class="input-control text" data-role="datepicker">
						<?php echo form_input(
							array(
								'name' => 'fecha',
								'value' => set_value('fecha')
								)
							);
						?>
                        <button class="btn-date"></button>
                    </div>
				</div>
				<div>
					<h3 class="fg-white"><?php echo form_label('Concepto'); ?></h3>
					<div class="input-control select">
						<?php $js = 'id="concepto" onChange=""'; ?>
						<?php echo form_dropdown('concepto', $conceptos, set_value('concepto'), $js); ?>
					</div>
				</div>
				<div>
					<h3 class="fg-white"><?php echo form_label('Responsable'); ?></h3>
					<?php
					echo form_input(
						array(
							'id' => 'persona',
							'name' => 'persona',
							'type' => 'hidden',
							'value' => set_value('persona'),
							)
						);
						?>
					<div class="input-control text">
						<?php
						echo form_input(
							array(
									'id' => 'persona_nombre',
									'name' => 'persona_nombre',
									'onClick' => 'desplegar_formulario_busqueda();',
									'value' => set_value('persona_nombre'),
								)
							);
							?>
					</div>
				</div>
				<div>
					<h3 class="fg-white"><?php echo form_label('Importe') ?></h3>
					<div class="input-control text">
						<?php echo form_input(
								array(
									'data-role'=>'datepicker',
									'name' => 'monto',
									'value' => set_value('monto')
								)
							);
						?>
						<button class="btn-clear"></button>
					</div>
				</div>
				<div>
					<h3 class="fg-white"><?php echo form_label('Forma de Pago'); ?></h3>
					<div class="input-control select">
						<?php echo form_dropdown('forma_pago', $formas, set_value('forma_pago')) ?>
					</div>
				</div>
				<div>
					<h3 class="fg-white"><?php echo form_label('Gasto por comprobar') ?></h3>
					<div class="input-control radio">
						<label class="fg-green">
							<?php echo form_radio(
								array(
									'name' => 'gasto_comprobar',
									'value' => '1',
									'group' => 'intermediarios'
									)
								);
								?>
							<span class="check"></span>
							Si
						</label>
					</div>
					<div class="input-control radio">
						<label class="fg-lightRed">
							<?php echo form_radio(
								array(
									'name' => 'gasto_comprobar',
									'value' => '0',
									'group' => 'intermediarios'
									)
								);
								?>
						<span class="check"></span>
						No
					</label>
				</div>
			</div>
				
			</div>
			<!--/span7-->
			<div class="span1">
				<?php echo form_button(
						array(
							'name' => 'cancelar',
							'class'=>'shortcut  icon-cancel danger cancelar',
							'value' => 'Cancelar',
							'onclick' => "window.location.assign('".site_url('usuario')."');"
						)
					);
				?>
			</div>
			
		</div>
		<?php echo form_close(); ?>
		</div>
	<!--/row-->
	</div>
	<!--/grid-->
</div>
</body>
</html>