<body class="metro">
	<script type="text/javascript" src="<?php echo base_url('js/inventarios/cambiar_estado_elemento_catalogo.js') ?>"></script>
	<div class="grid" style="padding-left: 4%;">
		<div class="row">
			<div class="span2"></div>
				<div class="span12">
					<h1 class="tile-area-title fg-white">
						Listado de Licitaciones
						<?php echo $this->session->userdata('id_amigo'); ?>
					</h1>
					<?php echo form_open('catalogos/modificar_puesto'); ?>
					<div class="text-right">
						<?php echo form_input(
									array(
										'id' => 'url_servicio',
										'name' => 'url_servicio',
										'type' => 'hidden',
										'value' => $url_servicio,
									)
								); ?>
						<?php echo form_input(
									array(
										'id' => 'modelo',
										'name' => 'modelo',
										'type' => 'hidden',
										'value' => $modelo,
									)
								); ?>
						<?php echo form_button(
									array(
										'class'=>'icon-plus-2 large success',
										'value' => 'Agregar',
										'title' => 'Agregar',
										'onclick' => "window.location.assign('".site_url('nocomprometido/agregar_licitacion')."');"
									)
								); ?>
						<?php 
						/*
						echo form_button(
									array(
										'class'=>'large icon-pencil btn warning',
										'value' => 'Modificar',
										'title' => 'Modificar',
										'type' => 'submit'
									)
								); */ 
								?>
						<?php echo form_button(
									array(
										'name' => 'cancelar',
										'class'=>'large  icon-home danger cancelar',
										'value' => 'Cancelar',
										'title' => 'Inicio',
										'onclick' => "window.location.assign('".site_url('login/salir_amigo')."');"
									)
								); ?>
							<br><br>
					</div>
					<table class="table striped bordered hovered">
						<tr>
							<th>Opcion</th>
							<th>Clave</th>
							<th>Nombre</th>
							<th>Descripcion</th>
							<th>Secretaria</th>
							<th>Estado</th>
							<th>Prioridad</th>
						</tr>
						<?php foreach ($licitaciones as $licitacion) { ?>
						<tr>
							<td>
								<div class="input-control radio">
									<label>
										<?php echo form_radio(
													array(
														'name' => 'id_licitacion',
														'value' => $licitacion['id_licitacion'],
														'group' => 'licitaciones'
														)
													);
												?>
										<span class="check"></span>
									</label>
								</div>
							</td>
							<td><?php echo $licitacion['clave']; ?></td>
							<td><?php echo $licitacion['nombre_licitacion']; ?></td>
							<td><?php echo $licitacion['descripcion']; ?></td>
							<td><?php echo $licitacion['nombre_secretaria']; ?></td>
							<td><?php echo $licitacion['nombre_estado']; ?></td>
							<td><?php echo $licitacion['prioridad']; ?></td>
						</tr>
						<?php } ?>
					</table>
					<?php echo form_close(); ?>
					<div>
						<?php echo form_open('nocomprometido/listar_licitaciones'); ?>

						<?php echo form_button(
										array(
											'name' => 'opcion',
											'class' => 'success',
											'value' => 'dec',
											'type' => 'submit',
											'content' => 'Anteriores'
										)
									); ?>
						<?php echo form_button(
										array(
											'name' => 'opcion',
											'class' => 'inverse',
											'value' => 'inc',
											'type' => 'submit',
											'content' => 'Siguientes'
										)
									); ?>
						<?php echo form_close(); ?>
					</div>
				</div><!-- fin span8-->
			<div class="span2"></div>
		</div><!-- fin row-->
	</div><!-- fin grid-->
</body>