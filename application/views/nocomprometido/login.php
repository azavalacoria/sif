<!DOCTYPE html>
<head>
<meta charset="UTF-8"> 
<title>
	<?php if (isset($titulo)) {
		echo "$titulo";
	} else {
		echo "Iniciar Sesión";
	}
	 ?>
	
</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/tema-metro/css/metro-bootstrap.css" />
<script src="<?php echo base_url('css/tema-metro/js/jquery/jquery.min.js'); ?>"></script>

</head>

<body class="metro">

<h1 class="tile-area-title fg-white text-center" style="padding-bottom: 5%;">Sistema de Registro de Información</h1>
	<div class="grid" style="padding-left: 15%;">

		<div class="row">

			<div class="span4">
				<div>
					<ul>
						<?php if (isset($mensaje)) {
							echo '<li class="validation-errors-list">'.$mensaje.'</li>';
						} ?>
						<?php echo validation_errors('<li class="validation-errors-list">','</li>'); ?>
					</ul>
				</div>
			</div>
			<div class="span4">
				
			<?php $attributes = array('class' => 'form-2'); ?>
			<?php echo form_open('nocomprometido/login', $attributes); ?>
			
			<h3 class="text-center"><span class="log-in">Iniciar Sesión</span></h3>
			<h3><?php echo form_label('<i class="icon-user"></i> Nombre de Usuario'); ?><h3>

			<div class="input-control text">

				<?php
				echo form_input(
					array(
							'id' => 'nickname',
							'name' => 'nickname',
							'placeholder' => 'Usuario',
							'value' => set_value('nickname')
						)
				); ?>
				<button class="btn-clear"></button>
			</div>

			<h3><?php echo form_label('<i class="icon-locked"></i>Contraseña'); ?></h3>

			<div class="input-control password">
				<?php
				echo form_password(
					array(
							'id' => 'password',
							'class' => 'showpassword',
							'name' => 'password',
							'placeholder' => 'Contraseña',
							'value' => set_value('password')
						)
				); ?>
				<button class="btn-reveal"></button>
			</div>
			
			<?php echo form_submit(array('id' => 'iniciar','class' => 'success','name' => 'iniciar', 'value' => 'Iniciar Sesión')); ?>
			<?php echo form_close(); ?>
		</div>
		<!--/row-->
	</div>
</div>
</body>
</html>