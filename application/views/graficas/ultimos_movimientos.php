<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Ultimos Movimientos</title>
		<link rel="stylesheet" href="<?php echo base_url('css/Grafica/style.css'); ?>"	type="text/css">

		<script src="<?php echo base_url('js/Graficas/amcharts.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('js/Graficas/serial.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('js/Graficas/amstock.js'); ?>" type="text/javascript"></script>		
		
		<script type="text/javascript">

			var chartData1 = [];
			var chartData2 = [];
			var chartData3 = [];
			
			generateChartData();
			
			function generateChartData() {
				var firstDate = new Date();
				firstDate.setDate(firstDate.getDate() - 50);
				firstDate.setHours(0, 0, 0, 0);
			
				for (var i = 0; i < 50; i++) {
					var newDate = new Date(firstDate);
					newDate.setDate(newDate.getDate() + i);
			
					var a1 = Math.round(Math.random() * (40 + i)) + 100 + i;
					var b1 = Math.round(Math.random() * (1000 + i)) + 500 + i * 2;

					var a2 = Math.round(Math.random() * (100 + i)) + 200 + i;
					var b2 = Math.round(Math.random() * (1000 + i)) + 600 + i * 2;
			
					var a3 = Math.round(Math.random() * (100 + i)) + 200;
					var b3 = Math.round(Math.random() * (1000 + i)) + 600 + i * 2;
			
					chartData1.push({
						date: newDate,
						value: a1,
						volume: b1
					});
					chartData2.push({
						date: newDate,
						value: a2,
						volume: b2
					});
					chartData3.push({
						date: newDate,
						value: a3,
						volume: b3
					});
				}
			}
			
			AmCharts.makeChart("chartdiv", {
				type: "stock",
				pathToImages: "<?php echo base_url('images').'/'; ?>",
			
				dataSets: [{
					title: "Entradas",
					fieldMappings: [{
						fromField: "value",
						toField: "value"
					}, {
						fromField: "volume",
						toField: "volume"
					}],
					dataProvider: chartData1,
					categoryField: "date"
				},
			
				{
					title: "Salidas",
					fieldMappings: [{
						fromField: "value",
						toField: "value"
					}, {
						fromField: "volume",
						toField: "volume"
					}],
					dataProvider: chartData2,
					categoryField: "date"
				},
			
				{
					title: "Cuentas Por Pagar",
					fieldMappings: [{
						fromField: "value",
						toField: "value"
					}, {
						fromField: "volume",
						toField: "volume"
					}],
					dataProvider: chartData3,
					categoryField: "date"
				},],
			
				panels: [{
			
					showCategoryAxis: false,
					title: "Movimientos",
					percentHeight: 70,
			
					stockGraphs: [{
						id: "g1",
			
						valueField: "value",
						comparable: true,
						compareField: "value",
						bullet: "round",
						bulletBorderColor: "#FFFFFF",
						bulletBorderAlpha: 1,
						balloonText: "[[title]]:<b>[[value]]</b>",
						compareGraphBalloonText: "[[title]]:<b>[[value]]</b>",
						compareGraphBullet: "round",
						compareGraphBulletBorderColor: "#FFFFFF",
						compareGraphBulletBorderAlpha: 1
					}],
			
					stockLegend: {
						periodValueTextComparing: "[[percents.value.close]]%",
						periodValueTextRegular: "[[value.close]]"
					}
				},
			
				{
					title: "Cantidad en Pesos",
					percentHeight: 30,
					stockGraphs: [{
						valueField: "volume",
						type: "column",
						showBalloon: false,
						fillAlphas: 1
					}],
			
			
					stockLegend: {
						periodValueTextRegular: "[[value.close]]"
					}
				}],
			
				chartScrollbarSettings: {
					graph: "g1"
				},
			
				chartCursorSettings: {
					valueBalloonsEnabled: true
				},
			
				periodSelector: {
					position: "left",
					periods: [{
						period: "DD",
						count: 10,
						label: "10 Días"
					}, {
						period: "MM",
						selected: true,
						count: 1,
						label: "1 Mes"
					}, {
						period: "YYYY",
						count: 1,
						label: "1 Año"
					}, {
						period: "YTD",
						label: "Por Año"
					}, {
						period: "MAX",
						label: "Todo"
					}]
				},
			
				dataSetSelector: {
					position: "left"
				}
			});
		</script>
	</head>
	<body style="background-color:#FFFFFF">
		<div id="chartdiv" style="width:100%; height:600px;"></div>
	</body>

</html>