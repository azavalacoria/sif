<div class="window inactive">
	<div class="caption">
		<span class="icon icon-cancel"></span>
		<div class="title">Error al agregar el nuevo registro</div>

		<!--
		<button class="btn-min"></button>
		<button class="btn-max"></button>
		-->
    	<button class="btn-close" onclick=window.location.assign("<?php echo $url_cerrar; ?>")></button>
    </div>
    <div class="content" style="padding:30px;text-align:center;">
    	<br>
    	<span>No se pudieron registrar los datos</span>
    	<div>
    		<?php foreach ($urls as $mensaje => $url) { ?>
	    		<button onclick=window.location.assign("<?php echo $url ?>")>
	    			<?php echo $mensaje; ?>
	    		</button>
			<?php } ?>
		</div>
    </div>
</div>