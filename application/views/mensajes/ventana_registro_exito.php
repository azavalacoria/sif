<div class="window shadow">
	<div class="caption">
		<span class="icon icon-checkmark"></span>
		<div class="title">
			<?php
			if (isset($titulo)) {
				echo $titulo;
			} else {
				echo "Se agrego un nuevo registro";
			}
			?>
		</div>

		<!--
		<button class="btn-min"></button>
		<button class="btn-max"></button>
		-->
    	<button class="btn-close" onclick=window.location.assign("<?php echo $url_cerrar; ?>")></button>
    </div>
    <div class="content" style="padding:30px;text-align:center;">
    	<br>
    	<span>
    		<?php
    		if (isset($mensaje)) {
    			echo "$mensaje";
    		} else {
    			echo "Sus datos han sido registrados con éxito.";
    		}
    		?>
    	</span>
    	<div>
    		<?php foreach ($urls as $mensaje => $url) { ?>
	    		<button onclick=window.location.assign("<?php echo $url ?>")>
	    			<?php echo $mensaje; ?>
	    		</button>
			<?php } ?>
		</div>
    </div>
</div>