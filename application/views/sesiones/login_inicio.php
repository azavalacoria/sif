<body class="metro">


<h1 class="tile-area-title fg-white text-center" style="padding-bottom: 5%;">Sistema de Registro de Información</h1>
	<div class="grid" style="padding-left: 15%;">

		<div class="row">

			<div class="span4"></div>
			<div class="span4">
				
			<?php $attributes = array('class' => 'form-2'); ?>
			<?php echo form_open('login/iniciar', $attributes); ?>
			
			<h3 class="text-center"><span class="log-in">Iniciar Sesión</span></h3>

				<h3><?php echo form_label('<i class="icon-user"></i> Nombre de Usuario'); ?><h3>
						<div class="input-control text">
						<?php
						echo form_input(
						array(
								'id' => 'nickname',
								'name' => 'nickname',
								'placeholder' => 'Usuario',
								'value' => set_value('nickname')
							)
							);
						?>
						<button class="btn-clear"></button>
						</div>

						<h3><?php echo form_label('<i class="icon-locked"></i>Contraseña'); ?></h3>
						<div class="input-control password">
						<?php
						echo form_password(
								array(
										'id' => 'password',
										'class' => 'showpassword',
										'name' => 'password',
										'placeholder' => 'Contraseña',
										'value' => set_value('password')
									)
							);
						?>
						 <button class="btn-reveal"></button>
						</div>

						<div>
					<ul>
						<?php echo validation_errors('<li style="margin-bottom:5px;width:100%;background: #B80000;border-radius: 5px;font-size: 12px;color: #FFF;padding: 5px;display:block;">','</li>'); ?>
					</ul>
				</div>

						
						<?php
						echo form_submit(array('id' => 'iniciar','class' => 'success','name' => 'iniciar', 'value' => 'Iniciar Sesión'));
						?>


				
			<?php echo form_close(); ?>
		</div>
		<div class="span4"></div>
	<!--/row-->
	</div>
	<!--/grid-->
</div>
</body>
</html>