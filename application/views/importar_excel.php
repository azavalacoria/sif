<body class="metro">
	<div class="tile-area tile-area-darkTeal">
			<h1 class="tile-area-title fg-white">Importar Licitaciones Vía Excel</h1>
				<div class="user-id">
					<div class="user-id-image">
						<span class="icon-user no-display1"></span>
						<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
					</div>
					<div class="user-id-name">
						<span class="first-name">Usuario</span>
						<span class="last-name">Rol</span>
					</div>
				</div>
				<div class="grid" style="padding-left: 15%;">
					<div class="row">
						<div class="span1"></div>
						<div class="span7">

							<form action="<?php echo base_url() ?>index.php/importar_excel/importar" method="post" enctype="multipart/form-data">
							    <p>
							    	
							    	<h3 class="fg-white"><?php echo form_label('Seleccionar Excel: '); ?></h3>
							    	<input type="file" name="file" data-transform="input-control">
							    	<input type="submit" value="Listar" class="warning">
							
							    </p>
							</form>
						
						</div>
						
					</div><!-- /row -->
				</div><!-- /grid -->
			</div><!-- /tile-area -->
	</body>
</html>