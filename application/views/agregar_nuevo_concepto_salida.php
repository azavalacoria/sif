<body class="metro">

<div class="tile-area tile-area-darkTeal">
	<script src="<?php echo base_url('js/metro/metro-listview.js') ?>"></script>
	<script src="<?php echo base_url('js/inventarios/catalogos-tipos-cuenta.js') ?>"></script>
	<script src="<?php echo base_url('js/catalogos/agregar_nuevo_concepto_salida.js') ?>"></script>
	
	<!-- inicio clase grid -->
	<?php echo form_open('agregar_nuevo_concepto_salida', array('id' => 'form_nuevo_concepto_salida')); ?>
	<div class="grid fluid">
		<!-- inicio clase row -->

		<div class="row">
			<div class="span10 offset1">
				<p class="header fg-white">
					Agregar Concepto de salida
				</p>
			</div>

			<div class="span3">
				<div class="user-id">
					<div class="user-id-image">
						<span class="icon-user no-display1"></span>
						<img src="docs/images/Battlefield_4_Icon.png" class="no-display">
					</div>
					<div class="user-id-name">
						<span class="first-name">Usuario</span>
						<span class="last-name">Rol</span>
					</div>
				</div>
			</div>
		</div>
		<!-- fin clase row -->

		<!-- inicio clase row -->
		<div class="row">
			<div>
				<ul><?php echo validation_errors('<li class="error-li">','</li>'); ?></ul>
			</div>
		</div>
		<!-- fin clase row -->

		<!-- inicio clase row -->
		<div class="row">
			<!--<div class="span1"></div>-->
			<div class="span9 offset1">
				<?php echo form_input(
					array(
						'id' => 'url_servicio',
						'name' => 'url_servicio',
						'type' => 'hidden',
						'value' => $url_servicio,
						) ); ?>
				<?php echo form_input(
					array(
						'id' => 'url_servicio_tipos_concepto',
						'name' => 'url_servicio_tipos_concepto',
						'type' => 'hidden',
						'value' => $url_servicio_tipos_concepto,
						) ); ?>
				
				<div class="span8">
					<div>
						<h3 class="fg-white"><?php echo form_label('Tipo de Cuenta'); ?></h3>
						<div class="input-control select">
							<?php $js = 'id="tipo_cuenta" onChange="obtener_tipos_concepto();"' ?>
							<?php echo form_dropdown('tipo_cuenta', $cuentas, set_value('tipo_cuenta'), $js); ?>
						</div>
					</div>
				</div>
				
			</div>
			<div class="span1">
				<?php echo form_submit(
					array(
						'name' => 'agregar',
						'class'=>'icon-floppy shortcut success',
						'value' => 'Agregar'
						) ); ?>
			</div> 
			<!--/span7-->
		</div><!--/row-->


		<!-- inicio clase row -->

		<div class="row">
			<div class="span9 offset1">
				<div id="salida" style="display: none">
					<div>
									<h3 class="fg-white"><?php echo form_label('Nombre'); ?></h3>
									<div class="input-control text">
										<?php echo form_input(
											array(
												'id' => 'nombre_gasto',
												'name' => 'nombre_gasto',
												'value' => set_value('nombre_gasto')
												) ); ?>
										<button class="btn-clear"></button>
									</div>
								</div>
								<div>
									<h3 class="fg-white"><?php echo form_label('Descripcion') ?></h3>
									<div class="input-control textarea">
										<?php echo form_textarea(
											array(
												'name' => 'descripcion_gasto',
												'value' => set_value('descripcion_gasto')
												) ); ?>
									</div>
								</div>
								<div>
									<h3 class="fg-white"><?php echo form_label('Tipo de Concepto'); ?></h3>
									<div class="input-control select">
										<?php $js = 'id="tipo_concepto_gasto"'?>
										<?php echo form_dropdown('tipo_concepto_gasto', $tipos, set_value('tipo_concepto_gasto'), $js) ?>
									</div>
								</div>
								<div>
									<h3 class="fg-white"><?php echo form_label('Estado del Concepto'); ?></h3>
									<div class="input-control select">
										<?php echo form_dropdown('estado_gasto', $estados, set_value('estado_gasto')) ?>
									</div>
								</div>
				</div>
			</div>

			<div class="span1">
				<?php echo form_button(
					array(
						'name' => 'cancelar',
						'class'=>'shortcut  icon-cancel danger cancelar',
						'value' => 'Cancelar',
						'onclick' => "window.location.assign('".site_url('listados/listar_todos_conceptos')."');"
						) ); ?>
			</div>
		</div>
		<!-- fin clase row -->
		<?php echo form_close(); ?>
	</div><!--/grid-->
</body>
</html>