<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="product" content="Metro UI CSS Framework">
	<meta name="description" content="Simple responsive css framework">
	<meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">

	<?php echo link_tag('metro/metro-bootstrap.css'); ?>
	<?php echo link_tag('metro/metro-bootstrap-responsive.css'); ?>
	<?php echo link_tag('metro/docs.css') ?>
	<?php echo link_tag('js/prettify/prettify.css'); ?>

	<!--<script src="<?php //echo base_url('js/jquery/jquery.min.js'); ?>"></script>-->
	<script src="<?php echo base_url('js/jquery/jquery-2.1.0.min.js'); ?>"></script>
	<script src="<?php echo base_url('js/jquery/jquery.widget.min.js'); ?>"></script>
	<script src="<?php echo base_url('js/jquery/jquery.mousewheel.js'); ?>"></script>
	<script src="<?php echo base_url('js/jquery/jquery.easing.1.3.min.js'); ?>"></script>
	<script src="<?php echo base_url('js/prettify/prettify.js'); ?>"></script>
	<script src="<?php echo base_url('js/inventarios/generales.js'); ?>"></script>

	<!-- Metro UI CSS JavaScript plugins -->
	<script src="<?php echo base_url('js/load-metro.js'); ?>"></script>

	<!-- Local JavaScript -->
	<script src="<?php echo base_url('js/docs.js'); ?>"></script>
	<script src="<?php echo base_url('js/github.info.js'); ?>"></script>
	<script src="<?php echo base_url('js/start-screen.js'); ?>"></script>

	<title>Sistema de Regristro de Información</title>
</head>
<body class="metro">
	<div>
		<div class="tile-area tile-area-darkTeal">
			<h1 class="tile-area-title fg-white">Inicio</h1>
			<div class="user-id">
				<div class="user-id-name" style="float:left;">
					<span class="first-name">
						<?php
						$usuario = $this->session->userdata('nombre_usuario');
						if (isset($usuario)) {
							echo "$usuario";
						} else {
							echo "Usuario";
						}
						?>
					</span>
					<span class="last-name">
						<?php
						$puesto = $this->session->userdata('puesto');
						if (isset($puesto)) {
							echo "$puesto";
						} else {
							echo "Puesto";
						}
						?>
					</span>
				</div>
				<div class="user-id-image" style="float:left;">
					<span class="icon-user no-display1"></span>
					<img src="<?php echo base_url('images/Battlefield_4_Icon.png')?>" class="no-display">
				</div>
				<div class="toolbar transparent" style="float:left;">
					<button Onclick="window.location.assign('index.php/login/limpiar');">
						Salir
						<i class="icon-locked"></i>
					</button>
				</div>
			</div>
			<div class="tile-group six">

			<a class="tile double bg-lightBlue live" data-role="live-tile" data-effect="slideUp">
				<div class="tile-content email">
					<div class="email-image">
						<img src="<?php echo base_url('images/foto1.png') ?>">
					</div>
					<div class="email-data">
						<span class="email-data-title">Administrador</span>
						<span class="email-data-subtitle">Impuesto</span>
						<span class="email-data-text">Aumento en los Impuestos</span>
					</div>
				</div>
				<div class="tile-content email">
					<div class="email-image">
						<img src="<?php echo base_url('images/jolie.jpg') ?>">
					</div>
					<div class="email-data">
						<span class="email-data-title">Sistemas</span>
						<span class="email-data-subtitle">Adecuaciones</span>
						<span class="email-data-text">Lista de los cambios nuevos al sistema </span>
					</div>
				</div>
				<div class="tile-content email">
					<div class="email-image">
						<img src="<?php echo base_url('images/shvarcenegger.jpg') ?>">
					</div>
					<div class="email-data">
						<span class="email-data-title">Parra</span>
						<span class="email-data-subtitle">Nuevos Cheques</span>
						<span class="email-data-text">Lista de Nuevos cheques emitidos </span>
					</div>
				</div>
				<div class="brand">
					<div class="label"><h3 class="no-margin fg-white"><span class="icon-mail"></span></h3></div>
					<div class="badge">3</div>
				</div>
			</a> <!-- end tile -->

			<a class="tile double bg-violet">
				<div class="tile-content" style="overflow: visible">
					<div class="input-control text span2 place-left margin10" style="margin-left: 10px" data-role="datepicker">
						<input type="text" name="sel_date">
						<button class="btn-date"></button>
					</div>
					<div class="text-right padding10 ntp">
						<h1 class="fg-white no-margin">24</h1>
						<p class="fg-white">Diciembre</p>
					</div>
				</div>
				<div class="brand">
					<div class="label"><h3 class="no-margin fg-white"><span class="icon-calendar"></span></h3></div>
				</div>
			</a> <!-- Modulo para agregar un Nuevo Concepto -->

			<a class="tile bg-darkRed" onclick=window.location.assign("index.php/agregar_nuevo_concepto")>
				<div class="tile-content icon">
					<span class="icon-chart-alt"></span>
				</div>
				<div class="brand">
					<div class="label"><h3 class="no-margin fg-white"><span class="icon-plus-2"></span></h3></div>
					<div class="label">Concepto</div>
				</div>
			</a>
			<!-- Fin de Modulo para agregar un nuevo concepto -->

			<!-- Modulo para agregar un Responsable de proyecto -->
			
			<a class="tile bg-darkBrown" href="<?php echo site_url('catalogos/agregar_nuevo_responsable_proyecto'); ?>">
				<div class="tile-content icon">
					<span class="icon-user-3"></span>
				</div>
				<div class="brand">
					<div class="label"><h3 class="no-margin fg-white"><span class="icon-plus-2"></span></h3></div>
					<div class="label">Responsable de Proyecto</div>
				</div>
			</a>
			
			<!-- Fin de modulo para agregar a un nuevo lider de proyecto -->

			<a class="tile double double-vertical bg-steel">
				<div class="tile-content" style="background: url(metro/images/clouds2.png)">
					<div class="padding10">
						<h1 class="fg-white ntm">57&deg;</h1>
						<h2 class="fg-white no-margin">Mérida, Yucatán</h2>
						<h5 class="fg-white no-margin">Día Nublado</h5>
						<p class="tertiary-text fg-white no-margin">Hoy</p>
						<p class="tertiary-text fg-white">63&deg;/55&deg; LLuvias</p>
						<p class="tertiary-text fg-white no-margin">Mañana</p>
						<p class="tertiary-text fg-white">64&deg;/54&deg; Nublados</p>
					</div>

				</div>
				<div class="tile-status">
					<div class="label"></div>
				</div>
			</a> <!-- end tile -->

			<div class="tile double">
				<div class="tile-content image-set">
					<img src="<?php echo base_url('images/jeki_chan.jpg') ?>">
					<img src="<?php echo base_url('images/jek_vorobey.jpg') ?>">
					<img src="<?php echo base_url('images/jolie.jpg') ?>">
					<img src="<?php echo base_url('images/obama.jpg') ?>">
					<img src="<?php echo base_url('images/shvarcenegger.jpg') ?>">
				</div>
				<div class="brand bg-dark opacity">
					<span class="text">
						Ultimos Usuarios Registrados
					</span>
				</div>

			</div> <!-- end tile -->

			<!-- Modulo para agregar una Persona al sistema -->
			
			<a class="tile bg-darkBlue" id="vista_addpersona" href="<?php echo site_url('catalogos/agregar_nueva_persona') ?>">
				<div class="tile-content icon">
					<span class="icon-accessibility"></span>
				</div>
				<div class="brand">
					<div class="label"><h3 class="no-margin fg-white"><span class="icon-plus-2"></span></h3></div>
					<div class="label">Persona</div>
				</div>
			</a>
			<!-- Fin de modulo para agregar a una nueva persona al sistema -->

			<!-- Modulo para agregar un nuevo usuario del sistema -->
			<a class="tile bg-darkOrange" href="<?php echo site_url('catalogos/agregar_nuevo_usuario') ?>">
				<div class="tile-content icon">
					<span class="icon-user"></span>
				</div>
				<div class="brand">
					<div class="label"><h3 class="no-margin fg-white"><span class="icon-plus-2"></span></h3></div>
					<div class="label">Usuario</div>
				</div>
			</a>
			<!-- Fin de modulo para agregar un nuevo usuario al sistema -->
			
			<!-- Modulo para los ultimos movimientos -->
			<a class="tile double bg-darkGreen" href="<?php echo site_url('graficas/ultimos_movimientos') ?>">
				<div class="tile-content icon">
					<span class="icon-bars"></span>
				</div>
				<div class="brand">
					<div class="label">Ultimos movimientos</div>
				</div>
			</a>
			<!-- Fin de Modulo Ultimos Movimientos -->


			<!-- Modulo para mostrar los reportes que proporciona el sistema -->
			<a class="tile double bg-darkMagenta">
				<div class="tile-content icon">
					<span class=" icon-stats-2"></span>
				</div>
				<div class="brand">
					<div class="label">Reportes</div>
				</div>
			</a>
			<!-- Fin de modulo para Mostrar los reportes que proporciona el sistema -->

		</div> <!-- End group -->

		<!-- Grupo de Catalogos -->
		<div class="tile-group double">
			
			<div class="tile-group-title">Catálogos</div>

			<!-- Catalogo de Concepto -->
			<a class="tile half bg-darkRed" data-hint="Catálogo de Conceptos" data-hint-position="left" href="<?php echo site_url('listados/listar_todos_conceptos'); ?>">
				<div class="tile-content icon">
					<span class="icon-chart-alt"></span>
				</div>
			</a>

			<!-- Catalogo de Responsable del proyecto -->
			<a class="tile half bg-darkBrown" data-hint="Catálogo de Responsables de Proyecto" data-hint-position="left" href="<?php echo site_url('listados/listar_responsables_proyectos') ?>">
				<div class="tile-content icon">
					<span class="icon-user-3"></span>
				</div>
			</a>

			<!-- Catalogo de Persona -->
			<a class="tile half bg-darkBlue" data-hint="Catálogo de Personas" data-hint-position="left" href="<?php echo site_url('listados/listar_personas') ?>">
				<div class="tile-content icon">
					<span class="icon-accessibility"></span>
				</div>
			</a>

			<!-- Catalogo de Usuarios -->
			<a class="tile half bg-darkOrange" data-hint="Catálogo de Usuarios" data-hint-position="left" href="<?php echo site_url('listados/listar_usuarios') ?>">
				<div class="tile-content icon">
					<span class="icon-user"></span>
				</div>
			</a>

			<!-- Catalogo de Entradas -->
			<a class="tile half bg-darkCobalt" data-hint="Catálogo de Entradas" data-hint-position="left" onClick="alert('Catálogo Pendiente');">
				<div class="tile-content icon">
					<span class="icon-download"></span>
				</div>
			</a>

			<!-- Catalogo de Salidas -->
			<a class="tile half bg-darkMagenta" data-hint="Catálogo de Salidas" data-hint-position="left" onClick="alert('Catálogo Pendiente');">
				<div class="tile-content icon">
					<span class="icon-upload"></span>
				</div>
			</a>

			<!-- Catalogo de Gastos Internos -->
			<a class="tile half bg-darkCrimson" data-hint="Catálogo de Gastos Internos" data-hint-position="left" onClick="alert('Catálogo Pendiente');">
				<div class="tile-content icon">
					<span class="icon-cars"></span>
				</div>
			</a>

			<!-- Catalogo Gastos Por Comprobar -->
			<a class="tile half bg-darkEmerald" data-hint="Catálogo de Gastos por Comprobrar" data-hint-position="left" onClick="alert('Catálogo Pendiente');">
				<div class="tile-content icon">
					<span class="icon-shipping"></span>
				</div>
			</a>

			<!-- Catalogo Agregar Nueva Forma de Pago -->
			<a class="tile half bg-lightOrange" href="<?php echo site_url('listados/listar_formas_pago') ?>" data-hint="Catálogo de Formas de Pago" data-hint-position="left">
				<div class="tile-content icon">
					<span class="icon-new"></span>
				</div>
			</a>

			<!-- Catalogo Agregar Nueva Organización -->
			<a class="tile half bg-darkBrown" href="<?php echo site_url('listados/listar_organizaciones') ?>" data-hint="Catálogo de Organizaciones" data-hint-position="left">
				<div class="tile-content icon">
					<span class="icon-tree-view"></span>
				</div>
			</a>

			<!-- Catalogo Agregar Nueva Secretaría -->
			<a class="tile half bg-green" href="<?php echo site_url('listados/listar_secretarias'); ?>" data-hint="Catálogo de Secretarías" data-hint-position="left">
				<div class="tile-content icon">
					<span class="icon-grid"></span>
				</div>
			</a>

			<!-- Catalogo Agregar Nuevo Puesto -->
			<a class="tile half bg-orange" href="<?php echo site_url('listados/listar_puestos') ?>" data-hint="Catálogo de Puestos" data-hint-position="left">
				<div class="tile-content icon">
					<span class="icon-briefcase"></span>
				</div>
			</a>

			<!-- Catalogo Agregar Nuevo Pago
			<a class="tile half bg-yellow">
				<div class="tile-content icon" title="Nuevo Pago" onclick=window.location.assign("index.php/nuevo_pago")>
					<span class="icon-dollar"></span>
				</div>
			</a>
			-->

			<!-- Catalogo Agregar Nuevo Intermediario -->
			<a class="tile half bg-olive" href="<?php echo site_url('listados/listar_intermediarios'); ?>" data-hint="Catálogo de Intermediarios" data-hint-position="left">
				<div class="tile-content icon">
					<span class="icon-link"></span>
				</div>
			</a>

			<!-- Catalogo Agregar Nuevo Tipo Boveda -->
			<a class="tile half bg-crimson" href="<?php echo site_url('listados/listar_tipos_bovedas') ?>" data-hint="Catálogo de Bóvedas" data-hint-position="left">
				<div class="tile-content icon">
					<span class="icon-box"></span>
				</div>
			</a>
		</div>

		<!-- Fin de Grupo de Catalogos -->

		<!-- Grupo de Opciones para Movimientos en el Sistema -->
		<div class="tile-group double">

			<div class="tile-group-title">Movimientos</div>
			
			<!--Resgistrar una Entrada-->
			<div class="tile bg-darkCobalt">
				<a href="<?php echo site_url('nuevo_pago') ?>">
					<div class="tile-content icon">
						<span class="icon-download"></span>
					</div>
					<div class="brand">
						<div class="label">Entrada</div>
					</div>
				</a>
				
			</div>

			<!-- Registrar una salida -->
			<div class="tile bg-darkMagenta">
				<div class="tile-content icon">
					<span class="icon-upload"></span>
				</div>
				<div class="brand">
					<div class="label">Salida</div>
				</div>
			</div>

			<!-- Registrar gastos internos -->
			<div class="tile bg-darkCrimson">
				<div class="tile-content icon">
					<span class="icon-cars"></span>
				</div>
				<div class="brand">
					<div class="label">Gasto Interno</div>
				</div>
			</div>

			<!--Comprobar gastos-->
			<div class="tile bg-darkEmerald">
				<div class="tile-content icon">
					<span class="icon-shipping"></span>
				</div>
				<div class="brand">
					<div class="label">Comprobar</div>
				</div>
			</div>
		</div>
		<!-- Fin de Movimientos del Sistema -->
	</div>
</div>
</body>
</html>