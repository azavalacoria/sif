SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `inventarios` ;
CREATE SCHEMA IF NOT EXISTS `inventarios` DEFAULT CHARACTER SET utf8 ;
USE `inventarios` ;

-- -----------------------------------------------------
-- Table `inventarios`.`siri_bovedas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_bovedas` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_bovedas` (
  `id_tipo_boveda` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_boveda` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id_tipo_boveda`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`catalogos_formas_pago`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`catalogos_formas_pago` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`catalogos_formas_pago` (
  `id_forma_pago` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_forma_pago` VARCHAR(45) NOT NULL ,
  `tipo_boveda` INT(11) NOT NULL ,
  PRIMARY KEY (`id_forma_pago`) ,
  INDEX `fk_catalogos_formas_pago_catalogos_tipos_bovedas1_idx` (`tipo_boveda` ASC) ,
  CONSTRAINT `fk_catalogos_formas_pago_catalogos_tipos_bovedas1`
    FOREIGN KEY (`tipo_boveda` )
    REFERENCES `inventarios`.`siri_bovedas` (`id_tipo_boveda` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`catalogos_usuarios_permisos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`catalogos_usuarios_permisos` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`catalogos_usuarios_permisos` (
  `id_permiso` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_permiso` VARCHAR(45) NOT NULL ,
  `valor` SMALLINT(6) NOT NULL ,
  PRIMARY KEY (`id_permiso`) )
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`universoyucatan_entidades_federativas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`universoyucatan_entidades_federativas` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`universoyucatan_entidades_federativas` (
  `id_entidad_federativa` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_entidad` VARCHAR(45) NOT NULL ,
  `nombre_corto` VARCHAR(4) NULL DEFAULT NULL ,
  PRIMARY KEY (`id_entidad_federativa`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`universoyucatan_municipios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`universoyucatan_municipios` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`universoyucatan_municipios` (
  `id_municipio` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_municipio` VARCHAR(45) NOT NULL ,
  `nombre_corto` VARCHAR(45) NULL DEFAULT NULL ,
  `entidad_federativa` INT(11) NOT NULL ,
  PRIMARY KEY (`id_municipio`) ,
  INDEX `fk_universo_yucatan_municipios_universoyucatan_estados1_idx` (`entidad_federativa` ASC) ,
  CONSTRAINT `fk_universo_yucatan_municipios_universoyucatan_estados1`
    FOREIGN KEY (`entidad_federativa` )
    REFERENCES `inventarios`.`universoyucatan_entidades_federativas` (`id_entidad_federativa` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`universoyucatan_localidades`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`universoyucatan_localidades` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`universoyucatan_localidades` (
  `id_localidad` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_localidad` VARCHAR(45) NOT NULL ,
  `municipio` INT(11) NOT NULL ,
  PRIMARY KEY (`id_localidad`) ,
  INDEX `fk_table1_universo_yucatan_municipios1_idx` (`municipio` ASC) ,
  CONSTRAINT `fk_table1_universo_yucatan_municipios1`
    FOREIGN KEY (`municipio` )
    REFERENCES `inventarios`.`universoyucatan_municipios` (`id_municipio` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`catalogos_personas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`catalogos_personas` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`catalogos_personas` (
  `id_persona` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombres` VARCHAR(45) NOT NULL ,
  `apellido_paterno` VARCHAR(45) NOT NULL ,
  `apellido_materno` VARCHAR(45) NOT NULL ,
  `sexo` CHAR(1) NOT NULL ,
  `direccion` VARCHAR(45) NOT NULL ,
  `codigo_postal` MEDIUMINT(5) NULL ,
  `telefono` VARCHAR(15) NULL ,
  `curp` VARCHAR(18) NULL ,
  `seccion_electoral` VARCHAR(45) NULL ,
  `entidad_federativa` INT(11) NOT NULL ,
  `municipio` INT(11) NOT NULL ,
  `localidad` INT(11) NOT NULL ,
  PRIMARY KEY (`id_persona`) ,
  INDEX `fk_catalogos_personas_universoyucatan_entidades1_idx` (`entidad_federativa` ASC) ,
  INDEX `fk_catalogos_personas_universo_yucatan_municipios1_idx` (`municipio` ASC) ,
  INDEX `fk_catalogos_personas_universoyucatan_localidades1_idx` (`localidad` ASC) ,
  CONSTRAINT `fk_catalogos_personas_universoyucatan_entidades1`
    FOREIGN KEY (`entidad_federativa` )
    REFERENCES `inventarios`.`universoyucatan_entidades_federativas` (`id_entidad_federativa` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_catalogos_personas_universoyucatan_localidades1`
    FOREIGN KEY (`localidad` )
    REFERENCES `inventarios`.`universoyucatan_localidades` (`id_localidad` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_catalogos_personas_universo_yucatan_municipios1`
    FOREIGN KEY (`municipio` )
    REFERENCES `inventarios`.`universoyucatan_municipios` (`id_municipio` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`catalogos_puestos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`catalogos_puestos` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`catalogos_puestos` (
  `id_puesto` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_puesto` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id_puesto`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`catalogos_tipos_concepto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`catalogos_tipos_concepto` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`catalogos_tipos_concepto` (
  `id_tipo_concepto` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_tipo_concepto` VARCHAR(45) NOT NULL ,
  `descripcion_tipo_concepto` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id_tipo_concepto`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`catalogos_tipos_cuentas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`catalogos_tipos_cuentas` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`catalogos_tipos_cuentas` (
  `id_tipo_cuenta` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_tipo_cuenta` VARCHAR(45) NOT NULL ,
  `aumenta_capital` TINYINT(1) NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id_tipo_cuenta`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`catalogos_usuarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`catalogos_usuarios` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`catalogos_usuarios` (
  `id_usuario` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_usuario` VARCHAR(45) NOT NULL ,
  `contrasena` VARCHAR(45) NOT NULL ,
  `puesto` INT(11) NOT NULL ,
  `persona` INT(11) NOT NULL ,
  `permisos` INT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_usuario`) ,
  INDEX `fk_usuarios_puestos_idx` (`puesto` ASC) ,
  INDEX `fk_usuarios_personas1_idx` (`persona` ASC) ,
  CONSTRAINT `fk_usuarios_personas1`
    FOREIGN KEY (`persona` )
    REFERENCES `inventarios`.`catalogos_personas` (`id_persona` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_puestos`
    FOREIGN KEY (`puesto` )
    REFERENCES `inventarios`.`catalogos_puestos` (`id_puesto` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2;


-- -----------------------------------------------------
-- Table `inventarios`.`siri_cajas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_cajas` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_cajas` (
  `id_caja` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_caja` VARCHAR(45) NOT NULL ,
  `tipo_boveda` INT(11) NOT NULL ,
  `usuario` INT(11) NOT NULL ,
  PRIMARY KEY (`id_caja`) ,
  INDEX `fk_cajas_catalogos_tipos_bovedas1_idx` (`tipo_boveda` ASC) ,
  INDEX `fk_cajas_usuarios1_idx` (`usuario` ASC) ,
  CONSTRAINT `fk_cajas_catalogos_tipos_bovedas1`
    FOREIGN KEY (`tipo_boveda` )
    REFERENCES `inventarios`.`siri_bovedas` (`id_tipo_boveda` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cajas_usuarios1`
    FOREIGN KEY (`usuario` )
    REFERENCES `inventarios`.`catalogos_usuarios` (`id_usuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`universoyucatan_secretarias`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`universoyucatan_secretarias` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`universoyucatan_secretarias` (
  `id_secretaria` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_secretaria` VARCHAR(255) NOT NULL ,
  `siglas` VARCHAR(45) NOT NULL ,
  `direccion` VARCHAR(45) NULL DEFAULT NULL ,
  `entidad_federativa` INT(11) NOT NULL ,
  `municipio` INT(11) NOT NULL ,
  `localidad` INT(11) NOT NULL ,
  PRIMARY KEY (`id_secretaria`) ,
  INDEX `fk_universoyucatan_secretarias_universoyucatan_entidades1_idx` (`entidad_federativa` ASC) ,
  INDEX `fk_universoyucatan_secretarias_universo_yucatan_municipios1_idx` (`municipio` ASC) ,
  INDEX `fk_universoyucatan_secretarias_universoyucatan_localidades1_idx` (`localidad` ASC) ,
  CONSTRAINT `fk_universoyucatan_secretarias_universoyucatan_entidades1`
    FOREIGN KEY (`entidad_federativa` )
    REFERENCES `inventarios`.`universoyucatan_entidades_federativas` (`id_entidad_federativa` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_universoyucatan_secretarias_universoyucatan_localidades1`
    FOREIGN KEY (`localidad` )
    REFERENCES `inventarios`.`universoyucatan_localidades` (`id_localidad` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_universoyucatan_secretarias_universo_yucatan_municipios1`
    FOREIGN KEY (`municipio` )
    REFERENCES `inventarios`.`universoyucatan_municipios` (`id_municipio` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`siri_entes_generadores_flujos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_entes_generadores_flujos` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_entes_generadores_flujos` (
  `id_ente` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `titular` INT(11) NOT NULL ,
  `secretaria` INT(11) NOT NULL ,
  PRIMARY KEY (`id_ente`) ,
  INDEX `fk_entes_generadores_flujos_universoyucatan_secretarias1_idx` (`secretaria` ASC) ,
  INDEX `titular` (`titular` ASC) ,
  CONSTRAINT `fk_entes_generadores_flujos_universoyucatan_secretarias1`
    FOREIGN KEY (`secretaria` )
    REFERENCES `inventarios`.`universoyucatan_secretarias` (`id_secretaria` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `siri_entes_generadores_flujos_ibfk_1`
    FOREIGN KEY (`titular` )
    REFERENCES `inventarios`.`catalogos_personas` (`id_persona` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`universoyucatan_organizaciones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`universoyucatan_organizaciones` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`universoyucatan_organizaciones` (
  `id_organizacion` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_organizacion` VARCHAR(150) NOT NULL ,
  `descripcion` MEDIUMTEXT NOT NULL ,
  `siglas` VARCHAR(45) NULL DEFAULT NULL ,
  `direccion` VARCHAR(45) NOT NULL ,
  `entidad_federativa` INT(11) NOT NULL ,
  `municipio` INT(11) NOT NULL ,
  `localidad` INT(11) NOT NULL ,
  PRIMARY KEY (`id_organizacion`) ,
  INDEX `fk_universoyucatan_organizaciones_universoyucatan_entidades_idx` (`entidad_federativa` ASC) ,
  INDEX `fk_universoyucatan_organizaciones_universoyucatan_municipio_idx` (`municipio` ASC) ,
  INDEX `fk_universoyucatan_organizaciones_universoyucatan_localidad_idx` (`localidad` ASC) ,
  CONSTRAINT `fk_universoyucatan_organizaciones_universoyucatan_entidades_f1`
    FOREIGN KEY (`entidad_federativa` )
    REFERENCES `inventarios`.`universoyucatan_entidades_federativas` (`id_entidad_federativa` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_universoyucatan_organizaciones_universoyucatan_municipios1`
    FOREIGN KEY (`municipio` )
    REFERENCES `inventarios`.`universoyucatan_municipios` (`id_municipio` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_universoyucatan_organizaciones_universoyucatan_localidades1`
    FOREIGN KEY (`localidad` )
    REFERENCES `inventarios`.`universoyucatan_localidades` (`id_localidad` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inventarios`.`siri_intermediarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_intermediarios` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_intermediarios` (
  `id_intermediario` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `persona` INT(11) NOT NULL ,
  `organizacion` INT(11) NOT NULL ,
  PRIMARY KEY (`id_intermediario`) ,
  INDEX `fk_intermediarios_universoyucatan_organizaciones1_idx` (`organizacion` ASC) ,
  INDEX `fk_siri_intermediarios_catalogos_personas1_idx` (`persona` ASC) ,
  CONSTRAINT `fk_intermediarios_universoyucatan_organizaciones1`
    FOREIGN KEY (`organizacion` )
    REFERENCES `inventarios`.`universoyucatan_organizaciones` (`id_organizacion` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_siri_intermediarios_catalogos_personas1`
    FOREIGN KEY (`persona` )
    REFERENCES `inventarios`.`catalogos_personas` (`id_persona` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`siri_responsables_proyectos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_responsables_proyectos` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_responsables_proyectos` (
  `id_responsable` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `persona` INT(11) NOT NULL ,
  `organizacion` INT(11) NOT NULL ,
  PRIMARY KEY (`id_responsable`) ,
  INDEX `fk_responsables_proyectos_universoyucatan_organizaciones1_idx` (`organizacion` ASC) ,
  INDEX `fk_siri_responsables_proyectos_catalogos_personas1_idx` (`persona` ASC) ,
  CONSTRAINT `fk_responsables_proyectos_universoyucatan_organizaciones1`
    FOREIGN KEY (`organizacion` )
    REFERENCES `inventarios`.`universoyucatan_organizaciones` (`id_organizacion` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_siri_responsables_proyectos_catalogos_personas1`
    FOREIGN KEY (`persona` )
    REFERENCES `inventarios`.`catalogos_personas` (`id_persona` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`siri_estados_concepto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_estados_concepto` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_estados_concepto` (
  `id_estado_concepto` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_estado_concepto` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id_estado_concepto`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`siri_conceptos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_conceptos` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_conceptos` (
  `id_concepto` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `nombre_concepto` VARCHAR(45) NOT NULL ,
  `fecha_creacion` DATETIME NOT NULL ,
  `tipo_cuenta` INT(11) NOT NULL ,
  `ente_generador_flujo` INT(11) NOT NULL ,
  `intermediario` INT(11) NOT NULL ,
  `responsable_proyecto` INT(11) NOT NULL ,
  `tipo_concepto` INT(11) NOT NULL ,
  `estado` INT(11) NOT NULL ,
  PRIMARY KEY (`id_concepto`) ,
  INDEX `fk_conceptos_entes_generadores_flujos1_idx` (`ente_generador_flujo` ASC) ,
  INDEX `fk_conceptos_intermediarios1_idx` (`intermediario` ASC) ,
  INDEX `fk_conceptos_responsables_proyectos1_idx` (`responsable_proyecto` ASC) ,
  INDEX `fk_conceptos_tipos_concepto1_idx` (`tipo_concepto` ASC) ,
  INDEX `fk_siri_conceptos_siri_estados_concepto1_idx` (`estado` ASC) ,
  INDEX `fk_siri_conceptos_catalogos_tipos_cuentas1_idx` (`tipo_cuenta` ASC) ,
  CONSTRAINT `fk_conceptos_entes_generadores_flujos1`
    FOREIGN KEY (`ente_generador_flujo` )
    REFERENCES `inventarios`.`siri_entes_generadores_flujos` (`id_ente` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_conceptos_intermediarios1`
    FOREIGN KEY (`intermediario` )
    REFERENCES `inventarios`.`siri_intermediarios` (`id_intermediario` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_conceptos_responsables_proyectos1`
    FOREIGN KEY (`responsable_proyecto` )
    REFERENCES `inventarios`.`siri_responsables_proyectos` (`id_responsable` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_conceptos_tipos_concepto1`
    FOREIGN KEY (`tipo_concepto` )
    REFERENCES `inventarios`.`catalogos_tipos_concepto` (`id_tipo_concepto` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_siri_conceptos_siri_estados_concepto1`
    FOREIGN KEY (`estado` )
    REFERENCES `inventarios`.`siri_estados_concepto` (`id_estado_concepto` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_siri_conceptos_catalogos_tipos_cuentas1`
    FOREIGN KEY (`tipo_cuenta` )
    REFERENCES `inventarios`.`catalogos_tipos_cuentas` (`id_tipo_cuenta` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inventarios`.`siri_conceptos_detalles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_conceptos_detalles` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_conceptos_detalles` (
  `id_detalle_concepto` INT(11) NOT NULL AUTO_INCREMENT ,
  `concepto` INT(11) NOT NULL ,
  `descripcion_concepto` LONGTEXT NOT NULL ,
  `duracion_en_meses` MEDIUMINT(9) NOT NULL ,
  `latitud` FLOAT NULL DEFAULT NULL ,
  `longitud` FLOAT NULL DEFAULT NULL ,
  `municipio` INT(11) NOT NULL ,
  `localidad` INT(11) NOT NULL ,
  `entidad_federativa` INT(11) NOT NULL ,
  PRIMARY KEY (`id_detalle_concepto`, `concepto`) ,
  INDEX `fk_detalles_concepto_universoyucatan_municipios1_idx` (`municipio` ASC) ,
  INDEX `fk_detalles_concepto_universoyucatan_localidades1_idx` (`localidad` ASC) ,
  INDEX `fk_detalles_concepto_universoyucatan_entidades1_idx` (`entidad_federativa` ASC) ,
  INDEX `fk_siri_detalles_concepto_siri_conceptos1_idx` (`concepto` ASC) ,
  CONSTRAINT `fk_detalles_concepto_universoyucatan_entidades1`
    FOREIGN KEY (`entidad_federativa` )
    REFERENCES `inventarios`.`universoyucatan_entidades_federativas` (`id_entidad_federativa` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_detalles_concepto_universoyucatan_localidades1`
    FOREIGN KEY (`localidad` )
    REFERENCES `inventarios`.`universoyucatan_localidades` (`id_localidad` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalles_concepto_universoyucatan_municipios1`
    FOREIGN KEY (`municipio` )
    REFERENCES `inventarios`.`universoyucatan_municipios` (`id_municipio` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_siri_detalles_concepto_siri_conceptos1`
    FOREIGN KEY (`concepto` )
    REFERENCES `inventarios`.`siri_conceptos` (`id_concepto` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`siri_conceptos_detalles_financieros`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_conceptos_detalles_financieros` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_conceptos_detalles_financieros` (
  `id_detalle_financieros` INT(11) NOT NULL AUTO_INCREMENT ,
  `concepto` INT(11) NOT NULL ,
  `costo_publicado` FLOAT(24,2) NOT NULL ,
  `costo_autorizado` FLOAT(24,2) NOT NULL ,
  `costo_incrementado` FLOAT(24,2) NOT NULL DEFAULT '0.0000' ,
  `monto_acordado` FLOAT(24,2) NOT NULL ,
  `numero_pagos` INT(11) NOT NULL DEFAULT '1' ,
  `forma_pago` INT(11) NOT NULL ,
  PRIMARY KEY (`id_detalle_financieros`) ,
  INDEX `fk_detalles_financieros_catalogos_formas_pago1_idx` (`forma_pago` ASC) ,
  INDEX `fk_siri_detalles_financieros_siri_conceptos1_idx` (`concepto` ASC) ,
  CONSTRAINT `fk_detalles_financieros_catalogos_formas_pago1`
    FOREIGN KEY (`forma_pago` )
    REFERENCES `inventarios`.`catalogos_formas_pago` (`id_forma_pago` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_siri_detalles_financieros_siri_conceptos1`
    FOREIGN KEY (`concepto` )
    REFERENCES `inventarios`.`siri_conceptos` (`id_concepto` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `inventarios`.`siri_movimientos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_movimientos` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_movimientos` (
  `id_pago` INT(11) NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `concepto` INT(11) NOT NULL ,
  `fecha_pago` DATETIME NOT NULL ,
  `forma_pago` INT(11) NOT NULL ,
  `tipo_cuenta` INT(11) NOT NULL ,
  `monto` FLOAT(24,2) NOT NULL DEFAULT 0.00 ,
  PRIMARY KEY (`id_pago`) ,
  INDEX `fk_pagos_siri_conceptos1_idx` (`concepto` ASC) ,
  INDEX `fk_siri_pagos_catalogos_formas_pago1_idx` (`forma_pago` ASC) ,
  INDEX `fk_siri_pagos_catalogos_tipos_cuentas1_idx` (`tipo_cuenta` ASC) ,
  CONSTRAINT `fk_pagos_siri_conceptos1`
    FOREIGN KEY (`concepto` )
    REFERENCES `inventarios`.`siri_conceptos` (`id_concepto` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_siri_pagos_catalogos_formas_pago1`
    FOREIGN KEY (`forma_pago` )
    REFERENCES `inventarios`.`catalogos_formas_pago` (`id_forma_pago` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_siri_pagos_catalogos_tipos_cuentas1`
    FOREIGN KEY (`tipo_cuenta` )
    REFERENCES `inventarios`.`catalogos_tipos_cuentas` (`id_tipo_cuenta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inventarios`.`siri_bovedas_estados`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_bovedas_estados` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_bovedas_estados` (
  `id_estado_boveda` INT NOT NULL AUTO_INCREMENT ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `tipo_boveda` INT(11) NOT NULL ,
  `saldo_actual` FLOAT(24,2) NOT NULL ,
  PRIMARY KEY (`id_estado_boveda`) ,
  INDEX `fk_estado_boveda_catalogos_tipos_bovedas1_idx` (`tipo_boveda` ASC) ,
  CONSTRAINT `fk_estado_boveda_catalogos_tipos_bovedas1`
    FOREIGN KEY (`tipo_boveda` )
    REFERENCES `inventarios`.`siri_bovedas` (`id_tipo_boveda` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inventarios`.`siri_cajas_historial`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inventarios`.`siri_cajas_historial` ;

CREATE  TABLE IF NOT EXISTS `inventarios`.`siri_cajas_historial` (
  `id_caja_historial` INT NOT NULL AUTO_INCREMENT ,
  `caja` INT(11) NOT NULL ,
  `fecha_apertura` DATETIME NOT NULL ,
  `saldo_apertura` FLOAT(24,2) NOT NULL DEFAULT 0.00 ,
  `fecha_cierre` DATETIME NOT NULL ,
  `saldo_cierre` FLOAT(24,2) NOT NULL DEFAULT 0.00 ,
  PRIMARY KEY (`id_caja_historial`) ,
  INDEX `fk_siri_cajas_historial_siri_cajas1` (`caja` ASC) ,
  CONSTRAINT `fk_siri_cajas_historial_siri_cajas1`
    FOREIGN KEY (`caja` )
    REFERENCES `inventarios`.`siri_cajas` (`id_caja` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
